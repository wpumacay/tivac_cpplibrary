/*
 * ejemploUart.hpp
 *
 *  Created on: 18/01/2014
 *      Author: Ricardo Rodriguez
 */

#include "include/Gpio.hpp"
#include "include/clock.hpp"
#include "delays.hpp"
#include "include/Uart.h"
#include "include/Core.hpp"
#include "interrupts.hpp"


int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOA);

	PF1::enableAsDigital();
	PF1::setMode(Gpio::options::mode::gpio);
	PF1::setIOmode(Gpio::options::IOmode::output);

	PF3::enableAsDigital();
	PF3::setMode(Gpio::options::mode::gpio);
	PF3::setIOmode(Gpio::options::IOmode::output);

	PA0::enableAsDigital();
	PA0::setMode(Gpio::options::mode::alternate);
	PA0::setAlternateMode(Gpio::options::altModes::alt1);

	PA1::enableAsDigital();
	PA1::setMode(Gpio::options::mode::alternate);
	PA1::setAlternateMode(Gpio::options::altModes::alt1);

	UART0::enableClock();
	UART0::configUart(uart::configOptions::baudrate::baud_115200);

	core::IntEnableMaster();
	core::enableInterrupt(core::interrupts::uart0);

	while(1)
	{
		PF3::toogle();
		delays::delay_ms(500);
	}
}

void interruptFuncs::uart0rxtx_isr()
{
	UART0::clearInterrupts(uart::configOptions::interrupts::receiveInt);
	unsigned char foo = UART0::receiveByte();
	UART0::sendString("oe!!!!... LOLOLOL!!!\n\r");
	UART0::sendString("TUUU� has enviado: ");
	UART0::sendByte(foo);
	UART0::sendByte(10);
	UART0::sendByte(13);
	UART0::sendByte(7);
}
