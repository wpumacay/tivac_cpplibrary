/*
 * usb.hpp
 *
 *  Created on: Sep 12, 2013
 *      Author: wilbert
 */

#include "common.h"
#include "interrupts.hpp"
#include "mapusb.hpp"

/*
 * DEVELOPING.... IT'LL TAKE SOME DAYS... (T_T)'
 *
 */

namespace usb
{

	namespace usbStatus
	{
		enum status
		{
			SUSPEND 		= 1,
			RESUME 			= 2,
			RESET   		= 4,
			START_OF_FRAME 	= 8
		};
	}

	//end points...

	//configure endpoints




	//descriptors...

	//device descriptor

	typedef struct
	{

	}deviceInstance;

	class usb
	{
		public:


	};

}


void interruptFuncs::usb0_isr()
{
	//get status register
	u8 intStatus = reinterpret_cast<mapusb::USBREGS *>
						(mapusb::usbModAddress)->USBIS;

	//check what the heck happened!!!
	if((intStatus & 0x08) == usb::usbStatus::START_OF_FRAME)//handle start of frame condition
	{

	}
	if((intStatus & 0x04) == usb::usbStatus::RESET)//handle reset condition
	{

	}
	if((intStatus & 0x02) == usb::usbStatus::RESUME)//handle resume condition
	{

	}
	if((intStatus & 0x01) == usb::usbStatus::SUSPEND)//handle suspend condition
	{

	}

}
