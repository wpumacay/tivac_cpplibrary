/*
 * systemIdent.hpp
 *
 *  Created on: 25/09/2013
 *      Author: wilbert
 */

#include "linalg.hpp"

namespace sysIdent
{


	namespace recursive
	{

		namespace kalman
		{
			struct belief
			{
				linalg::matrix mu;
				linalg::matrix sigma;
			};
			belief predict(belief x,linalg::matrix A,linalg::matrix R)
			{
				x.mu 		= A*x.mu;
				x.sigma 	= A*x.sigma*A.trsp() + R;
				return x;
			}
			belief correct(belief x,linalg::matrix C,linalg::matrix Q,linalg::matrix z)
			{
				linalg::matrix K;
				K 		= x.sigma * C.trsp() * ((C*x.sigma*C.trsp() + Q).inv());
				x.mu 	= x.mu + K*(z - C*x.mu);
				x.sigma = (linalg::identity(K.nRows) - K*C)*x.sigma;
				return x;
			}
		}



	}


}
