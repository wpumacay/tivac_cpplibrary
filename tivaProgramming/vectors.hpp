/*
 * vectors.hpp
 *
 *  Created on: 26/09/2013
 *      Author: wilbert
 */

#ifndef VECTORS_HPP_
#define VECTORS_HPP_

namespace vectors
{
	template<unsigned int dim>
	class vector
	{
		public:

		double buff[3];

		vector()
		{
			buff[0]=0;
			buff[1]=0;
			buff[2]=0;
		}
		vector(double x,double y,double z)
		{
			buff[0] = x;
			buff[1] = y;
			buff[2] = z;
		}

		vector operator + (vector v2)
		{
			vector v3;
			u8 i;
			for(i=0;i<3;i++)
			{
				v3.buff[i] = this->buff[i] + v2.buff[i];
			}
			return v3;
		}

	};

	vector sumar(vector v1,vector v2)
	{
		vector v3;
		u8 i;
		for(i=0;i<3;i++)
		{
			v3.buff[i] = v1.buff[i] + v2.buff[i];
		}
		return v3;
	}

}




#endif /* VECTORS_HPP_ */
