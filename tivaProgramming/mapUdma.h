/*
 * mapUdma.h
 *
 *  Created on: Aug 17, 2013
 *      Author: wilbert
 */

#ifndef MAPUDMA_H_
#define MAPUDMA_H_


#include "common.h"

namespace mapudma
{

	namespace udmaAddress
	{
		u32 dmaRegsAddress = 0x400FF000;
	}

	struct uDMAregs
	{
		u32 DMASTAT;
		u32 DMACFG;
		u32 DMACTLBASE;
		u32 DMAALTBASE;
		u32 DMAWAITSTAT;
		u32 DMASWREQ;
		u32 DMAUSEBURSTSET;
		u32 DMAUSEBURSTCLR;
		u32 DMAREQMASKSET;
		u32 DMAREQMASKCLR;
		u32 DMAENASET;
		u32 DMAENACLR;
		u32 DMAALTSET;
		u32 DMAALTCLR;
		u32 DMAPRIOSET;
		u32 DMAPRIOCLR;
		u32 reserved1[3];
		u32 DMAERRCLR;
		u32 reserved2[300];
		u32 DMACHASGN;
		u32 DMACHIS;
		u32 reserved3[2];
		u32 DMACHMAP0;
		u32 DMACHMAP1;
		u32 DMACHMAP2;
		u32 DMACHMAP3;
		u32 reserved4[684];
		u32 DMAPeriphlD4;
		u32 reserved5[3];
		u32 DMAPeriphlD0;
		u32 DMAPeriphlD1;
		u32 DMAPeriphlD2;
		u32 DMAPeriphlD3;
		u32 DMAPCellD0;
		u32 DMAPCellD1;
		u32 DMAPCellD2;
		u32 DMAPCellD3;

	};

}


#endif /* MAPUDMA_H_ */
