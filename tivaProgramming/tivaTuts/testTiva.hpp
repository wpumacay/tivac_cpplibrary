/*
 * testTiva.hpp
 *
 *  Created on: 12/10/2013
 *      Author: JOTA
 */
#include "include/clock.hpp"
#include "include/Gpio.hpp"
#include "include/Uart.h"
#include "delays.hpp"
#include "include/Core.hpp"
#include "interrupts.hpp"
#include "include/Gptimer.hpp"
//#include "myVectors.hpp"
//#include "usb.hpp"
int main()
{
	clock::clock::config(
			clock::configOptions::clockSource::mainOscilator,
			clock::configOptions::clockRate::clock_80Mhz,
			clock::configOptions::enablePIOSC::enablePinternalOsc,
			clock::configOptions::enableMOSC::enableMainOsc);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOA);

	PF1::enableAsDigital();
	PF1::setMode(Gpio::options::mode::gpio);
	PF1::setIOmode(Gpio::options::IOmode::output);

	PF1::setLow();

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PF2::setLow();

	PF3::enableAsDigital();
	PF3::setMode(Gpio::options::mode::gpio);
	PF3::setIOmode(Gpio::options::IOmode::output);

	PF3::setLow();


	PA0::enableAsDigital();
	PA0::setMode(Gpio::options::mode::alternate);
	PA0::setAlternateMode(Gpio::options::altModes::alt1);

	PA1::enableAsDigital();
	PA1::setMode(Gpio::options::mode::alternate);
	PA1::setAlternateMode(Gpio::options::altModes::alt1);

	UART0::enableClock();
	UART0::configUart(uart::configOptions::baudrate::baud_115200,
					  uart::configOptions::nbits::bits_8,
					  uart::configOptions::parity::none,
					  uart::configOptions::stopbits::stopBits_one,
					  uart::configOptions::interrupts::receiveInt,
					  uart::configOptions::fifo::fifoDisabled,
					  uart::clockSource::systemClock);

	/*
	float *x = new float[1];
	myVectors::vector v1(3);
	myVectors::vector v2(3);
	myVectors::vector v3(3);
	v3 = myVectors::add(v1,v2);
	 */

	timer0::enableClock();
	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);
	timer0::config(gpTimer::subTimers::subTimerA,
				   100,
				   gpTimer::options::timerOptions::joiningMode::timers_joinTimers,
				   gpTimer::options::timerOptions::timerMode::periodic,
				   gpTimer::options::timerOptions::countDirection::down,
				   gpTimer::options::timerOptions::interrupts::timerAtimeout);
	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);

	core::enableInterrupt(core::interrupts::uart0);
	core::enableInterrupt(core::interrupts::timer0a_simple);
	core::IntEnableMaster();

	while(1)
	{
		PF3::setHigh();
		delays::delay_ms(500);
		//UART0::sendString("hey!");
		PF3::setLow();
		delays::delay_ms(500);
		//UART0::sendString("hey xD!!!");
	}
}

void interruptFuncs::timer0SubA_isr()
{
	timer0::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);
	PF2::toogle();

}


void interruptFuncs::uart0rxtx_isr()
{
	UART0::clearInterrupts(uart::configOptions::interrupts::receiveInt);
	unsigned char foo = UART0::receiveByte();
	UART0::sendString("sent: ");
	UART0::sendByte(foo);
	UART0::sendString(" !!!\n\r");
	PF1::toogle();
}

