/*
 * testQEI3.hpp
 *
 *  Created on: 21/10/2013
 *      Author: wilbert
 */

#include "include/Gpio.hpp"
#include "include/clock.hpp"
#include "delays.hpp"
#include "QEI.hpp"
#include "include/Gptimer.hpp"
#include "include/Core.hpp"
#include "interrupts.hpp"

typedef qei::qeiMod<mapQEI::QEImodules::QEI1> qe1;

u32 encoderCount_t 		= 0;
u32 encoderCount_t_1 	= 0;
long int pulses 				= 0;

u32 speedReg = 0;

int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
								 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOC);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PF3::enableAsDigital();
	PF3::setMode(Gpio::options::mode::gpio);
	PF3::setIOmode(Gpio::options::IOmode::output);

	PC5::enableAsDigital();
	PC5::setPullUp();
	PC5::setMode(Gpio::options::mode::alternate);
	PC5::setAlternateMode(Gpio::options::altModes::alt6);

	PC6::enableAsDigital();
	PC6::setPullUp();
	PC6::setMode(Gpio::options::mode::alternate);
	PC6::setAlternateMode(Gpio::options::altModes::alt6);

	qe1::enableClock();
	qe1::setTimer(80000000);
	qe1::config(qei::options::stallOnDebugger::dontStall,
					 qei::options::velocityPredivider::div_1,
					 qei::options::capVelocity::captureVel,
					 qei::options::captureMode::mode2);

	qe1::enableQEI();

	PC4::enableAsDigital();
	PC4::setMode(Gpio::options::mode::alternate);
	PC4::setAlternateMode(Gpio::options::altModes::alt7);

	pwmW_0A::enableClock();
	pwmW_0A::config(1,0.9,gpTimer::options::pwmOptions::outputLevel::inverted);

	timer0::enableClock();
	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);
	timer0::config(gpTimer::subTimers::subTimerA,
						  100,
						  gpTimer::options::timerOptions::joiningMode::timers_joinTimers,
						  gpTimer::options::timerOptions::timerMode::periodic,
						  gpTimer::options::timerOptions::countDirection::down,
						  gpTimer::options::timerOptions::interrupts::timerAtimeout);
	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);

	core::IntEnableMaster();
	core::enableInterrupt(core::interrupts::timer0a_simple);

	while(1)
	{
		PF2::toogle();
		delays::delay_ms(100);
	}
}

void interruptFuncs::timer0SubA_isr()
{
	timer0::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);
	/*
	encoderCount_t = qe1::getCount();
	pulses = encoderCount_t - encoderCount_t_1;
	encoderCount_t_1 = encoderCount_t;
	PF3::toogle();
	*/
	speedReg = qe1::getSpeedReg();
}
