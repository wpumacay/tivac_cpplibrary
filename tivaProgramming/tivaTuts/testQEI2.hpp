/*
 * testQEI2.hpp
 *
 *  Created on: 18/10/2013
 *      Author: wilbert
 */
#include "include/Gpio.hpp"
#include "include/clock.hpp"
#include "delays.hpp"
#include "QEI.hpp"

typedef qei::qeiMod<mapQEI::QEImodules::QEI0> qe0;

u32 encoderCount = 0;
u32 speedCount		= 0;
double rpm = 0;

int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
								 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOD);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PD6::unlock();
	PD6::enableCommit();
	PD6::enableAsDigital();
	PD6::setPullUp();
	PD6::setMode(Gpio::options::mode::alternate);
	PD6::setAlternateMode(Gpio::options::altModes::alt6);



	PD7::unlock();
	PD7::enableCommit();
	PD7::enableAsDigital();
	PD7::setPullUp();
	PD7::setMode(Gpio::options::mode::alternate);
	PD7::setAlternateMode(Gpio::options::altModes::alt6);

	qe0::enableClock();
	qe0::config(360,
				qei::options::stallOnDebugger::dontStall,
				qei::options::velocityPredivider::div_1,
				qei::options::capVelocity::captureVel,
				qei::options::captureMode::mode1);
	qe0::setTimer(8000000);
	qe0::enableQEI();

	while(1)
	{
		PF2::toogle();
		encoderCount 	= qe0::getCount();
		speedCount		= qe0::getSpeedReg();
		//rpm = (1000.*1.*speedCount*60.)/(80000)
		rpm = ((double)(speedCount))*60/18.;
		delays::delay_ms(10);
	}
}

