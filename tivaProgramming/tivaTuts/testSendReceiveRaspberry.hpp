/*
 * testSendReceiveRaspberry.hpp
 *
 *  Created on: 23/11/2013
 *      Author: wilbert
 */

#include "include/Gpio.hpp"
#include "include/clock.hpp"
#include "include/Uart.h"
#include "delays.hpp"
#include "include/Core.hpp"
#include "interrupts.hpp"

union data
{
	float floatData;
	u8 bytes[4];
};

data buff;
u8 foo = 0;

int pos = 0;

int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
								 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOE);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PE0::enableAsDigital();
	PE0::setMode(Gpio::options::mode::alternate);
	PE0::setAlternateMode(Gpio::options::altModes::alt1);

	PE1::enableAsDigital();
	PE1::setMode(Gpio::options::mode::alternate);
	PE1::setAlternateMode(Gpio::options::altModes::alt1);

	UART7::enableClock();
	UART7::configUart(uart::configOptions::baudrate::baud_115200);

	core::IntEnableMaster();
	core::enableInterrupt(core::interrupts::uart7);

	UART7::sendByte('s');

	while(1)
	{
		PF2::toogle();
		delays::delay_ms(100);
	}
}


void interruptFuncs::uart7rxtx_isr()
{
	UART7::clearInterrupts(uart::configOptions::interrupts::receiveInt);
	/*
	if (pos==0)
	{
		foo = UART7::receiveByte();
	}
	else
	{
		buff.bytes[pos-1] = UART7::receiveByte();
	}
	pos++;
	if(pos>4)
	{
		pos = 0;
		UART7::sendByte('s');
	}
	*/
	buff.bytes[pos] = UART7::receiveByte();
	pos++;
	if(pos>3)
	{
		pos = 0;
		UART7::sendByte('s');
	}
}
