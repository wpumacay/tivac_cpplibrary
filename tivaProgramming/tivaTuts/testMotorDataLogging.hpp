/*
 * testMotorDataLogging.hpp
 *
 *  Created on: 30/12/2013
 *      Author: wilbert
 */

#include "include/Gpio.hpp"
#include "include/clock.hpp"
#include "delays.hpp"
#include "QEI.hpp"
#include "include/Gptimer.hpp"
#include "include/Uart.h"
#include "include/Core.hpp"
#include "interrupts.hpp"

typedef qei::qeiMod<mapQEI::QEImodules::QEI1> qe1;

u32 encoderCount_t 		= 0;
u32 encoderCount_t_1 	= 0;
long int pulses 				= 0;

u16 speedReg = 0;

float duty = 0.2;

union data
{
	u16 value;
	u8 bytes[2];
};

data buff;

int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
								 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOC);
	Gpio::enableClock(Gpio::peripheral::GPIOA);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PF3::enableAsDigital();
	PF3::setMode(Gpio::options::mode::gpio);
	PF3::setIOmode(Gpio::options::IOmode::output);

	PA0::enableAsDigital();
	PA0::setMode(Gpio::options::mode::alternate);
	PA0::setAlternateMode(Gpio::options::altModes::alt1);

	PA1::enableAsDigital();
	PA1::setMode(Gpio::options::mode::alternate);
	PA1::setAlternateMode(Gpio::options::altModes::alt1);

	UART0::enableClock();
	UART0::configUart(uart::configOptions::baudrate::baud_1000000);

	PC5::enableAsDigital();
	PC5::setPullUp();
	PC5::setMode(Gpio::options::mode::alternate);
	PC5::setAlternateMode(Gpio::options::altModes::alt6);

	PC6::enableAsDigital();
	PC6::setPullUp();
	PC6::setMode(Gpio::options::mode::alternate);
	PC6::setAlternateMode(Gpio::options::altModes::alt6);

	qe1::enableClock();
	qe1::setTimer(8000000);
	qe1::config(qei::options::stallOnDebugger::dontStall,
					 qei::options::velocityPredivider::div_1,
					 qei::options::capVelocity::captureVel,
					 qei::options::captureMode::mode2);

	qe1::enableQEI();

	PC4::enableAsDigital();
	PC4::setMode(Gpio::options::mode::alternate);
	PC4::setAlternateMode(Gpio::options::altModes::alt7);

	pwmW_0A::enableClock();
	pwmW_0A::config(1,
								0.2,
								gpTimer::options::pwmOptions::outputLevel::inverted);

	core::IntEnableMaster();
	core::enableInterrupt(core::interrupts::uart0);

	while(1)
	{
		PF2::toogle();
		delays::delay_ms(100);
	}
}

void interruptFuncs::uart0rxtx_isr()
{
	UART0::clearInterrupts(uart::configOptions::interrupts::receiveInt);
	u8 foo = UART0::receiveByte();
	if (foo == 'h')
	{
		duty = 0.25;
	}
	else if(foo == 'l')
	{
		duty = 0.0;
	}
	PF3::toogle();
	speedReg = (u16)(qe1::getSpeedReg());
	pwmW_0A::setDuty(duty);
	buff.value = speedReg;
	UART0::sendByte(buff.bytes[1]);
	UART0::sendByte(buff.bytes[0]);
}
