/*
 * testQEI.hpp
 *
 *  Created on: 15/10/2013
 *      Author: wilbert
 */
#include "include/Gpio.hpp"
#include "include/clock.hpp"
#include "delays.hpp"
#include "QEI.hpp"

typedef qei::qeiMod<mapQEI::QEImodules::QEI1> qe1;

int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
								 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOC);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PC5::enableAsDigital();
	PC5::setPullUp();
	PC5::setMode(Gpio::options::mode::alternate);
	PC5::setAlternateMode(Gpio::options::altModes::alt6);

	PC6::enableAsDigital();
	PC6::setPullUp();
	PC6::setMode(Gpio::options::mode::alternate);
	PC6::setAlternateMode(Gpio::options::altModes::alt6);

	qe1::enableClock();
	qe1::config(360,
				qei::options::stallOnDebugger::dontStall,
				qei::options::velocityPredivider::div_1,
				qei::options::capVelocity::dontCaptVel,
				qei::options::captureMode::mode1);

	qe1::enableQEI();

	while(1)
	{
		PF2::toogle();
		delays::delay_ms(100);
	}
}

