/*
 * main.cpp
 */



//nuestras libs
#include "Core.h"
#include "SysCtrl.h"
#include "Gpio.h"
#include "Uart.h"
#include "interrupts.hpp"
#include "include/Gptimer.hpp"
#include "clock.h"
//del stellarisware
#include "driverlib/sysctl.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/timer.h"

typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio1> ledr;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio2> ledb;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio3> ledg;

typedef Gpio::gpio<mapGpio::GPIOPortA_APB,mapGpio::gpio0> u0rx;
typedef Gpio::gpio<mapGpio::GPIOPortA_APB,mapGpio::gpio1> u0tx;


typedef gpTimer::timer<gpTimer::timer0_simple,
					   gpTimer::options::operationMode::oneshot_or_periodic>
					   timer0;

typedef uart::uart<uart::module::uart0> port;

unsigned int foo = 0;

int main()
{
	SysCtlClockSet(SYSCTL_SYSDIV_2_5|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);

	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);

	ledr::enableAsDigital();
	ledr::setMode(mapGpio::options::mode::gpio);
	ledr::setIOmode(mapGpio::options::IOmode::output);

	ledb::enableAsDigital();
	ledb::setMode(mapGpio::options::mode::gpio);
	ledb::setIOmode(mapGpio::options::IOmode::output);

	ledg::enableAsDigital();
	ledg::setMode(mapGpio::options::mode::gpio);
	ledg::setIOmode(mapGpio::options::IOmode::output);

	u0rx::enableAsDigital();
	u0rx::setMode(mapGpio::options::mode::alternate);
	u0rx::setAlternateMode(mapGpio::options::altModes::alt1);

	u0tx::enableAsDigital();
	u0tx::setMode(mapGpio::options::mode::alternate);
	u0tx::setAlternateMode(mapGpio::options::altModes::alt1);

	port::configUart(uart::configOptions::baudrate::baud_115200,
					 uart::configOptions::nbits::bits_8,
					 uart::configOptions::parity::none,
					 uart::configOptions::stopbits::stopBits_one,
					 //uart::configOptions::interrupts::receiveTimeOut|
					 uart::configOptions::interrupts::receiveInt,
					 uart::configOptions::fifo::fifoDisabled,
					 uart::clockSource::pioscClock);

	timer0::configTimer(gpTimer::subTimers::subTimerA,
						1000,
						gpTimer::options::joiningMode::timers_joinTimers,
						gpTimer::options::timerMode::periodic,
						gpTimer::options::countDirection::down,
						gpTimer::options::interrupts::timerAtimeout);

	core::enableInterrupt(core::interrupts::timer0a_simple);
	core::enableInterrupt(core::interrupts::uart0);
	sysCtrl::IntEnableMaster();



	while(1){

	}

}

void interruptFuncs::uart0rxtx_isr()
{
	unsigned char var = port::receiveByte();
	ledb::toogle();
	port::sendByte('o');
	port::sendByte('k');
	port::sendByte(':');
	port::sendByte(' ');
	port::sendByte(var);
	port::sendByte('\n');
	port::sendByte('\r');
	port::clearInterrupts(uart::configOptions::interrupts::receiveTimeOut|
						  uart::configOptions::interrupts::receiveInt);
}




void interruptFuncs::timer0SubA_isr()
{
	TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
	foo++;
	switch(foo%2)
	{
		case 0:
			ledr::setHigh();
			ledg::setLow();
		break;
		case 1:
			ledr::setLow();
			ledg::setHigh();
		break;
	}

}

