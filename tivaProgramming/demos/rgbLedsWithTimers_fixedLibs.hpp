/*
 * main.cpp
 */



//nuestras libs
#include "Core.h"
#include "SysCtrl.h"
#include "Gpio.h"
#include "interrupts.hpp"
#include "clock.h"
#include "include/Gptimer.hpp"


typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio1> ledr;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio2> ledb;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio3> ledg;

typedef gpTimer::gpTimerMod<gpTimer::timer0_simple> timer0;

unsigned long long foo = 0;


int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz,
						 clock::configOptions::enablePIOSC::enablePinternalOsc,
						 clock::configOptions::enableMOSC::enableMainOsc);

	Gpio::enableClock(Gpio::peripheral::GPIOF);


	ledr::enableAsDigital();
	ledr::setMode(mapGpio::options::mode::gpio);
	ledr::setIOmode(mapGpio::options::IOmode::output);

	ledb::enableAsDigital();
	ledb::setMode(mapGpio::options::mode::gpio);
	ledb::setIOmode(mapGpio::options::IOmode::output);

	ledg::enableAsDigital();
	ledg::setMode(mapGpio::options::mode::gpio);
	ledg::setIOmode(mapGpio::options::IOmode::output);

	//foo = SysCtlClockGet();

	timer0::enableClock();
	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);
	timer0::config(gpTimer::subTimers::subTimerA,
				   1000,
				   gpTimer::options::timerOptions::joiningMode::timers_joinTimers,
				   gpTimer::options::timerOptions::timerMode::periodic,
				   gpTimer::options::timerOptions::countDirection::down,
				   gpTimer::options::timerOptions::interrupts::timerAtimeout);

	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);
	sysCtrl::IntEnableMaster();
	core::enableInterrupt(core::interrupts::timer0a_simple);



	while(1){

	}

}



void interruptFuncs::timer0SubA_isr()
{
	timer0::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);
	foo++;
	switch(foo%2)
	{
		case 0:
			ledr::setHigh();
			ledg::setLow();
		break;
		case 1:
			ledr::setLow();
			ledg::setHigh();
		break;
	}

}

