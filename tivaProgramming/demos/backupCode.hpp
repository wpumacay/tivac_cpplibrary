/*
 * main.cpp
 */



//nuestras libs
#include "Core.h"
#include "SysCtrl.h"
#include "Gpio.h"
#include "interrupts.hpp"
#include "clock.h"
#include "include/Gptimer.hpp"
#include "Adc.h"
#include "SPI.h"
#include "sensors.hpp"



typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio1> ledr;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio2> ledb;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio3> ledg;
typedef Gpio::gpio<mapGpio::GPIOPortE_APB,mapGpio::gpio3> analogPin;
typedef Gpio::gpio<mapGpio::GPIOPortC_APB,mapGpio::gpio6> pwm_pc6;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio4> spi0ChipSelect;
typedef Gpio::gpio<mapGpio::GPIOPortA_APB,mapGpio::gpio2> spi0Clock;
typedef Gpio::gpio<mapGpio::GPIOPortA_APB,mapGpio::gpio3> spi0FrameSignal;
typedef Gpio::gpio<mapGpio::GPIOPortA_APB,mapGpio::gpio4> spi0Rx;
typedef Gpio::gpio<mapGpio::GPIOPortA_APB,mapGpio::gpio5> spi0Tx;



typedef gpTimer::gpTimerMod<gpTimer::timer0_simple> timer0;
typedef adc::adc<adc::converter::adc0,adc::sequencer::sequencer0> analogVolt;
typedef gpTimer::pwm<gpTimer::timer1_wide> pwm1_a;

unsigned long long foo = 0;
unsigned int voltage[1] = {0};
unsigned long buffer[50];



int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz,
						 clock::configOptions::enablePIOSC::enablePinternalOsc,
						 clock::configOptions::enableMOSC::enableMainOsc);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOE);
	Gpio::enableClock(Gpio::peripheral::GPIOC);

	ledr::enableAsDigital();
	ledr::setMode(mapGpio::options::mode::gpio);
	ledr::setIOmode(mapGpio::options::IOmode::output);

	ledb::enableAsDigital();
	ledb::setMode(mapGpio::options::mode::gpio);
	ledb::setIOmode(mapGpio::options::IOmode::output);

	ledg::enableAsDigital();
	ledg::setMode(mapGpio::options::mode::gpio);
	ledg::setIOmode(mapGpio::options::IOmode::output);


	analogPin::setMode(mapGpio::options::mode::alternate);
	analogPin::disableDigitalFunc();
	analogPin::setAnalogMode();

	adc::enableClock(adc::converter::adc0);
	adc::setSampleRate(adc::converter::adc0,
					   adc::configOptions::sampleRate::sample_1msps);

	analogVolt::disableSequencer();
	analogVolt::selectTrigger(adc::configOptions::trigger::software);
	analogVolt::sampleConfigure(adc::configOptions::sample::sample_1st,
								adc::configOptions::inputpin::ain0,
								adc::configOptions::tempsensor::ommitTempSensor,
								adc::configOptions::interruptEnable::sampleIntEnable,
								adc::configOptions::endOfSequence::isEndOfSample,
								adc::configOptions::differentialMode::ommitDiffMode);
	analogVolt::enableSequencer();
	//foo = SysCtlClockGet();


	pwm_pc6::enableAsDigital();
	pwm_pc6::setMode(mapGpio::options::mode::alternate);
	pwm_pc6::setAlternateMode(mapGpio::options::altModes::alt7);
	//pwm_pc6::setPullUp();


	pwm1_a::enableClock();
	pwm1_a::config(2000,
				   0.75,
				   gpTimer::subTimers::subTimerA,
				   gpTimer::options::pwmOptions::outputLevel::normal);

	timer0::enableClock();
	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);
	timer0::config(gpTimer::subTimers::subTimerA,
				   1,
				   gpTimer::options::timerOptions::joiningMode::timers_joinTimers,
				   gpTimer::options::timerOptions::timerMode::periodic,
				   gpTimer::options::timerOptions::countDirection::down,
				   gpTimer::options::timerOptions::interrupts::timerAtimeout);

	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);
	sysCtrl::IntEnableMaster();
	core::enableInterrupt(core::interrupts::timer0a_simple);



	while(1){

	}

}



void interruptFuncs::timer0SubA_isr()
{
	timer0::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);
	foo++;
	switch(foo%2)
	{
		case 0:
			ledr::setHigh();
			ledg::setLow();
		break;
		case 1:
			ledr::setLow();
			ledg::setHigh();
		break;
	}
	analogVolt::clearInterrupt();
	analogVolt::softBeginSampling();
	while(!analogVolt::finishedSampling())
	{
	}
	analogVolt::takeData(voltage);//value in the range [0-4096] <> [0.0 - 3.3V]
	buffer[foo%50] = voltage[0];
}

