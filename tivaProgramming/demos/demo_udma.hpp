/*
 * demo_udma.hpp
 *
 *  Created on: Sep 10, 2013
 *      Author: wilbert
 */

/*
 * main.cpp
 */



//nuestras libs

#include "interrupts.hpp"
#include "include/clock.hpp"
#include "include/Core.hpp"
#include "include/Gpio.hpp"
#include "include/Gptimer.hpp"
#include "include/Adc.h"
#include "include/Uart.h"
#include "uDMA.h"

typedef Gpio::gpio<Gpio::Port::GPIOPortF_APB,Gpio::Pin::pin1> ledr;
typedef Gpio::gpio<Gpio::Port::GPIOPortF_APB,Gpio::Pin::pin2> ledb;
typedef Gpio::gpio<Gpio::Port::GPIOPortF_APB,Gpio::Pin::pin3> ledg;
typedef Gpio::gpio<Gpio::Port::GPIOPortA_APB,Gpio::Pin::pin0> u0rx;
typedef Gpio::gpio<Gpio::Port::GPIOPortA_APB,Gpio::Pin::pin1> u0tx;

typedef gpTimer::gpTimerMod<gpTimer::timer0_simple> timer0;
typedef uart::uartMod<uart::module::uart0> port;
typedef uDMA::uDMA<uDMA::channels::channel9> dmachn9;
typedef uDMA::uDMA<uDMA::channels::channel8> dmachn8;

unsigned long long foo = 0;
unsigned int count = 0;
unsigned char bufferTx[14] = {'o','k',':',' ','0',' ','j','a','j','a','j','a','\r','\n'};
char digits[10] = {'0','1','2','3','4','5','6','7','8','9'};
unsigned char buffRx[1] = {0};
union funkyVal
{
	unsigned int foo1;
	unsigned char foo2[4];
}val;
u32 fun = 0;


int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz,
						 clock::configOptions::enablePIOSC::enablePinternalOsc,
						 clock::configOptions::enableMOSC::enableMainOsc);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOA);

	ledr::enableAsDigital();
	ledr::setMode(Gpio::options::mode::gpio);
	ledr::setIOmode(Gpio::options::IOmode::output);

	ledb::enableAsDigital();
	ledb::setMode(Gpio::options::mode::gpio);
	ledb::setIOmode(Gpio::options::IOmode::output);

	ledg::enableAsDigital();
	ledg::setMode(Gpio::options::mode::gpio);
	ledg::setIOmode(Gpio::options::IOmode::output);

	u0rx::enableAsDigital();
	u0rx::setMode(Gpio::options::mode::alternate);
	u0rx::setAlternateMode(Gpio::options::altModes::alt1);

	u0tx::enableAsDigital();
	u0tx::setMode(Gpio::options::mode::alternate);
	u0tx::setAlternateMode(Gpio::options::altModes::alt1);


	port::enableClock();
	port::configUart(uart::configOptions::baudrate::baud_3000000,
					 uart::configOptions::nbits::bits_8,
					 uart::configOptions::parity::none,
					 uart::configOptions::stopbits::stopBits_one,
					 0,
					 uart::configOptions::fifo::fifoEnabled,
					 uart::clockSource::systemClock);
/*
	port::enableDMA(//uart::configOptions::dma::dmaTxEnabled|
					uart::configOptions::dma::dmaRxEnabled);
*/
	port::enableDMA(uart::configOptions::dma::dmaTxEnabled|
					uart::configOptions::dma::dmaRxEnabled);

	foo = std::strlen("hola xD!!!");

	foo = sizeof(bufferTx);
	foo = sizeof(unsigned char);
	foo = sizeof(unsigned short);
	foo = sizeof(unsigned int);
	foo = sizeof(u32);
	foo = sizeof(buffRx);
	val.foo1 = 0x20000301;

	uDMA::enableClock();

	dmachn9::disableChannel();
	dmachn9::config(&bufferTx,
					&UART0_REGS->UARTDR,
					sizeof(bufferTx),
					0,
					uDMA::configOptions::typeTransfer::memory2peripheral);

	dmachn8::disableChannel();
	dmachn8::config(&buffRx,
					&UART0_REGS->UARTDR,
					sizeof(buffRx),
					0,
					uDMA::configOptions::typeTransfer::peripheral2memory);
	dmachn8::startTransfer();

	timer0::enableClock();
	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);
	timer0::config(gpTimer::subTimers::subTimerA,
				   1000,
				   gpTimer::options::timerOptions::joiningMode::timers_joinTimers,
				   gpTimer::options::timerOptions::timerMode::periodic,
				   gpTimer::options::timerOptions::countDirection::down,
				   gpTimer::options::timerOptions::interrupts::timerAtimeout);

	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);
	core::IntEnableMaster();
	core::enableInterrupt(core::interrupts::timer0a_simple);
	core::enableInterrupt(core::interrupts::uart0);

	while(1)
	{

	}
}

void interruptFuncs::uart0rxtx_isr()
{
	if(dmachn8::isTransferCompleted())
	{
		port::clearInterrupts(uart::configOptions::interrupts::receiveInt);
		dmachn8::clearFlag();
		bufferTx[4] = buffRx[0];
		if(buffRx[0] == 'a')
		{
			ledg::setHigh();
		}
		else if(buffRx[0] == 'b')
		{
			ledg::setLow();
		}
		dmachn8::config(&buffRx,
						&UART0_REGS->UARTDR,
						sizeof(buffRx),
						0,
						uDMA::configOptions::typeTransfer::peripheral2memory);
		dmachn8::startTransfer();
		dmachn9::config(&bufferTx,
						&UART0_REGS->UARTDR,
						sizeof(bufferTx),
						0,
						uDMA::configOptions::typeTransfer::memory2peripheral);
		dmachn9::startTransfer();
	}
	else if(dmachn9::isTransferCompleted())
	{
		port::clearInterrupts(uart::configOptions::interrupts::transmitInt);
		dmachn9::clearFlag();
	}
	//dmachn8::startTransfer();
	/*
	fun = UART0_REGS->UARTRIS;
	fun = fun & uart::configOptions::interrupts::receiveInt;
	if(port::isInterruptAsserted(uart::
				configOptions::interrupts::transmitInt))
	{
		port::clearInterrupts(uart::
				configOptions::interrupts::transmitInt);
	}
	*/
	/*
	if(port::isInterruptAsserted(uart::
				configOptions::interrupts::receiveInt))
	{
		port::clearInterrupts(uart::
				configOptions::interrupts::receiveInt);

		buffRx = port::receiveByte();
		//dmachn8::startTransfer();
		ledg::toogle();
		bufferTx[4] = buffRx;
		/*
		bufferTx[4] = buffRx;
		dmachn8::config(&buffRx,
						&UART0_REGS->UARTDR,
						sizeof(buffRx),
						0,
						uDMA::configOptions::typeTransfer::peripheral2memory);

		//dmachn8::startTransfer();
	}*/
	//port::clearInterrupts(uart::configOptions::interrupts::receiveInt);

	//count = port::sendString("hola xD!!!\n\r");
	//ledg::toogle();
}

void interruptFuncs::timer0SubA_isr()
{
	timer0::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);
	foo++;
	//bufferTx[4] = (unsigned char)(48 + foo%10);
	/*
	dmachn9::config(&bufferTx,
					&UART0_REGS->UARTDR,
					sizeof(bufferTx),
					0,
					uDMA::configOptions::typeTransfer::memory2peripheral);
	dmachn9::startTransfer();
	*/
	ledb::toogle();
}
