/*
 * main.cpp
 */
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/timer.h"


#include "interrupts.hpp"
#include "Gpio.h"
#include "Adc.h"
#include "include/Gptimer.hpp"
#include "SysCtrl.h"
#include "Core.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"

#define PART_LM4F120H5QR

typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio1> led1;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio2> led2;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio3> led3;
typedef Gpio::gpio<mapGpio::GPIOPortC_APB,mapGpio::gpio6> pwm_pc6;
typedef Gpio::gpio<mapGpio::GPIOPortE_APB,mapGpio::gpio3> analogPin;

typedef adc::adc<adc::converter::adc0,adc::sequencer::sequencer0> analogVolt;
typedef gpTimer::timer<gpTimer::timer0_simple,
					   gpTimer::options::operationMode::oneshot_or_periodic> timer0;

typedef gpTimer::timer<gpTimer::timer1_wide,
					   gpTimer::options::operationMode::pwmMode> pwm1_a;

unsigned int count = 0;
unsigned int foo = 0;
unsigned int voltage[1]={0};
unsigned int buffer[50];

int main(void) {
	for(foo=0;foo<50;foo++)
	{
		buffer[foo] = 0;
	}

	SysCtlClockSet(SYSCTL_SYSDIV_2_5|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);


	//enable the gpio port related to the rgb leds
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	//enable the gpio port related to the t1ccp0 pin
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
	//enable the gpio port related to adc pins
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	//RGB LEDS configuration******************************************************************
	led1::setIOmode(mapGpio::options::IOmode::output);
	led2::setIOmode(mapGpio::options::IOmode::output);
	led3::setIOmode(mapGpio::options::IOmode::output);

	led1::enableAsDigital();
	led2::enableAsDigital();
	led3::enableAsDigital();
	//****************************************************************************************
	//ADC configuration***********************************************************************
	//enable the adc clock to adc0
	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	SysCtlADCSpeedSet(SYSCTL_ADCSPEED_250KSPS);
	analogPin::setMode(mapGpio::options::mode::alternate);
	analogPin::disableDigitalFunc();
	analogPin::setAnalogMode();

	analogVolt::disableSequencer();
	analogVolt::selectTrigger(adc::configOptions::trigger::software);
	analogVolt::sampleConfigure(adc::configOptions::sample::sample_1st,
								adc::configOptions::inputpin::ain0,
								adc::configOptions::tempsensor::ommitTempSensor,
								adc::configOptions::interruptEnable::sampleIntEnable,
								adc::configOptions::endOfSequence::isEndOfSample,
								adc::configOptions::differentialMode::ommitDiffMode);
	analogVolt::enableSequencer();
	//****************************************************************************************
	//PW1_A PIN pb4 configuration
	//GPIOPinTypePWM(GPIO_PORTB_BASE,GPIO_PIN_4);
	//GPIOPinConfigure(0x00011007);//0x00011007
	//GPIOPinTypeTimer(GPIO_PORTB_BASE,GPIO_PIN_4);
	pwm_pc6::enableAsDigital();
	pwm_pc6::setMode(mapGpio::options::mode::alternate);
	pwm_pc6::setAlternateMode(mapGpio::options::altModes::alt7);
	pwm_pc6::setPullUp();
	//****************************************************************************************
	//PWM1_A configuration
	SysCtlPeripheralEnable(SYSCTL_PERIPH_WTIMER1);
	pwm1_a::config(20,
				   0.5,
				   gpTimer::subTimers::subTimerA,
				   gpTimer::pwmOptions::outputLevel::normal);
	//****************************************************************************************
	//TIMER0 configuration********************************************************************
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
	foo = timer0::configTimer(gpTimer::subTimers::subTimerA,
						      1,
						      gpTimer::options::joiningMode::timers_joinTimers,
						      gpTimer::options::timerMode::periodic,
						      gpTimer::options::countDirection::down,
						      gpTimer::options::interrupts::timerAtimeout);

	foo = sizeof(unsigned int);
	foo = sizeof(unsigned short);
	foo = sizeof(unsigned char);
	//INTERRUPTS configuration****************************************************************
	core::enableInterrupt(core::interrupts::timer0a_simple);
	foo = sysCtrl::IntEnableMaster();
	//****************************************************************************************
	while(1)
	{

	}
}


void interruptFuncs::timer0SubA_isr()
{
	TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
	count++;
	switch(count%3)
	{
		case 0:
			led1::setHigh();
			led2::setLow();
			led3::setLow();
			break;
		case 1:
			led2::setHigh();
			led1::setLow();
			led3::setLow();
			break;
		case 2:
			led3::setHigh();
			led2::setLow();
			led1::setLow();
			break;
	}

	analogVolt::clearInterrupt();
	analogVolt::softBeginSampling();
	while(!analogVolt::finishedSampling())
	{
	}
	analogVolt::takeData(voltage);//value in the range [0-4096] <> [0.0 - 3.3V]
	buffer[count%50] = voltage[0];
}
