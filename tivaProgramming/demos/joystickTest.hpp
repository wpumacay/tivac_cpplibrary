
#include "driverlib/sysctl.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/timer.h"


#include "Adc.h"
#include "Gpio.h"
#include "include/Gptimer.hpp"
#include "Core.h"
#include "SysCtrl.h"
#include "interrupts.hpp"

typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio1> led1;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio2> led2;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio3> led3;

typedef Gpio::gpio<mapGpio::GPIOPortE_APB,mapGpio::gpio3> joystick_x;
typedef Gpio::gpio<mapGpio::GPIOPortE_APB,mapGpio::gpio2> joystick_y;

typedef adc::adc<adc::converter::adc0,adc::sequencer::sequencer0> adc0Seq0;
typedef gpTimer::timer<gpTimer::timer0_simple,
					   gpTimer::options::operationMode::oneshot_or_periodic>
					   timer0;


unsigned int buffer[2] = {0,0};
//unsigned int buffer[1] = {0};

void configClocks()
{

}



void configADC()
{

}


void configGPIO()
{

}

void configTimer()
{

}

int main()
{
	SysCtlClockSet(SYSCTL_SYSDIV_2_5|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);

	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	SysCtlADCSpeedSet(SYSCTL_ADCSPEED_250KSPS);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);

	led1::enableAsDigital();
	led1::setMode(mapGpio::options::mode::gpio);
	led1::setIOmode(mapGpio::options::IOmode::output);

	led2::enableAsDigital();
	led2::setMode(mapGpio::options::mode::gpio);
	led2::setIOmode(mapGpio::options::IOmode::output);

	led3::enableAsDigital();
	led3::setMode(mapGpio::options::mode::gpio);
	led3::setIOmode(mapGpio::options::IOmode::output);

	joystick_x::setMode(mapGpio::options::mode::alternate);
	joystick_x::disableDigitalFunc();
	joystick_x::setAnalogMode();

	joystick_y::setMode(mapGpio::options::mode::alternate);
	joystick_y::disableDigitalFunc();
	joystick_y::setAnalogMode();

	//adc configuration**************************************************************************

	adc0Seq0::disableSequencer();
	adc0Seq0::selectTrigger(adc::configOptions::trigger::software);
	adc0Seq0::sampleConfigure(adc::configOptions::sample::sample_1st,
							  adc::configOptions::inputpin::ain0,
							  adc::configOptions::tempsensor::ommitTempSensor,
							  adc::configOptions::interruptEnable::sampleIntDisabled,
							  adc::configOptions::endOfSequence::isntEndOfSample,
							  adc::configOptions::differentialMode::ommitDiffMode);
	adc0Seq0::sampleConfigure(adc::configOptions::sample::sample_2nd,
							  adc::configOptions::inputpin::ain1,
							  adc::configOptions::tempsensor::ommitTempSensor,
							  adc::configOptions::interruptEnable::sampleIntEnable,
							  adc::configOptions::endOfSequence::isEndOfSample,
							  adc::configOptions::differentialMode::selectDiffMode);
	adc0Seq0::enableSequencer();
	//end of adc configuration*******************************************************************


	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);

	timer0::configTimer(gpTimer::subTimers::subTimerA,
						1000,
						gpTimer::options::joiningMode::timers_joinTimers,
						gpTimer::options::timerMode::periodic,
						gpTimer::options::countDirection::down,
						gpTimer::options::interrupts::timerAtimeout);

	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);
	//configClocks();
	//configGPIO();
	//configADC();
	//configTimer();
	core::enableInterrupt(core::interrupts::timer0a_simple);
	sysCtrl::IntEnableMaster();
	while(1){

	}

}





void interruptFuncs::timer0SubA_isr()
{
//	timer0::clearInterrupt(gpTimer::options::interrupts::timerAtimeout);
	TimerIntClear(TIMER0_BASE,TIMER_TIMA_TIMEOUT);
	adc0Seq0::clearInterrupt();
	adc0Seq0::softBeginSampling();
	while(!adc0Seq0::finishedSampling())
	{
	}
	adc0Seq0::takeData(buffer);//value in the range [0-4096] <> [0.0 - 3.3V]
}

