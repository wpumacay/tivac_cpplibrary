//*****************************************************************************
//
// startup_ccs.c - Startup code for use with TI's Code Composer Studio.
//
// Copyright (c) 2012 Texas Instruments Incorporated.  All rights reserved.
// Software License Agreement
// 
// Texas Instruments (TI) is supplying this software for use solely and
// exclusively on TI's microcontroller products. The software is owned by
// TI and/or its suppliers, and is protected under applicable copyright
// laws. You may not combine this software with "viral" open-source
// software in order to form a larger program.
// 
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY
// CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
// DAMAGES, FOR ANY REASON WHATSOEVER.
// 
// This is part of revision 9453 of the EK-LM4F120XL Firmware Package.
//
//*****************************************************************************
#include "interrupts.hpp"


//*****************************************************************************
//
// Forward declaration of the default fault handlers.
//
//*****************************************************************************
void ResetISR(void);
static void NmiSR(void);
static void FaultISR(void);
static void IntDefaultHandler(void);

//*****************************************************************************
//
// External declaration for the reset handler that is to be called when the
// processor is started
//
//*****************************************************************************
extern void _c_int00(void);

//*****************************************************************************
//
// Linker variable that marks the top of the stack.
//
//*****************************************************************************
extern unsigned long __STACK_TOP;

//*****************************************************************************
//
// External declarations for the interrupt handlers used by the application.
//
//*****************************************************************************
extern void IntGPIOa(void);
extern void IntGPIOb(void);
extern void IntGPIOc(void);

#pragma WEAK;
void interruptFuncs::reset_isr(){
    //
    // Jump to the CCS C initialization routine.  This will enable the
    // floating-point unit as well, so that does not need to be done here.
    //
    __asm("    .global _c_int00\n"
          "    b.w     _c_int00");
}

void interruptFuncs::mpuFault_isr(){

}

#pragma WEAK;
void interruptFuncs::timer0SubA_isr(){

}
//*****************************************************************************
//
// The vector table.  Note that the proper constructs must be placed on this to
// ensure that it ends up at physical address 0x0000.0000 or at the start of
// the program if located at a start address other than 0.
//
//*****************************************************************************


#pragma DATA_ALIGN(1024)
#pragma DATA_SECTION(".vtable")
void (*g_pfnRAMVectors[155])(void);


namespace interruptFuncs{

#pragma DATA_SECTION(".intvecs")
extern "C" void (* const g_pfnVectors[])(void) =
{
    (void (*)(void))((unsigned long)&__STACK_TOP),// The initial stack pointer
	#ifdef mpuLib
    (void (*)(void))reset_isr,
	#else
    (void (*)(void))ResetISR,                               // The reset handler
	#endif
    (void (*)(void))NmiSR,                                  // The NMI handler
    (void (*)(void))FaultISR,                               // The hard fault handler
	#ifdef mpuLib
    (void (*)(void))mpuFault_isr,                      // The MPU fault handler
	#else
    (void (*)(void))IntDefaultHandler,
	#endif
    (void (*)(void))IntDefaultHandler,                      // The bus fault handler
    (void (*)(void))IntDefaultHandler,                      // The usage fault handler
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))IntDefaultHandler,                      // SVCall handler
    (void (*)(void))IntDefaultHandler,                      // Debug monitor handler
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))IntDefaultHandler,                      // The PendSV handler
    (void (*)(void))IntDefaultHandler,                      // The SysTick handler
	#ifdef gpioInts
    (void (*)(void))gpioA_isr,            // GPIO Port A
	#else
    (void (*)(void))IntDefaultHandler,                      // GPIO Port A
	#endif
    (void (*)(void))IntDefaultHandler,                      // GPIO Port B
    (void (*)(void))IntDefaultHandler,                      // GPIO Port C
    (void (*)(void))IntDefaultHandler,                      // GPIO Port D
    (void (*)(void))IntDefaultHandler,                      // GPIO Port E
    (void (*)(void))uart0rxtx_isr,                      // UART0 Rx and Tx
    (void (*)(void))IntDefaultHandler,                      // UART1 Rx and Tx
    (void (*)(void))IntDefaultHandler,                      // SSI0 Rx and Tx
    (void (*)(void))IntDefaultHandler,                      // I2C0 Master and Slave
    (void (*)(void))IntDefaultHandler,                      // PWM Fault
    (void (*)(void))IntDefaultHandler,                      // PWM Generator 0
    (void (*)(void))IntDefaultHandler,                      // PWM Generator 1
    (void (*)(void))IntDefaultHandler,                      // PWM Generator 2
    (void (*)(void))IntDefaultHandler,                      // Quadrature Encoder 0
    (void (*)(void))IntDefaultHandler,                      // ADC Sequence 0
    (void (*)(void))IntDefaultHandler,                      // ADC Sequence 1
    (void (*)(void))IntDefaultHandler,                      // ADC Sequence 2
    (void (*)(void))IntDefaultHandler,                      // ADC Sequence 3
    (void (*)(void))IntDefaultHandler,                      // Watchdog timer
    (void (*)(void))timer0SubA_isr,                      // Timer 0 subtimer A
    (void (*)(void))IntDefaultHandler,                      // Timer 0 subtimer B
    (void (*)(void))timer1SubA_isr,                      // Timer 1 subtimer A
    (void (*)(void))timer1SubB_isr,                      // Timer 1 subtimer B
    (void (*)(void))IntDefaultHandler,                      // Timer 2 subtimer A
    (void (*)(void))IntDefaultHandler,                      // Timer 2 subtimer B
    (void (*)(void))IntDefaultHandler,                      // Analog Comparator 0
    (void (*)(void))IntDefaultHandler,                      // Analog Comparator 1
    (void (*)(void))IntDefaultHandler,                      // Analog Comparator 2
    (void (*)(void))IntDefaultHandler,                      // System Control (PLL, OSC, BO)
    (void (*)(void))IntDefaultHandler,                      // FLASH Control
    (void (*)(void))IntDefaultHandler,                      // GPIO Port F
    (void (*)(void))IntDefaultHandler,                      // GPIO Port G
    (void (*)(void))IntDefaultHandler,                      // GPIO Port H
    (void (*)(void))IntDefaultHandler,                      // UART2 Rx and Tx
    (void (*)(void))IntDefaultHandler,                      // SSI1 Rx and Tx
    (void (*)(void))IntDefaultHandler,                      // Timer 3 subtimer A
    (void (*)(void))IntDefaultHandler,                      // Timer 3 subtimer B
    (void (*)(void))IntDefaultHandler,                      // I2C1 Master and Slave
    (void (*)(void))IntDefaultHandler,                      // Quadrature Encoder 1
    (void (*)(void))IntDefaultHandler,                      // CAN0
    (void (*)(void))IntDefaultHandler,                      // CAN1
    (void (*)(void))IntDefaultHandler,                      // CAN2
    (void (*)(void))IntDefaultHandler,                      // Ethernet
    (void (*)(void))IntDefaultHandler,                      // Hibernate
    (void (*)(void))IntDefaultHandler,                      // USB0
    (void (*)(void))IntDefaultHandler,                      // PWM Generator 3
    (void (*)(void))IntDefaultHandler,                      // uDMA Software Transfer
    (void (*)(void))IntDefaultHandler,                      // uDMA Error
    (void (*)(void))IntDefaultHandler,                      // ADC1 Sequence 0
    (void (*)(void))IntDefaultHandler,                      // ADC1 Sequence 1
    (void (*)(void))IntDefaultHandler,                      // ADC1 Sequence 2
    (void (*)(void))IntDefaultHandler,                      // ADC1 Sequence 3
    (void (*)(void))IntDefaultHandler,                      // I2S0
    (void (*)(void))IntDefaultHandler,                      // External Bus Interface 0
    (void (*)(void))IntDefaultHandler,                      // GPIO Port J
    (void (*)(void))IntDefaultHandler,                      // GPIO Port K
    (void (*)(void))IntDefaultHandler,                      // GPIO Port L
    (void (*)(void))IntDefaultHandler,                      // SSI2 Rx and Tx
    (void (*)(void))IntDefaultHandler,                      // SSI3 Rx and Tx
    (void (*)(void))IntDefaultHandler,                      // UART3 Rx and Tx
    (void (*)(void))IntDefaultHandler,                      // UART4 Rx and Tx
    (void (*)(void))IntDefaultHandler,                      // UART5 Rx and Tx
    (void (*)(void))IntDefaultHandler,                      // UART6 Rx and Tx
    (void (*)(void))IntDefaultHandler,                      // UART7 Rx and Tx
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))IntDefaultHandler,                      // I2C2 Master and Slave
    (void (*)(void))IntDefaultHandler,                      // I2C3 Master and Slave
    (void (*)(void))IntDefaultHandler,                      // Timer 4 subtimer A
    (void (*)(void))IntDefaultHandler,                      // Timer 4 subtimer B
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))IntDefaultHandler,                      // Timer 5 subtimer A
    (void (*)(void))IntDefaultHandler,                      // Timer 5 subtimer B
    (void (*)(void))IntDefaultHandler,                      // Wide Timer 0 subtimer A
    (void (*)(void))IntDefaultHandler,                      // Wide Timer 0 subtimer B
    (void (*)(void))IntDefaultHandler,                      // Wide Timer 1 subtimer A
    (void (*)(void))IntDefaultHandler,                      // Wide Timer 1 subtimer B
    (void (*)(void))IntDefaultHandler,                      // Wide Timer 2 subtimer A
    (void (*)(void))IntDefaultHandler,                      // Wide Timer 2 subtimer B
    (void (*)(void))IntDefaultHandler,                      // Wide Timer 3 subtimer A
    (void (*)(void))IntDefaultHandler,                      // Wide Timer 3 subtimer B
    (void (*)(void))IntDefaultHandler,                      // Wide Timer 4 subtimer A
    (void (*)(void))IntDefaultHandler,                      // Wide Timer 4 subtimer B
    (void (*)(void))IntDefaultHandler,                      // Wide Timer 5 subtimer A
    (void (*)(void))IntDefaultHandler,                      // Wide Timer 5 subtimer B
    (void (*)(void))IntDefaultHandler,                      // FPU
    (void (*)(void))IntDefaultHandler,                      // PECI 0
    (void (*)(void))IntDefaultHandler,                      // LPC 0
    (void (*)(void))IntDefaultHandler,                      // I2C4 Master and Slave
    (void (*)(void))IntDefaultHandler,                      // I2C5 Master and Slave
    (void (*)(void))IntDefaultHandler,                      // GPIO Port M
    (void (*)(void))IntDefaultHandler,                      // GPIO Port N
    (void (*)(void))IntDefaultHandler,                      // Quadrature Encoder 2
    (void (*)(void))IntDefaultHandler,                      // Fan 0
    (void (*)(void))0,                                      // Reserved
    (void (*)(void))IntDefaultHandler,                      // GPIO Port P (Summary or P0)
    (void (*)(void))IntDefaultHandler,                      // GPIO Port P1
    (void (*)(void))IntDefaultHandler,                      // GPIO Port P2
    (void (*)(void))IntDefaultHandler,                      // GPIO Port P3
    (void (*)(void))IntDefaultHandler,                      // GPIO Port P4
    (void (*)(void))IntDefaultHandler,                      // GPIO Port P5
    (void (*)(void))IntDefaultHandler,                      // GPIO Port P6
    (void (*)(void))IntDefaultHandler,                      // GPIO Port P7
    (void (*)(void))IntDefaultHandler,                      // GPIO Port Q (Summary or Q0)
    (void (*)(void))IntDefaultHandler,                      // GPIO Port Q1
    (void (*)(void))IntDefaultHandler,                      // GPIO Port Q2
    (void (*)(void))IntDefaultHandler,                      // GPIO Port Q3
    (void (*)(void))IntDefaultHandler,                      // GPIO Port Q4
    (void (*)(void))IntDefaultHandler,                      // GPIO Port Q5
    (void (*)(void))IntDefaultHandler,                      // GPIO Port Q6
    (void (*)(void))IntDefaultHandler,                      // GPIO Port Q7
    (void (*)(void))IntDefaultHandler,                      // GPIO Port R
    (void (*)(void))IntDefaultHandler,                      // GPIO Port S
    (void (*)(void))IntDefaultHandler,                      // PWM 1 Generator 0
    (void (*)(void))IntDefaultHandler,                      // PWM 1 Generator 1
    (void (*)(void))IntDefaultHandler,                      // PWM 1 Generator 2
    (void (*)(void))IntDefaultHandler,                      // PWM 1 Generator 3
    (void (*)(void))IntDefaultHandler                       // PWM 1 Fault
};
}
//#pragma SET_DATA_SECTION()
//*****************************************************************************
//
// This is the code that gets called when the processor first starts execution
// following a reset event.  Only the absolutely necessary set is performed,
// after which the application supplied entry() routine is called.  Any fancy
// actions (such as making decisions based on the reset cause register, and
// resetting the bits in that register) are left solely in the hands of the
// application.
//
//*****************************************************************************
#pragma WEAK;
void interruptFuncs::uart0rxtx_isr(){}


void interruptFuncs::gpioA_isr(){}

#pragma WEAK;
void interruptFuncs::timer1SubA_isr(){}
#pragma WEAK;
void interruptFuncs::timer1SubB_isr(){}


void
ResetISR(void)
{
    //
    // Jump to the CCS C initialization routine.  This will enable the
    // floating-point unit as well, so that does not need to be done here.
    //
    __asm("    .global _c_int00\n"
          "    b.w     _c_int00");
}

//*****************************************************************************
//
// This is the code that gets called when the processor receives a NMI.  This
// simply enters an infinite loop, preserving the system state for examination
// by a debugger.
//
//*****************************************************************************
static void
NmiSR(void)
{
    //
    // Enter an infinite loop.
    //
    while(1)
    {
    }
}

//*****************************************************************************
//
// This is the code that gets called when the processor receives a fault
// interrupt.  This simply enters an infinite loop, preserving the system state
// for examination by a debugger.
//
//*****************************************************************************
static void
FaultISR(void)
{
    //
    // Enter an infinite loop.
    //
    while(1)
    {
    }
}

//*****************************************************************************
//
// This is the code that gets called when the processor receives an unexpected
// interrupt.  This simply enters an infinite loop, preserving the system state
// for examination by a debugger.
//
//*****************************************************************************
static void
IntDefaultHandler(void)
{
    //
    // Go into an infinite loop.
    //
    while(1)
    {
    }
}
