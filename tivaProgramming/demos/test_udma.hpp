/*
 * main.cpp
 */



//nuestras libs
#include "Core.h"
#include "SysCtrl.h"
#include "Gpio.h"
#include "Uart.h"
#include "interrupts.hpp"
#include "include/Gptimer.hpp"
#include "clock.h"
#include "uDMA.h"


typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio1> ledr;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio2> ledb;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio3> ledg;

typedef Gpio::gpio<mapGpio::GPIOPortA_APB,mapGpio::gpio0> u0rx;
typedef Gpio::gpio<mapGpio::GPIOPortA_APB,mapGpio::gpio1> u0tx;


typedef gpTimer::timer<gpTimer::timer0_simple,
					   gpTimer::options::operationMode::oneshot_or_periodic>
					   timer0;

typedef uart::uart<uart::module::uart0> port;
typedef uDMA::uDMA<uDMA::channels::channel9> dmachn7;

unsigned int foo = 0;

char bufferTx[14] = {'o','k',':',' ','0',' ','j','a','j','a','j','a','\r','\n'};
char digits[10] = {'0','1','2','3','4','5','6','7','8','9'};

union funkyVal
{
	unsigned int foo1;
	unsigned char foo2[4];
}val;

int main()
{
	clock::clock::config(clock::configOptions::clockSource::precisionInternalOscillator,
						 clock::configOptions::clockRate::clock_80Mhz,
						 clock::configOptions::enablePIOSC::enablePinternalOsc,
						 clock::configOptions::enableMOSC::enableMainOsc);

	ucControlTable[0] = 'a';
	ucControlTable[1] = 'c';
	ucControlTable[2] = 'd';
	ucControlTable[3] = 'e';

	//SysCtlPeripheralEnable(SYSCTL_PERIPH2_GPIOA);
	Gpio::enableClock(Gpio::peripheral::GPIOA);
	Gpio::enableClock(Gpio::peripheral::GPIOF);

	//SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
	port::enableClock();
	//SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
	timer0::enableClock();

	ledr::enableAsDigital();
	ledr::setMode(mapGpio::options::mode::gpio);
	ledr::setIOmode(mapGpio::options::IOmode::output);

	ledb::enableAsDigital();
	ledb::setMode(mapGpio::options::mode::gpio);
	ledb::setIOmode(mapGpio::options::IOmode::output);

	ledg::enableAsDigital();
	ledg::setMode(mapGpio::options::mode::gpio);
	ledg::setIOmode(mapGpio::options::IOmode::output);

	u0rx::enableAsDigital();
	u0rx::setMode(mapGpio::options::mode::alternate);
	u0rx::setAlternateMode(mapGpio::options::altModes::alt1);

	u0tx::enableAsDigital();
	u0tx::setMode(mapGpio::options::mode::alternate);
	u0tx::setAlternateMode(mapGpio::options::altModes::alt1);

	port::configUart(uart::configOptions::baudrate::baud_115200,
					 uart::configOptions::nbits::bits_8,
					 uart::configOptions::parity::none,
					 uart::configOptions::stopbits::stopBits_one,
					 uart::configOptions::interrupts::receiveInt,
					 uart::configOptions::fifo::fifoEnabled,
					 uart::clockSource::pioscClock);

	port::enableDMA(uart::configOptions::dma::dmaTxEnabled);

	foo = sizeof(bufferTx);
	foo = sizeof(unsigned char);
	foo = sizeof(unsigned short);
	foo = sizeof(unsigned int);
	foo = sizeof(u32);
	val.foo1 = 0x20000301;

	uDMA::enableClock();
	dmachn7::disableChannel();
	dmachn7::config(&bufferTx,&UART0_REGS->UARTDR,sizeof(bufferTx),0);
	//dmachn7::config((void *)(&bufferTx),(void *)(&UART0_REGS->UARTDR),sizeof(bufferTx),0);

	timer0::configTimer(gpTimer::subTimers::subTimerA,
						1000,
						gpTimer::options::joiningMode::timers_joinTimers,
						gpTimer::options::timerMode::periodic,
						gpTimer::options::countDirection::down,
						gpTimer::options::interrupts::timerAtimeout);


	sysCtrl::IntEnableMaster();
	//core::enableInterrupt(core::interrupts::uart0);
	core::enableInterrupt(core::interrupts::timer0a_simple);



	while(1){

	}

}

void interruptFuncs::uart0rxtx_isr()
{
	unsigned char var = port::receiveByte();
	ledb::toogle();

	bufferTx[4] = var;
	//dmachn7::startTransfer();


	port::sendByte('o');
	port::sendByte('k');
	port::sendByte(':');
	port::sendByte(' ');
	port::sendByte(var);
	port::sendByte(' ');
	port::sendByte('j');
	port::sendByte('a');
	port::sendByte('j');
	port::sendByte('a');
	port::sendByte('j');
	port::sendByte('a');
	port::sendByte('j');
	port::sendByte('a');
	port::sendByte('j');
	port::sendByte('a');
	port::sendByte('\n');
	port::sendByte('\r');


	/*
	UARTCharPut(UART0_BASE,'o');
	UARTCharPut(UART0_BASE,'k');
	UARTCharPut(UART0_BASE,':');
	UARTCharPut(UART0_BASE,' ');
	UARTCharPut(UART0_BASE,var);
	UARTCharPut(UART0_BASE,'\n');
	UARTCharPut(UART0_BASE,'\r');
	 */
	port::clearInterrupts(uart::configOptions::interrupts::receiveInt);
}




void interruptFuncs::timer0SubA_isr()
{
	//TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
	timer0::clearInterrupt(gpTimer::options::interrupts::timerAtimeout);
	foo++;
	bufferTx[4] = (unsigned char)foo;
	dmachn7::config(&bufferTx,&UART0_REGS->UARTDR,sizeof(bufferTx),0);
	dmachn7::startTransfer();
	switch(foo%2)
	{
		case 0:
			ledr::setHigh();
			ledg::setLow();
		break;
		case 1:
			ledr::setLow();
			ledg::setHigh();
		break;
	}

}

