/*
 * main.cpp
 */
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_i2c.h"
#include "inc/hw_ints.h"
#include "driverlib/i2c.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"

#include "driverlib/timer.h"
#include "globalPrototypes.h"
#include "interrupts.hpp"
#include "driverlib/adc.h"
#include "driverlib/uart.h"
#include "Gpio.h"
#include "include/Gptimer.hpp"
#include "I2C.h"
#include "Adc.h"
#include "SysCtrl.h"
#include "Core.h"


typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio1> led1;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio2> led2;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio3> led3;
typedef Gpio::gpio<mapGpio::GPIOPortB_APB,mapGpio::gpio2> i2c0scl;
typedef Gpio::gpio<mapGpio::GPIOPortB_APB,mapGpio::gpio3> i2c0sda;
typedef Gpio::gpio<mapGpio::GPIOPortE_APB,mapGpio::gpio3> analogPin;

//typedef gpTimer::timer<mapTimer::timer0_simple> timer0;
typedef gpTimer::timer<gpTimer::timer0_simple,
					   gpTimer::options::operationMode::oneshot_or_periodic> timer0;
typedef i2c::i2c<i2c::module::i2c0> i2c0;
typedef adc::adc<adc::converter::adc0,adc::sequencer::sequencer0> analogVolt;

unsigned int count = 0;
unsigned int foo = 0;

unsigned char buffDataI2C[2] = {0x0f,0xf0};
unsigned int voltage[4]={0,0,0,0};

union value
{
	unsigned short cValue;
	unsigned char buffData[2];
}val;





int main(void) {

	SysCtlClockSet(SYSCTL_SYSDIV_2_5|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);

	//enable the gpio port related to i2c0 pins
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	//enable the gpio port related to the rgb leds
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	//enable the gpio port related to adc pins
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);


	//RGB LEDS configuration******************************************************************
	led1::setIOmode(mapGpio::options::IOmode::output);
	led2::setIOmode(mapGpio::options::IOmode::output);
	led3::setIOmode(mapGpio::options::IOmode::output);

	led1::enableAsDigital();
	led2::enableAsDigital();
	led3::enableAsDigital();
	//****************************************************************************************
	//I2C configuration***********************************************************************
	//enable the i2c clock to i2c0
	SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);
	i2c0scl::enableAsDigital();
	i2c0scl::setMode(mapGpio::options::mode::alternate);
	i2c0scl::setAlternateMode(mapGpio::options::altModes::alt3);
	i2c0scl::setPullUp();

	i2c0sda::enableAsDigital();
	i2c0sda::setMode(mapGpio::options::mode::alternate);
	i2c0sda::setAlternateMode(mapGpio::options::altModes::alt3);
	i2c0sda::setOpenDrain();

	i2c0::config(i2c::configOptions::mode::master,
				 i2c::configOptions::loopback::disabled,
				 i2c::configOptions::speed::standarMode);
	//****************************************************************************************
	//ADC configuration***********************************************************************
	//enable the adc clock to adc0
	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	SysCtlADCSpeedSet(SYSCTL_ADCSPEED_250KSPS);
	analogPin::setMode(mapGpio::options::mode::alternate);
	analogPin::disableDigitalFunc();
	analogPin::setAnalogMode();

	analogVolt::disableSequencer();
	analogVolt::selectTrigger(adc::configOptions::trigger::software);
	analogVolt::sampleConfigure(adc::configOptions::sample::sample_1st,
								adc::configOptions::inputpin::ain0,
								adc::configOptions::tempsensor::ommitTempSensor,
								adc::configOptions::interruptEnable::sampleIntDisabled,
								adc::configOptions::endOfSequence::isntEndOfSample,
								adc::configOptions::differentialMode::ommitDiffMode);
	analogVolt::sampleConfigure(adc::configOptions::sample::sample_2nd,
								adc::configOptions::inputpin::ain1,
								adc::configOptions::tempsensor::ommitTempSensor,
								adc::configOptions::interruptEnable::sampleIntEnable,
								adc::configOptions::endOfSequence::isEndOfSample,
								adc::configOptions::differentialMode::ommitDiffMode);
	analogVolt::enableSequencer();
	//TIMER0 configuration********************************************************************
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
	timer0::configTimer(gpTimer::subTimers::subTimerA,
						100,
						gpTimer::options::joiningMode::timers_joinTimers,
						gpTimer::options::timerMode::periodic,
						gpTimer::options::countDirection::down,
						gpTimer::options::interrupts::timerAtimeout);

	val.cValue = 0;
	//INTERRUPTS configuration****************************************************************
	//IntEnable(INT_TIMER0A);
	core::enableInterrupt(core::interrupts::timer0a_simple);
	//IntMasterEnable();
	foo = sysCtrl::IntEnableMaster();
	//****************************************************************************************
	while(1)
	{

	}
}


void interruptFuncs::timer0SubA_isr()
{
	TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
	count++;
	val.cValue+=10;
	switch(count%3)
	{
		case 0:
			led1::setHigh();
			led2::setLow();
			led3::setLow();
			break;
		case 1:
			led2::setHigh();
			led1::setLow();
			led3::setLow();
			break;
		case 2:
			led3::setHigh();
			led2::setLow();
			led1::setLow();
			break;
	}

	if(val.cValue>4096)
	{
		val.cValue = 0;
	}
	//buffDataI2C[0] = val.buffData[1];
	//buffDataI2C[1] = val.buffData[0];
	i2c0::send(0x60,buffDataI2C,2);
	analogVolt::clearInterrupt();
	analogVolt::softBeginSampling();
	while(!analogVolt::finishedSampling())
	{
	}
	analogVolt::takeData(buffer);//value in the range [0-4096] <> [0.0 - 3.3V]
}
