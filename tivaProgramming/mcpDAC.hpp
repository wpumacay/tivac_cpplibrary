/*
 * mcpDAC.hpp
 *
 *  Created on: 24/09/2013
 *      Author: wilbert
 */

#include "include/I2C.h"

namespace mcp4725
{

	template<i2c::module::_module module_>
	class dac
	{
	public:
		static inline u16 setVoltage(double voltage,double vmax)
		{
			u8 buff[2];
			u16 volt;
			if(voltage>=vmax)
			{
				voltage = vmax;
			}
			volt = (u16)((voltage/vmax)*4095.0);
			buff[0] = (u8)((volt>>8)&(0x000f));
			buff[1] = (u8)((volt>>0)&(0x00ff));
			i2c::i2cMod<module_>::loadSlaveAddress(0x60,i2c::options::rw::write);
			i2c::i2cMod<module_>::loadDataRegister(buff[0]);
			i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::runMasterEnable|
											   i2c::configOptions::operation::startCondition);
			while(i2c::i2cMod<module_>::isBusBusy());
			if(i2c::i2cMod<module_>::isThereAnError())
			{
				while(1);
			}
			i2c::i2cMod<module_>::loadDataRegister(buff[1]);
			i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::runMasterEnable|
											   i2c::configOptions::operation::stopCondition);
			while(i2c::i2cMod<module_>::isBusBusy());
			if(i2c::i2cMod<module_>::isThereAnError())
			{
				while(1);
			}
			return volt;
		}

	};


}
