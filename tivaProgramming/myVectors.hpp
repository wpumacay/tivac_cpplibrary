/*
 * myVectors.hpp
 *
 *  Created on: 15/09/2013
 *      Author: wilbert
 */

#ifndef MYVECTORS_HPP_
#define MYVECTORS_HPP_

namespace myVectors
{


	class vector
	{
		public:

			float *v;
			unsigned int length;
			vector(unsigned int N)
			{
				this->v 		= new float[N];
				this->length 	= N;
			}

	};

	vector add(vector v1,vector v2)
	{
		vector v3(v1.length);
		unsigned int i;
		for(i=0;i<v1.length;i++)
		{
			*(v3.v) = *(v1.v) + *(v2.v);
			v1.v++;
			v2.v++;
			v3.v++;
		}
		return v3;
	}

}





#endif /* MYVECTORS_HPP_ */
