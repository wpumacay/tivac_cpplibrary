/*
 * linalg.hpp
 *
 *  Created on: Sep 23, 2013
 *      Author: wilbert
 */
#pragma once

#include "common.h"


namespace linalg
{

	class vector
	{
	public:
		double *buff;
		u8 dim;

		vector(u8 dim);
		vector operator + (vector);
		vector operator * (vector);
		~vector();
	};

	vector::vector(u8 dim)
	{
		u8 i;
		this->dim = dim;
		this->buff = new double[dim];
		for(i=0;i<dim;i++)
		{
			*(this->buff + i) = 0;
		}
	}
	vector::~vector()
	{
		delete [] this->buff;
	}
	vector vector::operator +(vector v)
	{
		vector dummyVector(v.dim);
		if(this->dim == v.dim)
		{
			u8 i = 0;
			for(i=0;i<this->dim;i++)
			{
				*(dummyVector.buff+i) = *(this->buff + i) + *(v.buff + i);
			}
			return dummyVector;
		}
		while(1);//an errr ocurred!!!... vectors' dimensions don't match
	}
	vector vector::operator *(vector v)
	{
		vector dummyVector(v.dim);
		if(this->dim == v.dim)
		{
			u8 i = 0;
			for(i=0;i<this->dim;i++)
			{
				*(dummyVector.buff+i) = *(this->buff + i) * *(v.buff + i);
			}
			return dummyVector;
		}
		while(1);//an errr ocurred!!!... vectors' dimensions don't match
	}

	class matrix
	{
	//private:
		//double *buff;
		//u8 nRows;
		//u8 nCols;
	public:
		double *buff;
		u8 nRows;
		u8 nCols;

		matrix(u8 nrows,u8 ncols);
		matrix()
		{

		}
		matrix(const matrix& toCopy)
		{
			nRows = toCopy.nRows;
			nCols = toCopy.nCols;
			buff = new double[nRows*nCols];
			u8 i,j;
			for(i=0;i<nRows;i++)
			{
				for(j=0;j<nCols;j++)
				{
					*(buff + i*nCols + j) = *(toCopy.buff + i*nCols + j);
				}
			}
		}
		void setValue(u8 row,u8 col,double val)
		{
			*(buff + row*nCols + col) = val;
			if(row>=nRows || col >=nCols)
			{
				while(1);
			}
		}
		matrix& operator=(const matrix& source)
		{
			nRows = source.nRows;
			nCols = source.nCols;
			u8 i,j;
			delete[] buff;
			buff = new double[nRows*nCols];
			for(i=0;i<nRows;i++)
			{
				for(j=0;j<nCols;j++)
				{
					*(buff + i*nCols + j) = *(source.buff + i*nCols + j);
				}
			}
			return *this;
		}

		double getElement(u8 row,u8 col)
		{
			if(row>=nRows || col >=nCols)
			{
				while(1);
			}
			return *(this->buff + row*this->nCols + col);
		}
		matrix operator + (matrix m);
		matrix operator * (matrix m);
		matrix operator - (matrix m);
		matrix inv();
		matrix trsp();
		~matrix();
	};
	matrix::matrix(u8 nrows,u8 ncols)
	{
		u8 i,j;
		nRows = nrows;
		nCols = ncols;
		buff = new double[nrows*ncols];
		for(i=0;i<nrows;i++)
		{
			for(j=0;j<ncols;j++)
			{
				*(buff + i*ncols + j) = 0.0;
			}
		}
	}
	matrix::~matrix()
	{
		delete[] this->buff;
		//delete this->buff;
	}
	matrix matrix::operator +(matrix m)
	{
		matrix dummyMatrix(m.nRows,m.nCols);
		if(nRows == m.nRows && nCols == m.nCols)
		{
			u8 i,j;
			for(i=0;i<nRows;i++)
			{
				for(j=0;j<nCols;j++)
				{
					*(dummyMatrix.buff+i*dummyMatrix.nCols + j) = *(buff+i*m.nCols + j) + *(m.buff+i*m.nCols + j);
				}
			}
			return dummyMatrix;
		}
		while(1);//an errr ocurred!!!... vectors' dimensions don't match
	}
	matrix matrix::operator -(matrix m)
	{
		matrix dummyMatrix(m.nRows,m.nCols);
		if(nRows == m.nRows && nCols == m.nCols)
		{
			u8 i,j;
			for(i=0;i<nRows;i++)
			{
				for(j=0;j<nCols;j++)
				{
					*(dummyMatrix.buff+i*dummyMatrix.nCols + j) = *(buff+i*nCols + j) - *(m.buff+i*m.nCols + j);
				}
			}
			return dummyMatrix;
		}
		while(1);//an errr ocurred!!!... vectors' dimensions don't match
	}
	/*
	 * 		1	2	3 -> 7	8
	 * 		4	5	6 -> 9	10
	 * 					 11	12
	 */

	matrix matrix::operator *(matrix m)
	{
		matrix dummyMatrix(nRows,m.nCols);
		if(nCols == m.nRows)
		{
			u8 i,j,k;
			for(i=0;i<nRows;i++)
			{
				for(j=0;j<m.nCols;j++)
				{
					double element = 0.0;
					for(k=0;k<nCols;k++)
					{
						element += *(buff + i*nCols + k) * *(m.buff + k*m.nCols + j);
					}
					*(dummyMatrix.buff + i*m.nCols + j) = element;
				}
			}
		}
		return dummyMatrix;
	}

	matrix identity(u8 dim)
	{
		matrix dummyMatrix(dim,dim);
		//TODO HERE:
		u8 i;
		for(i=0;i<dim;i++)
		{
			*(dummyMatrix.buff + i*dim + i) = 1.0;
		}
		return dummyMatrix;
	}

	matrix matrix::inv()
	{
		//matrix dummyMatrix(this->nRows,this->nCols);
		//matrix B(this->nRows,this->nCols);
		matrix B(nRows,nCols);
		B = identity(nRows);
		//TODO HERE:
		//First method:matrix inversion using naive gaussian elimination
		if(nRows == nCols)
		{
			//forward elimination
			u8 n = nRows;
			u8 i,j,k,q,m;
			bool thereIsASolution = true;
			//matrix B(n,n);
			//B = identity(n);
			for(i=0;i<n-1;i++)
			{
				double a = *(buff + i*n + i);
				bool found = true;
				if(a == 0.0)
				{
					found = false;
					for(q=0;q<(n-i-1);q++)
					{
						if(*(buff + (i+q+1)*n + i) != 0.0)
						{
							found = true;
							for(k=0;k<n;k++)
							{
								double temp = *(buff + (i+q+1)*n + k);
								*(buff + (i+q+1)*n + k) = *(buff + i*n + k);
								*(buff + i*n + k) = temp;
								temp = *(B.buff + (i+q+1)*n + k);
								*(B.buff + (i+q+1)*n + k) = *(B.buff + i*n + k);
								*(B.buff + i*n + k) = temp;
							}
							a = *(buff + i*n + i);
							break;
						}
					}
				}
				if(found == false)
				{
					thereIsASolution = false;
					while(1);
					//break;
				}
				for(j=0;j<(n-i-1);j++)
				{
					double l = *(buff + (i+j+1)*n + i) / a;
					for(m=0;m<n;m++)
					{
						*(buff + (i+j+1)*n + m) -= l*(*(buff + i*n + m));
						*(B.buff + (i+j+1)*n + m) -= l*(*(B.buff + i*n + m));
					}
				}
			}
			if(thereIsASolution == true)
			{
				for(i=0;i<n-1;i++)
				{
					double a = *(buff + (n-i-1)*n + (n-i-1));
					if(a==0.0)
					{
						thereIsASolution = false;
						while(1);
						//break;
					}
					for(j=0;j<(n-i-1);j++)
					{
						double l = (*(buff + (n-i-1-j-1)*n + (n-i-1)))/a;
						for(m=0;m<n;m++)
						{
							*(buff + (n-i-1-j-1)*n + m) -= l*(*(buff + (n-i-1)*n + m));
							*(B.buff + (n-i-1-j-1)*n + m) -= l*(*(B.buff + (n-i-1)*n + m));
						}

					}
				}
				if(thereIsASolution == true)
				{
					for(i=0;i<n;i++)
					{
						double a = *(buff + i*n + i);
						for(k=0;k<n;k++)
						{
							*(buff + i*n + k) = *(buff + i*n + k) / a;
							*(B.buff + i*n + k) = *(B.buff + i*n + k) / a;
						}
					}
				}
			}
			//return B;
		}
		return B;
		//while(1);//check the error
	}
	matrix matrix::trsp()
	{
		matrix dummyMatrix(nCols,nRows);
		//TODO HERE:
		u8 i,j;
		for(i=0;i<nRows;i++)
		{
			for(j=0;j<nCols;j++)
			{
				*(dummyMatrix.buff + j*dummyMatrix.nCols + i) = *(buff + i*nCols + j);
				/*

				double temp = *(this->buff + i*this->nCols + j);
				*(this->buff + i*this->nCols + j) = *(this->buff + j*this->nRows + i);
				*(this->buff + j*this->nRows + i) = temp;

				*/
			}
		}
		return dummyMatrix;
	}


	void checkMatrix(matrix m,double *theBuff)
	{
		u8 i,j;
		for(i=0;i<m.nRows;i++)
		{
			for(j=0;j<m.nCols;j++)
			{
				*theBuff = *(m.buff + i*m.nCols + j);
				theBuff++;
			}
		}
	}
}
