/*
 * tutorial4.hpp
 *
 *  Created on: 25/01/2014
 *      Author: wilbert
 */

// Incluyendo librer�as a usar...

#include "include/clock.hpp"// -> clock library
#include "include/Gpio.hpp"// -> gpio library
#include "include/Uart.h"// -> uart library
#include "delays.hpp"
#include "include/Core.hpp"// -> core library-> for interrupts... !!!
#include "interrupts.hpp"// -> interrupt interrupt service routines functions already mapped...

u8 buffRx = 0;

int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
								 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOA);

	// Configuring the GPIO
	PF1::enableAsDigital();
	PF1::setMode(Gpio::options::mode::gpio);
	PF1::setIOmode(Gpio::options::IOmode::output);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	// Configuring the UART0 pins
	PA0::enableAsDigital();
	PA0::setMode(Gpio::options::mode::alternate);
	PA0::setAlternateMode(Gpio::options::altModes::alt1);

	PA1::enableAsDigital();
	PA1::setMode(Gpio::options::mode::alternate);
	PA1::setAlternateMode(Gpio::options::altModes::alt1);

	UART0::enableClock();
	UART0::configUart(uart::configOptions::baudrate::baud_1000000);

	core::IntEnableMaster();
	core::enableInterrupt(core::interrupts::uart0);

	// Superloop
	while(1)
	{
		PF1::toogle();
		delays::delay_ms(500);
	}

}

void interruptFuncs::uart0rxtx_isr()
{
	UART0::clearInterrupts(uart::configOptions::interrupts::receiveInt);

	buffRx = UART0::receiveByte();

	UART0::sendString("you typed: ");
	UART0::sendByte(buffRx);
	UART0::sendString(" .\n\r");

	PF2::toogle();
}


