/*
 * tutorial2.hpp
 *
 *  Created on: 25/01/2014
 *      Author: wilbert
 */

// Incluyendo librer�as a usar...

#include "include/clock.hpp"// -> clock library
#include "include/Gpio.hpp"// -> gpio library
#include "delays.hpp"

int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
								 	 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);

	// Configuring the GPIO
	PF4::enableAsDigital();
	PF4::setMode(Gpio::options::mode::gpio);
	PF4::setIOmode(Gpio::options::IOmode::input);
	PF4::setPullUp();

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	// Superloop
	while(1)
	{
		if(PF4::isHigh())
		{
			PF2::setHigh();
		}
		else
		{
			PF2::setLow();
		}
		delays::delay_ms(50);
	}

}
