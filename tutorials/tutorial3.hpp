/*
 * tutorial3.hpp
 *
 *  Created on: 25/01/2014
 *      Author: wilbert
 */

// Incluyendo librer�as a usar...

#include "include/clock.hpp"// -> clock library
#include "include/Gpio.hpp"// -> gpio library
#include "include/Uart.h"// -> uart library
#include "delays.hpp"


int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
								 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOA);

	// Configuring the GPIO
	PF4::enableAsDigital();
	PF4::setMode(Gpio::options::mode::gpio);
	PF4::setIOmode(Gpio::options::IOmode::input);
	PF4::setPullUp();

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	// Configuring the UART0 pins
	PA0::enableAsDigital();
	PA0::setMode(Gpio::options::mode::alternate);
	PA0::setAlternateMode(Gpio::options::altModes::alt1);

	PA1::enableAsDigital();
	PA1::setMode(Gpio::options::mode::alternate);
	PA1::setAlternateMode(Gpio::options::altModes::alt1);

	UART0::enableClock();
	UART0::configUart(uart::configOptions::baudrate::baud_1000000);

	// Superloop
	while(1)
	{
		PF2::setHigh();
		UART0::sendString("hello world!!!: ");
		UART0::sendString("PF2 is high\n\r");

		delays::delay_ms(500);

		PF2::setLow();
		UART0::sendString("hello world!!!: ");
		UART0::sendString("PF2 is low \n\r");

		delays::delay_ms(500);
	}

}
