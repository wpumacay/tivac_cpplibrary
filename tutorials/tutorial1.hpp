/*
 * tutorial1.hpp
 *
 *  Created on: 25/01/2014
 *      Author: wilbert
 */

// Incluyendo librer�as a usar...

#include "include/clock.hpp"// -> clock library
#include "include/Gpio.hpp"// -> gpio library
#include "delays.hpp"

int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
								 	 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);

	// Configuring the GPIO
	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	// Superloop
	while(1)
	{
		PF2::toogle();
		delays::delay_ms(500);
	}

}
