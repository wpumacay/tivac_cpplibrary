/*
 * delays.hpp
 *
 *  Created on: Sep 10, 2013
 *      Author: wilbert
 *
 *  Esta librer�a contiene funciones que implementan delays por software
 *  (Los delays son generados usando bucles 'for' haciendo nada dentro del bucle)
 *
 */


namespace delays
{
	// Funci�n delay_ms -> genera un delay en milisegundos
	void delay_ms(unsigned int ms)
	{
		unsigned int i;
		for(i=0;i<5714*ms;i++);
	}
	// Funci�n delay_us -> genera un delay en microsegundos (a�n no ha sido calibrada)
	void delay_us(unsigned int us)
	{
		unsigned int i;
		for(i=0;i<6*us;i++);
	}

}

