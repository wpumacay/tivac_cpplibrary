/*
 * clock.h
 *
 *  Created on: Aug 14, 2013
 *      Author: wilbert
 */

#ifndef CLOCK_H_
#define CLOCK_H_

#include "mapClock.h"

namespace clock
{


	namespace configOptions
	{
		namespace clockRate
		{
			enum _clockRate
			{
				clock_40Mhz = 0x09,
				clock_50Mhz = 0x07,
				clock_80Mhz = 0x04
			};
		}
		namespace clockSource
		{
			enum sources
			{
				mainOscilator = 0x0,
				precisionInternalOscillator = 0x1,
				precisionInternalOscillator_Div4 = 0x2,
				internal_30khz = 0x3,
				external_32_768khz = 0x7
			};
		}
		namespace enablePIOSC
		{
			enum _enablePIOSC
			{
				enablePinternalOsc	= 0,
				disablePinternalOsc	= 1
			};
		}
		namespace enableMOSC
		{
			enum _enableMOSC
			{
				enableMainOsc		= 0,
				disableMainOsc		= 1
			};
		}
	}


	class clock
	{
		public:

			static inline void config(configOptions::clockSource::sources clockSource,
					 				  configOptions::clockRate::_clockRate clockRate_,
					 				  configOptions::enablePIOSC::_enablePIOSC enablePIOSC_,
					 				  configOptions::enableMOSC::_enableMOSC enableMOSC_);
	};

	void clock::config(configOptions::clockSource::sources clockSource,
									 configOptions::clockRate::_clockRate clockRate_,
									 configOptions::enablePIOSC::_enablePIOSC enablePIOSC_,
									 configOptions::enableMOSC::_enableMOSC enableMOSC_)
	{

		reinterpret_cast<mapClock::ClockRegs *>
			(mapClock::moduleAddresses::clockAddress)
			->RCC2 |= (u32)(1<<31);


		reinterpret_cast<mapClock::ClockRegs *>
			(mapClock::moduleAddresses::clockAddress)
			->RCC |= (u32)(1<<11);
		reinterpret_cast<mapClock::ClockRegs *>
			(mapClock::moduleAddresses::clockAddress)
			->RCC &= (u32)(0xffffffff - (1<<22));
		reinterpret_cast<mapClock::ClockRegs *>
			(mapClock::moduleAddresses::clockAddress)
			->RCC = (u32)((1<<11)|(0x15<<6)|(clockSource<<4)|(0x09<<23)|(1<<22)|(enablePIOSC_<<1)|(enableMOSC_<<0));
		reinterpret_cast<mapClock::ClockRegs *>
			(mapClock::moduleAddresses::clockAddress)
			->RCC2 = (u32)((1<<11)|(1<<31)|(1<<30)|(clockRate_<<22)|(clockSource<<4));
		while((reinterpret_cast<mapClock::ClockRegs *>
			  (mapClock::moduleAddresses::clockAddress)
			  ->RIS) & (0x40) != (0x40)
			 );
		reinterpret_cast<mapClock::ClockRegs *>
			(mapClock::moduleAddresses::clockAddress)
			->RCC &= (u32)(0xffffffff - (1<<11));
		reinterpret_cast<mapClock::ClockRegs *>
			(mapClock::moduleAddresses::clockAddress)
			->RCC2 &= (u32)(0xffffffff - (1<<11));
	}

}




#endif /* CLOCK_H_ */
