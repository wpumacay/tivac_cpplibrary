/*
 * main.c
 */
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "Gpio.h"


int LED = 2;


typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio1> ledR;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio2> ledG;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio3> ledB;

int main(void) {


	SysCtlClockSet(SYSCTL_SYSDIV_4|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);

	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	//SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	//GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);

	ledR::unlock();
	ledG::unlock();
	ledB::unlock();

	ledR::enableCommit();
	ledG::enableCommit();
	ledB::enableCommit();

	ledR::enableAsDigital();
	ledG::enableAsDigital();
	ledB::enableAsDigital();

	ledR::setPullUp();
	ledG::setPullUp();
	ledB::setPullUp();

	ledR::setMode(mapGpio::options::mode::gpio);
	ledG::setMode(mapGpio::options::mode::gpio);
	ledB::setMode(mapGpio::options::mode::gpio);

	ledR::setIOmode(mapGpio::options::IOmode::output);
	ledG::setIOmode(mapGpio::options::IOmode::output);
	ledB::setIOmode(mapGpio::options::IOmode::output);

	ledR::setCurrentDrive_mA(mapGpio::options::currentDrive::mA_4);


	//multiplyBy2(2);
	//dummy = multiply_by2(2);

	while(1){
		//GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3,LED);
		SysCtlDelay(2000000);

		if(LED == 8){
			LED = 2;
		}
		else {
			LED = LED*2;
		}

	}


}
