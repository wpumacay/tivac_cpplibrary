/*
 * GPTimer.hpp
 *
 *  Created on: 03/07/2013
 *      Author: wilbert
 */


#pragma once

#include "../memorymap/Gptimer.hpp"

namespace gpTimer{

	template<gpTimer::gpTimerAddresses Timer,gpTimer::options::operationMode::_operationMode operationMode_>
	class timer;

	template<gpTimer::gpTimerAddresses Timer>
	class timer<Timer,gpTimer::options::operationMode::oneshot_or_periodic>;

	template<gpTimer::gpTimerAddresses Timer>
	class timer<Timer,options::operationMode::realTimeClock>;

	template<gpTimer::gpTimerAddresses Timer>
	class timer<Timer,options::operationMode::inputEdge_countMode>;

	template<gpTimer::gpTimerAddresses Timer>
	class timer<Timer,options::operationMode::inputEdge_timeMode>;

	template<gpTimer::gpTimerAddresses Timer>
	class timer<Timer,gpTimer::options::operationMode::pwmMode>;

	template<gpTimer::gpTimerAddresses Timer>
	class timer<Timer,gpTimer::options::operationMode::oneshot_or_periodic>
	{
		public:
			static inline void enableClock();
			static inline unsigned int configTimer
					(gpTimer::subTimers::subtimer subtimer_,
					 unsigned long period_ms,
					 gpTimer::options::joiningMode::_joiningMode joiningMode_,
					 gpTimer::options::timerMode::tMode tMode,
					 gpTimer::options::countDirection::countDir cDir,
					 gpTimer::options::interrupts::interrupts_subtimer tInterrupt
					);

			static inline void enableSubTimer(gpTimer::subTimers::subtimer subtimer_);
			static inline void disableSubTimer(gpTimer::subTimers::subtimer subtimer_);
			static inline void clearInterrupt(gpTimer::options::interrupts::interrupts_subtimer interrupt_)
			{
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMICR |= interrupt_ ;
			}

		private:
			timer();
	};

	template<gpTimer::gpTimerAddresses Timer>
	class timer<Timer,gpTimer::options::operationMode::pwmMode>
	{
		public:
			static inline void config
					(unsigned int period_ms,
					 float duty,
					 gpTimer::subTimers::subtimer subtimer_,
					 gpTimer::pwmOptions::outputLevel::_outputLevel outputLevel_
					);
	};




}


#include "../src/GptimerImplement.hpp"

