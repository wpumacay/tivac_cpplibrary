/*
 * SysCtrl.h
 *
 *  Created on: 05/08/2013
 *      Author: wilbert
 */

#ifndef SYSCTRL_H_
#define SYSCTRL_H_

namespace sysCtrl
{
	unsigned char IntEnableMaster()
	{
		//
			// Read PRIMASK and enable interrupts.
			//
			__asm("    mrs     r0, PRIMASK\n"
				  "    cpsie   i\n"
				  "    bx      lr\n");

			//
			// The following keeps the compiler happy, because it wants to see a
			// return value from this function.  It will generate code to return
			// a zero.  However, the real return is the "bx lr" above, so the
			// return(0) is never executed and the function returns with the value
			// you expect in R0.
			//
			return 0;
	}
}

#endif /* SYSCTRL_H_ */
