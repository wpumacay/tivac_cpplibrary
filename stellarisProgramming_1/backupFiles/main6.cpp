/*
 * main.cpp
 */
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_i2c.h"
#include "driverlib/i2c.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "Gpio.h"
#include "driverlib/interrupt.h"
#include "inc/hw_ints.h"
#include "driverlib/timer.h"
#include "globalPrototypes.h"
#include "interrupts.hpp"
#include "Gptimer.h"
#include "driverlib/adc.h"
#include "driverlib/uart.h"
#include "Uart.h"
#include "Adc.h"
#include "I2C.h"

typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio1> led1;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio2> led2;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio3> led3;
typedef Gpio::gpio<mapGpio::GPIOPortE_APB,mapGpio::gpio3> analogPin;
typedef Gpio::gpio<mapGpio::GPIOPortB_APB,mapGpio::gpio2> i2c0scl;
typedef Gpio::gpio<mapGpio::GPIOPortB_APB,mapGpio::gpio3> i2c0sda;

typedef gpTimer::timer<mapTimer::timer0_simple> timer0;
typedef uart::uart<uart::module::uart0> uart0;
typedef adc::adc<adc::converter::adc0,adc::sequencer::sequencer0> analogVolt;
typedef i2c::i2c<i2c::module::i2c0> i2c0;

unsigned int count = 0;
unsigned int voltage[1]={0};
unsigned char buffDataI2C[2] = {15,255};
unsigned long LED = 1;

bool receivedSetPoint = false;
bool doControlLoop = false;

unsigned char foo=0;


struct gainsPID
{
	double kp;
	double ki;
	double kd;
};

struct errosPID
{
	double ep;
	double ei;
	double ed;
};


struct controllerPID
{
	double setPoint;
	gainsPID gains;
	errosPID erros;
	double x;
	double e_t;
	double e_t_1;
	double u;
}controller;

double dt = 0.010;//control loop sample time of 10ms

void convertForDAC(unsigned char *buffDataForDAC,double u_control)
{
	unsigned int voltage_uint = (unsigned int)((u_control/3.3)*4096.0);
	*buffDataForDAC = (unsigned char)((voltage_uint>>8)&0x000f);
	buffDataForDAC++;
	*buffDataForDAC = (unsigned char)((voltage_uint)&0x00ff);
}

double saturate(double u_control,double max,double min)
{
	if(u_control>max)
	{
		u_control = max;
	}
	else if(u_control<min)
	{
		u_control = min;
	}
	return u_control;
}

int main(void) {

	SysCtlClockSet(SYSCTL_SYSDIV_2_5|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);

	//enable the gpio port related to uart0 pins
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	//enable the gpio port related to i2c0 pins
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	//enable the gpio port related to the rgb leds
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	//enable the gpio port related to adc pins
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

	//enable the uart clock to uart0
	SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
	//enable the adc clock to adc0
	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	SysCtlADCSpeedSet(SYSCTL_ADCSPEED_250KSPS);
	//enable the i2c clock to i2c0
	SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);

	//ADC configuration***********************************************************************
	analogPin::setMode(mapGpio::options::mode::alternate);
	analogPin::disableDigitalFunc();
	analogPin::setAnalogMode();

	analogVolt::disableSequencer();
	analogVolt::selectTrigger(adc::configOptions::trigger::software);
	analogVolt::sampleConfigure(adc::configOptions::sample::sample_1st,
								adc::configOptions::inputpin::ain0,
								adc::configOptions::tempsensor::ommitTempSensor,
								adc::configOptions::interruptEnable::sampleIntEnable,
								adc::configOptions::endOfSequence::isEndOfSample,
								adc::configOptions::differentialMode::ommitDiffMode);
	analogVolt::enableSequencer();
	//****************************************************************************************
	//I2C configuration***********************************************************************
	i2c0scl::enableAsDigital();
	i2c0scl::setMode(mapGpio::options::mode::alternate);
	i2c0scl::setAlternateMode(mapGpio::options::altModes::alt3);
	i2c0scl::setPullUp();

	i2c0sda::enableAsDigital();
	i2c0sda::setMode(mapGpio::options::mode::alternate);
	i2c0sda::setAlternateMode(mapGpio::options::altModes::alt3);
	i2c0sda::setOpenDrain();

	i2c0::config(i2c::configOptions::mode::master,
				 i2c::configOptions::loopback::disabled,
				 i2c::configOptions::speed::standarMode);
	//****************************************************************************************
	//UART configuration**********************************************************************
	GPIOPinTypeUART(GPIO_PORTA_BASE,GPIO_PIN_0|GPIO_PIN_1);
	uart0::configUart(uart::configOptions::baudrate::baud_115200,
					  uart::configOptions::nbits::bits_8,
					  uart::configOptions::parity::none,
					  uart::configOptions::stopbits::stopBits_one,
					  uart::configOptions::interrupts::receiveInt|
					  uart::configOptions::interrupts::receiveTimeOut,
					  uart::configOptions::fifo::fifoDisabled,
					  uart::clockSource::pioscClock);
	//****************************************************************************************
	//TIMER0 configuration********************************************************************
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
	timer0::resetConfig();
	timer0::disableSubTimer(mapTimer::subTimers::subTimerA);
	timer0::disableSubTimer(mapTimer::subTimers::subTimerB);

	timer0::configTimer(10,
						gpTimer::options::timerMode::periodic,
						gpTimer::options::countDirection::down,
						gpTimer::options::interrupts::timerAtimeout);

	timer0::enableSubTimer(mapTimer::subTimers::subTimerA);
	timer0::enableSubTimer(mapTimer::subTimers::subTimerB);
	//****************************************************************************************
	//RGB LEDS configuration******************************************************************
	led1::setIOmode(mapGpio::options::IOmode::output);
	led2::setIOmode(mapGpio::options::IOmode::output);
	led3::setIOmode(mapGpio::options::IOmode::output);

	led1::enableAsDigital();
	led2::enableAsDigital();
	led3::enableAsDigital();
	//****************************************************************************************
	//Initialize controller
	controller.x        = 0;
	controller.setPoint = 1.0;
	controller.e_t 		= 0;
	controller.e_t_1 	= 0;
	controller.erros.ep = 0;
	controller.erros.ei = 0;
	controller.erros.ed = 0;
	controller.gains.kp = 1;
	controller.gains.ki = 0;
	controller.gains.kd = 0;
	controller.u		= 0;
	//INTERRUPTS configuration****************************************************************

	IntEnable(INT_TIMER0A);
	IntEnable(INT_UART0);
	TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
	IntMasterEnable();
	TimerEnable(TIMER0_BASE, TIMER_A);

	//****************************************************************************************
	while(1){
		if(receivedSetPoint)
		{
			foo = uart0::receiveByte();
			controller.setPoint = (((double)(foo))/255.0)*3.3;
			foo = (unsigned char)(((controller.x)/3.3)*255.0);
			uart0::sendByte(foo);
			receivedSetPoint = false;
			led1::toogle();
		}
		else if(false)
		{
			analogVolt::clearInterrupt();
			analogVolt::softBeginSampling();
			while(!analogVolt::finishedSampling())
			{
			}
			analogVolt::takeData(voltage);//value in the range [0-4096] <> [0.0 - 3.3V]
			controller.x = (((double)(voltage[0]))/4096.0)*3.3;
			controller.e_t = controller.setPoint - controller.x;
			controller.erros.ep = controller.e_t;
			controller.erros.ei = controller.erros.ei + dt*controller.e_t;
			controller.erros.ed = (controller.e_t - controller.e_t_1)/dt;
			controller.e_t_1 = controller.e_t;

			controller.u = controller.gains.kp * controller.erros.ep +
						   controller.gains.ki * controller.erros.ei +
						   controller.gains.kd * controller.erros.ed;

			convertForDAC(buffDataI2C,controller.u);
			i2c0::send(0x60,buffDataI2C,2);
			doControlLoop = false;
			led2::toogle();
			timer0::enableSubTimer(mapTimer::subTimers::subTimerA);
		}
	}
}



void interruptFuncs::timer0SubA_isr(){
	TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
	count++;
	doControlLoop = true;

	analogVolt::clearInterrupt();
	analogVolt::softBeginSampling();
	while(!analogVolt::finishedSampling())
	{
	}
	analogVolt::takeData(voltage);//value in the range [0-4096] <> [0.0 - 3.3V]
	controller.x = (((double)(voltage[0]))/4096.0)*3.3;
	controller.e_t = controller.setPoint - controller.x;
	controller.erros.ep = controller.e_t;
	controller.erros.ei = controller.erros.ei + dt*controller.e_t;
	controller.erros.ed = (controller.e_t - controller.e_t_1)/dt;
	controller.e_t_1 = controller.e_t;

	controller.u = controller.gains.kp * controller.erros.ep +
				   controller.gains.ki * controller.erros.ei +
				   controller.gains.kd * controller.erros.ed;

	controller.u = saturate(controller.u,3.3,0);
	convertForDAC(buffDataI2C,controller.u);
	i2c0::send(0x60,buffDataI2C,2);
	doControlLoop = false;
	led2::toogle();
	//timer0::disableSubTimer(mapTimer::subTimers::subTimerA);
}


void interruptFuncs::uart0rxtx_isr()
{
	unsigned long ulstatus;

	ulstatus = UARTIntStatus(UART0_BASE,true);
	UARTIntClear(UART0_BASE,ulstatus);
	receivedSetPoint = true;
	/*uart0::clearInterrupts(uart::configOptions::interrupts::receiveInt|
			  	  	  	   uart::configOptions::interrupts::receiveTimeOut);
	*/
}
