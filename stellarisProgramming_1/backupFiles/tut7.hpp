const double dummyConstant = 3.1415;

	namespace initOptions{
		enum options{
			zeros = 0,
			ones = 1
		};
	}
	class cvector
	{
		public:
			unsigned int *dim;
			double *buffer;
			static int nOfObjects;
			static void howManyObjectsSoFar();
			cvector(){

			}
			cvector(unsigned int dim);
			cvector(unsigned int dim,initOptions::options initOption);
			~cvector();
			cvector operator + (cvector);
			cvector operator * (cvector);
			void toStringPrint()
			{
				std::cout << "(";
				for(unsigned int i = 0; i < *(this->dim);i++)
				{
					std::cout << *(this->buffer+i) << ",";
				}
				std::cout << ")";				
			}
	};

	int cvector::nOfObjects = 0;

	cvector::cvector(unsigned int dim)
	{
		this->dim = new unsigned int;
		this->buffer = new double[dim];
		*this->dim = dim;
		this->nOfObjects++;
	}
	cvector::cvector(unsigned int dim,initOptions::options initOption)
	{
		this->dim = new unsigned int;
		this->buffer = new double[dim];
		*this->dim = dim;
		for(unsigned int i=0;i<dim;i++)
		{
			*(this->buffer + i) = initOption;
		}
	}


	cvector::~cvector()
	{
		//delete this->dim;
		//delete [] this->buffer;
		this->nOfObjects--;
	}

	cvector cvector::operator+ (cvector tVector)
	{
		
		if(*(this->dim) == *(tVector.dim))
		{
			cvector dummyVector(*(tVector.dim));
			for(unsigned int i=0;i<*(this->dim);i++)
			{
				*(dummyVector.buffer+i) = *(this->buffer + i) + *(tVector.buffer + i);
			}
			return dummyVector;
		}
		else
		{
			std::cout << "could make vector sum, since the vectors are not the same size" << std::endl;
		}
	}
	cvector cvector::operator* (cvector tVector)
	{
		if(*(this->dim) == *(tVector.dim))
		{
			cvector dummyVector(*(tVector.dim));
			for(unsigned int i=0;i<*(this->dim);i++)
			{
				*(dummyVector.buffer+i) = *(this->buffer + i) * *(tVector.buffer + i);
			}
			return dummyVector;
		}		
		else
		{
			std::cout << "could make element wise product, since the vectors are not the same size" << std::endl;
		}
	}

	cvector zeros(unsigned int dim)
	{
		cvector dummyVector(dim);
		for(unsigned int i=0;i<dim;i++)
		{
			*(dummyVector.buffer+i) = 0;
		}
		return dummyVector;
	}

	cvector ones(unsigned int dim)
	{
		cvector dummyVector(dim);
		for(unsigned int i=0;i<dim;i++)
		{
			*(dummyVector.buffer+i) = 1;
		}
		return dummyVector;
	}

	void cvector::howManyObjectsSoFar()
	{
		std::cout << "there are " << nOfObjects << " objects so far." << std::endl;
	}

int main()
{
	std::cout << "using enumarators..." << std::endl;
	std::cout << "sizeof of the enumerator: " << sizeof(myQuality) << std::endl;
	std::cout << "first possible value: " << excellent << std::endl;
	std::cout << "second possible value: " << good << std::endl;
	std::cout << "third possible value: " << soso << std::endl;
	std::cout << "fourth possible value: " << bad << std::endl;
	std::cout << "fifth possible value: " << awful << std::endl;


	nQuality::rating myRatingOnTheMovie = nQuality::notbad;

	takeEnum(myRatingOnTheMovie);

	std::cout << "address of myRatingOnTheMovie: " << &myRatingOnTheMovie << std::endl;
	std::cout << "address of dummyString: " << &dummyString << std::endl;
	std::cout << "address of dummyConstant: " << &dummyConstant << std::endl;
	std::cout << "address of takeEnum: " << &takeEnum << std::endl;
	std::cout << "address of exit: " << &exitNow << std::endl;

	useAgents();

	std::cout << "using clases and operator overloading....." << std::endl;
	
	cvector a(3);
	*(a.buffer+0) = 1;
	*(a.buffer+1) = 2;
	*(a.buffer+2) = 3;

	cvector b(3);
	*(b.buffer+0) = 1;
	*(b.buffer+1) = 3;
	*(b.buffer+2) = 5;
	//*(b.buffer+3) = 7;

	std::cout << "vector a is: ";
	a.toStringPrint();
	std::cout << std::endl;
	a.howManyObjectsSoFar();

	std::cout << "vector b is: ";
	b.toStringPrint();
	std::cout << std::endl;

	
	cvector c(3);
	c.howManyObjectsSoFar();
	c = a+b;
	std::cout << "the sum is: ";
	c.toStringPrint();
	std::cout << std::endl;
	c.howManyObjectsSoFar();

	std::cout << "the element wise product is: ";
	c = a*b;
	c.toStringPrint();
	std::cout << std::endl;
	c.howManyObjectsSoFar();

	
	cvector d(3);
	a.howManyObjectsSoFar();
	d = ones(3);
	std::cout << "a vector using the ones function : ";
	d.toStringPrint();
	std::cout << std::endl;
	d.howManyObjectsSoFar();

	exitNow();
	return 0;
}

void exitNow()
{
	std::string dummyString;
	std::cout << "press any letter and then enter to exit." << std::endl;
	std::cin >> dummyString;
}
