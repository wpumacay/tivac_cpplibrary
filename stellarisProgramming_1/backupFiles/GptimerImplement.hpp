/*
* Gptimer.cpp
*
*  Created on: 07/08/2013
*      Author: wilbert
*/

#pragma once
#include "../memorymap/Gptimer.hpp"
#include "../include/Gptimer.hpp"
#include "../mapClock.h"

namespace gpTimer{



	template<gpTimer::gpTimerAddresses Timer>
	void gpTimer::timer<Timer,gpTimer::options::operationMode::oneshot_or_periodic>::enableClock()
	{
			switch (Timer) {
				case gpTimer::timer0_simple:
					reinterpret_cast<mapClock::ClockRegs *>
						(mapClock::moduleAddresses::clockAddress)
						->RCGCTIMER |= (1<<0);
				break;
				case gpTimer::timer1_simple:
					reinterpret_cast<mapClock::ClockRegs *>
						(mapClock::moduleAddresses::clockAddress)
						->RCGCTIMER |= (1<<1);
				break;
				case gpTimer::timer2_simple:
					reinterpret_cast<mapClock::ClockRegs *>
						(mapClock::moduleAddresses::clockAddress)
						->RCGCTIMER |= (1<<2);
				break;
				case gpTimer::timer3_simple:
					reinterpret_cast<mapClock::ClockRegs *>
						(mapClock::moduleAddresses::clockAddress)
						->RCGCTIMER |= (1<<3);
				break;
				case gpTimer::timer4_simple:
					reinterpret_cast<mapClock::ClockRegs *>
						(mapClock::moduleAddresses::clockAddress)
						->RCGCTIMER |= (1<<4);
				break;
				case gpTimer::timer5_simple:
					reinterpret_cast<mapClock::ClockRegs *>
						(mapClock::moduleAddresses::clockAddress)
						->RCGCTIMER |= (1<<5);
				break;
				case gpTimer::timer0_wide:
					reinterpret_cast<mapClock::ClockRegs *>
						(mapClock::moduleAddresses::clockAddress)
						->RCGCWTIMER |= (1<<6);
				break;
				case gpTimer::timer1_wide:
					reinterpret_cast<mapClock::ClockRegs *>
						(mapClock::moduleAddresses::clockAddress)
						->RCGCWTIMER |= (1<<7);
				break;
				case gpTimer::timer2_wide:
					reinterpret_cast<mapClock::ClockRegs *>
						(mapClock::moduleAddresses::clockAddress)
						->RCGCWTIMER |= (1<<7);
				break;
				case gpTimer::timer3_wide:
					reinterpret_cast<mapClock::ClockRegs *>
						(mapClock::moduleAddresses::clockAddress)
						->RCGCWTIMER |= (1<<7);
				break;
				case gpTimer::timer4_wide:
					reinterpret_cast<mapClock::ClockRegs *>
						(mapClock::moduleAddresses::clockAddress)
						->RCGCWTIMER |= (1<<7);
				break;
				case gpTimer::timer5_wide:
					reinterpret_cast<mapClock::ClockRegs *>
						(mapClock::moduleAddresses::clockAddress)
						->RCGCWTIMER |= (1<<7);
				break;
			}
	}




//start func 1
	template<gpTimer::gpTimerAddresses Timer>
	void gpTimer::timer<Timer,gpTimer::options::operationMode::oneshot_or_periodic>::enableSubTimer(gpTimer::subTimers::subtimer subtimer_)
	{
        reinterpret_cast<gpTimer::GPTimerRegs *>
                (Timer)->GPTMCTL |=(1<<8*subtimer_);
	}
//end func 1

//start func 2
	template<gpTimer::gpTimerAddresses Timer>
	void gpTimer::timer<Timer,gpTimer::options::operationMode::oneshot_or_periodic>::disableSubTimer(gpTimer::subTimers::subtimer subtimer_)
	{
        reinterpret_cast<gpTimer::GPTimerRegs *>
                (Timer)->GPTMCTL &=0xffffffff - (1<<subtimer_*8);
	}
//end func 2

//start func 3
	template<gpTimer::gpTimerAddresses Timer>
	unsigned int gpTimer::timer<Timer,gpTimer::options::operationMode::oneshot_or_periodic>::configTimer
			(gpTimer::subTimers::subtimer subtimer_,
			 unsigned long period_ms,
			 gpTimer::options::joiningMode::_joiningMode joiningMode_,
			 gpTimer::options::timerMode::tMode tMode,
			 gpTimer::options::countDirection::countDir cDir,
			 gpTimer::options::interrupts::interrupts_subtimer tInterrupt
			)
	{
		unsigned int statusCode = 0;
		switch(subtimer_){

			case gpTimer::subTimers::subTimerA :

				//firstly, disable the sub-timer
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMCTL &=0xffffffff - (1<<(0*8));
				//configure the joining mode, weather or not the sub-timers and joined
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMCFG = joiningMode_;
				//configure the sub-timer A
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMTAMR = (tMode) | (cDir<<4);
				//load the period in the period register
				if(joiningMode_ == options::joiningMode::timers_joinTimers)
				{
					reinterpret_cast<gpTimer::GPTimerRegs *>
						(Timer)->GPTMTAILR = period_ms*80000;
				}
				else if(joiningMode_ == gpTimer::options::joiningMode::timers_dontJoinTimers)
				{
					//only use this mode if the desired period is less than 209ms
					reinterpret_cast<gpTimer::GPTimerRegs *>(Timer)->GPTMTAPR = 0x000000ff;
					if(period_ms>208){
						reinterpret_cast<gpTimer::GPTimerRegs *>(Timer)->GPTMTAILR = 0x0000ffff;
						statusCode = 1;
					}
					else
					{
						reinterpret_cast<gpTimer::GPTimerRegs *>(Timer)->GPTMTAILR = 312.5*period_ms;
					}
				}
				//enable interrupts if needed
				reinterpret_cast<gpTimer::GPTimerRegs *>(Timer)->GPTMIMR = tInterrupt;
				//lastly, enable the sub-timer
				reinterpret_cast<gpTimer::GPTimerRegs *>(Timer)->GPTMCTL |=(1);

			break;

			case gpTimer::subTimers::subTimerB :

				//firstly, disable the sub-timer
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMCTL &=0xffffffff - (1<<(1*8));
				//configure the joining mode, weather or not the sub-timers and joined
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMCFG = joiningMode_;
				//configure the sub-timer B
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMTBMR = (tMode) | (cDir<<4);
				//load the period in the period register
				if(joiningMode_ == gpTimer::options::joiningMode::timers_joinTimers)
				{
					//this sub-timer is not supposed to be working!!!
					statusCode = 2;
				}
				else if(joiningMode_ == gpTimer::options::joiningMode::timers_dontJoinTimers)
				{
					//only use this mode if the desired period is less than 209ms
					reinterpret_cast<gpTimer::GPTimerRegs *>
						(Timer)->GPTMTBPR = 0x000000ff;
					if(period_ms>208){
						reinterpret_cast<gpTimer::GPTimerRegs *>
							(Timer)->GPTMTBILR = 0x0000ffff;
						statusCode = 1;
					}
					else
					{
						reinterpret_cast<gpTimer::GPTimerRegs *>
							(Timer)->GPTMTBILR = 312.5*period_ms;
					}
				}
				//enable interrupts if needed
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMIMR = tInterrupt;
				//lastly, enable the sub-timer
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMCTL |=(1<<8);

			break;
		}

		return statusCode;
	}
//end func 3

//start func 4
	template<gpTimer::gpTimerAddresses Timer>
	void gpTimer::timer<Timer,gpTimer::options::operationMode::pwmMode>::config
		(unsigned int period_ms,
		 float duty,
		 gpTimer::subTimers::subtimer subtimer_,
		 gpTimer::pwmOptions::outputLevel::_outputLevel outputLevel_
		)
	{
		switch(subtimer_)
		{
			case gpTimer::subTimers::subTimerA :

				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMCTL &= 0xffffffff - (1);
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMCFG = 0x00000004;
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMTAMR = (0x02)|(0<<2)|(1<<3);
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMCTL |= (outputLevel_<<6);
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMTAPR = 0x00000000;//250... so... it's more accurate
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMTAILR = 0x2625a00;//320*period_ms;
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMTAPMR = 0x00000000;
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMTAMATCHR = 0x1312d00;//(unsigned long)(duty*320*period_ms);
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMCTL |= 1;
			break;
			case gpTimer::subTimers::subTimerB :

				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMCTL &= 0xffffffff - (1<<8);
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMCFG = 0x00000004;
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMTBMR = (0x02)|(0<<2)|(1<<3);
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMCTL |= (outputLevel_<<14);
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMTBPR = 0x000000fa;//250... so... it's more accurate
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMTBILR = 320*period_ms;
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMTBPMR = 0x000000fa;
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMTBMATCHR = (unsigned long)(duty*320*period_ms);
				reinterpret_cast<gpTimer::GPTimerRegs *>
					(Timer)->GPTMCTL |= 1<<8;
			break;
		}
	}
//end func 4

}
