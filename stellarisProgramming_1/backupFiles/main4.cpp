/*
 * main.c
 */
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_i2c.h"
#include "driverlib/i2c.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "Gpio.h"
#include "driverlib/interrupt.h"
#include "inc/hw_ints.h"
#include "driverlib/timer.h"
#include "globalPrototypes.h"
#include "interrupts.hpp"
#include "Gptimer.h"
#include "driverlib/adc.h"
#include "driverlib/uart.h"
#include "Uart.h"
#include "Adc.h"
#include "I2C.h"

//#include "startup_ccs.h"



unsigned long LED = 2;

//#define dirF_APB 0x40025000

unsigned long *puntero;

typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio1> led1;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio2> led2;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio3> led3;
typedef Gpio::gpio<mapGpio::GPIOPortE_APB,mapGpio::gpio3> analogPin;
typedef Gpio::gpio<mapGpio::GPIOPortB_APB,mapGpio::gpio2> i2c0scl;
typedef Gpio::gpio<mapGpio::GPIOPortB_APB,mapGpio::gpio3> i2c0sda;

typedef gpTimer::timer<mapTimer::timer0_simple> timer0;
typedef uart::uart<uart::module::uart0> uart0;

typedef adc::adc<adc::converter::adc0,adc::sequencer::sequencer1> tempSensor;
typedef adc::adc<adc::converter::adc0,adc::sequencer::sequencer0> analogVolt;

typedef i2c::i2c<i2c::module::i2c0> i2c0;

bool dummyBool = true;
unsigned long dummyValue = 0;

unsigned long ulPeriod;

unsigned int count = 0;


void Timer0IntHandler(void){
	// Clear the timer interrupt
	 TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
	 count++;
}

//VECTABLE vecTable1;
//unsigned long *pToVector;

unsigned long stackLong_value = 0;

PINT voidPointer;

unsigned int ulADC0Value[4];
volatile unsigned int ulTempAvg;
volatile unsigned int ulTempValueC;
volatile unsigned int ulTempValueF;

unsigned int voltage[1]={0};

unsigned char buffDataI2C[2] = {15,255};

int main(void) {
//	puntero = (unsigned long *)(0xe000e000 + 0x0d08);
//	*puntero = 0x20000000;


	SysCtlClockSet(SYSCTL_SYSDIV_2_5|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);
	
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	//GPIOPinTypeI2C()

	//enable the uart clock to uart0
	SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
	//enable the gpio port related to uart0 pins
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	//GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);

	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	SysCtlADCSpeedSet(SYSCTL_ADCSPEED_250KSPS);

	//GPIOPinTypeADC(GPIO_PORTE_BASE,GPIO_PIN_5);
	analogPin::setMode(mapGpio::options::mode::alternate);
	analogPin::disableDigitalFunc();
	analogPin::setAnalogMode();

	/*ADCSequenceDisable(ADC0_BASE, 1);

	ADCSequenceConfigure(ADC0_BASE, 1, ADC_TRIGGER_PROCESSOR, 0);
	ADCSequenceStepConfigure(ADC0_BASE, 1, 0, ADC_CTL_TS);
	ADCSequenceStepConfigure(ADC0_BASE, 1, 1, ADC_CTL_TS);
	ADCSequenceStepConfigure(ADC0_BASE, 1, 2, ADC_CTL_TS);
	ADCSequenceStepConfigure(ADC0_BASE, 1, 3, ADC_CTL_TS | ADC_CTL_IE | ADC_CTL_END);
	ADCSequenceEnable(ADC0_BASE, 1);*/

	SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);

	i2c0::config(i2c::configOptions::mode::master,
				 i2c::configOptions::loopback::disabled,
				 i2c::configOptions::speed::standarMode);

	i2c0scl::enableAsDigital();
	i2c0scl::setMode(mapGpio::options::mode::alternate);
	i2c0scl::setAlternateMode(mapGpio::options::altModes::alt3);
	//i2c0scl::setPullUp();

	i2c0sda::enableAsDigital();
	i2c0sda::setMode(mapGpio::options::mode::alternate);
	i2c0sda::setAlternateMode(mapGpio::options::altModes::alt3);
	i2c0sda::setOpenDrain();

	tempSensor::disableSequencer();
	tempSensor::selectTrigger(adc::configOptions::trigger::software);
	tempSensor::sampleConfigure(adc::configOptions::sample::sample_1st,
								adc::configOptions::inputpin::ain0,
								adc::configOptions::tempsensor::selectTempSensor,
								adc::configOptions::interruptEnable::sampleIntDisabled,
								adc::configOptions::endOfSequence::isntEndOfSample,
								adc::configOptions::differentialMode::ommitDiffMode);
	tempSensor::sampleConfigure(adc::configOptions::sample::sample_2nd,
								adc::configOptions::inputpin::ain0,
								adc::configOptions::tempsensor::selectTempSensor,
								adc::configOptions::interruptEnable::sampleIntDisabled,
								adc::configOptions::endOfSequence::isntEndOfSample,
								adc::configOptions::differentialMode::ommitDiffMode);
	tempSensor::sampleConfigure(adc::configOptions::sample::sample_3rd,
								adc::configOptions::inputpin::ain0,
								adc::configOptions::tempsensor::selectTempSensor,
								adc::configOptions::interruptEnable::sampleIntDisabled,
								adc::configOptions::endOfSequence::isntEndOfSample,
								adc::configOptions::differentialMode::ommitDiffMode);
	tempSensor::sampleConfigure(adc::configOptions::sample::sample_4th,
								adc::configOptions::inputpin::ain0,
								adc::configOptions::tempsensor::selectTempSensor,
								adc::configOptions::interruptEnable::sampleIntEnable,
								adc::configOptions::endOfSequence::isEndOfSample,
								adc::configOptions::differentialMode::ommitDiffMode);
	tempSensor::enableSequencer();

	analogVolt::disableSequencer();
	analogVolt::selectTrigger(adc::configOptions::trigger::software);
	analogVolt::sampleConfigure(adc::configOptions::sample::sample_1st,
								adc::configOptions::inputpin::ain0,
								adc::configOptions::tempsensor::ommitTempSensor,
								adc::configOptions::interruptEnable::sampleIntEnable,
								adc::configOptions::endOfSequence::isEndOfSample,
								adc::configOptions::differentialMode::ommitDiffMode);
	analogVolt::enableSequencer();

	GPIOPinTypeUART(GPIO_PORTA_BASE,GPIO_PIN_0|GPIO_PIN_1);
	uart0::configUart(uart::configOptions::baudrate::baud_115200,
					  uart::configOptions::nbits::bits_8,
					  uart::configOptions::parity::none,
					  uart::configOptions::stopbits::stopBits_one,
					  uart::configOptions::interrupts::receiveInt|
					  uart::configOptions::interrupts::receiveTimeOut,
					  uart::configOptions::fifo::fifoDisabled,
					  uart::clockSource::pioscClock);

	/*UARTConfigSetExpClk(UART0_BASE,SysCtlClockGet(),115200,
						UART_CONFIG_WLEN_8 | UART_CONFIG_PAR_NONE | UART_CONFIG_STOP_ONE);
	*/
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
	timer0::resetConfig();
	timer0::disableSubTimer(mapTimer::subTimers::subTimerA);
	timer0::disableSubTimer(mapTimer::subTimers::subTimerB);

	timer0::configTimer(500,
						gpTimer::options::timerMode::periodic,
						gpTimer::options::countDirection::down,
						gpTimer::options::interrupts::timerAtimeout);

	timer0::enableSubTimer(mapTimer::subTimers::subTimerA);
	timer0::enableSubTimer(mapTimer::subTimers::subTimerB);

//	TimerConfigure(TIMER0_BASE, TIMER_CFG_32_BIT_PER);

//	ulPeriod = (SysCtlClockGet() / 10) / 2;
//	TimerLoadSet(TIMER0_BASE, TIMER_A, ulPeriod -1);

//	pToVector = (unsigned long *)(0x0000008c);
//	*pToVector = (unsigned long)(&Timer0IntHandler);



//	IntEnable(INT_TIMER0A);
	IntEnable(INT_UART0);
	//TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
	IntMasterEnable();

	TimerEnable(TIMER0_BASE, TIMER_A);

	//UARTIntEnable(UART0_BASE,UART_INT_RX|UART_INT_RT);

	stackLong_value = sizeof(unsigned long long);
	//initPieVectTable();
	voidPointer = (PINT)(&Timer0IntHandler);
	//vectorTable.stackTop = (PINT)(&Timer0IntHandler);
	//vecTable1.timer0SubA_isr = (PINT)(&Timer0IntHandler);

	//configurar pin como entrada o salida
	//puntero = (unsigned long *)(0x40025000 + 0x0400);
	//*puntero = 0x000f;

	led1::setIOmode(mapGpio::options::IOmode::output);
	led2::setIOmode(mapGpio::options::IOmode::output);
	led3::setIOmode(mapGpio::options::IOmode::output);

	led1::enableAsDigital();



	//configurar pin como gpio
	//en este caso, configuramos los pines 0,1,2,3 como gpio, y los
	//pines 4,5,6,7 con su funci�n alterna
	puntero = (unsigned long *)(0x40025000 + 0x0420);
	*puntero = 0x0000;

	//configurar la corriente que maneja el pin
	//en este caso, seteamos los pines a 2mA
	puntero = (unsigned long *)(0x40025000 + 0x500);
	*puntero = 0x00ff;

	//pull ups,downs y open drains resactivados por default
	puntero = (unsigned long *)(0x40025000 + 0x50c);
	*puntero = 0x0000;
	puntero = (unsigned long *)(0x40025000 + 0x510);
	*puntero = 0x0000;
	puntero = (unsigned long *)(0x40025000 + 0x514);
	*puntero = 0x0000;

	//gpioDEN habilitar las funciones digitales
	puntero = (unsigned long *)(0x40025000 + 0x51c);
	*puntero = 0x00ff;



	puntero = (unsigned long *)(0x40025000 + 0x0000003C);
	*puntero = 2;

	led1::setHigh();
	//dummyValue = led1::isHigh();
	//dummyBool  = led1::isHigh() == 2;
	dummyBool = led1::isHigh();
	//led2::setHigh();
	//dummyValue = led2::isHigh();
	//dummyBool  = led2::isHigh() == 4;
	dummyBool = led2::isHigh();
	led3::setHigh();
	//dummyValue = led3::isHigh();
	dummyBool = led3::isHigh();
	//dummyBool  = led3::isHigh() == 8;

	led1::setHigh();
	led1::setLow();
	led2::setHigh();
	led2::setLow();
	led3::setHigh();
	led3::setLow();

	led1::toogle();
	led1::toogle();
	//UARTCharPut(UART0_BASE,'h');
	//UARTCharPut(UART0_BASE,'o');
	//UARTCharPut(UART0_BASE,'l');
	//UARTCharPut(UART0_BASE,'a');
	//UARTCharPut(UART0_BASE,' ');
	uart0::sendByte('h');
	uart0::sendByte('o');
	uart0::sendByte('l');
	uart0::sendByte('a');
	while(1){
		//GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3,LED);
		SysCtlDelay(2000000);
		i2c0::send(0x60,buffDataI2C,2);

		 count++;

/*
		 ADCIntClear(ADC0_BASE, 1);
		 ADCProcessorTrigger(ADC0_BASE, 1);


		 while(!ADCIntStatus(ADC0_BASE, 1, false))
		 {
		 }
		 ADCSequenceDataGet(ADC0_BASE, 1, ulADC0Value);
	*/
		 tempSensor::clearInterrupt();
		 tempSensor::softBeginSampling();
		 while(!tempSensor::finishedSampling())
		 {
		 }


		 tempSensor::takeData(ulADC0Value);
		 ulTempAvg = (ulADC0Value[0] + ulADC0Value[1] + ulADC0Value[2] + ulADC0Value[3] + 2)/4;
		 ulTempValueC = (1475 - ((2475 * ulTempAvg)) / 4096)/10;
		 ulTempValueF = ((ulTempValueC * 9) + 160) / 5;

		 analogVolt::clearInterrupt();
		 analogVolt::softBeginSampling();
		 while(!analogVolt::finishedSampling())
		 {
		 }
		 analogVolt::takeData(voltage);
	}


}

void interruptFuncs::reset_isr(){
    //
    // Jump to the CCS C initialization routine.  This will enable the
    // floating-point unit as well, so that does not need to be done here.
    //
    __asm("    .global _c_int00\n"
          "    b.w     _c_int00");
}

void interruptFuncs::gpioA_isr(){

}

void interruptFuncs::timer0SubA_isr(){
	TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

/*	UARTCharPut(UART0_BASE,'h');
	UARTCharPut(UART0_BASE,'o');
	UARTCharPut(UART0_BASE,'l');
	UARTCharPut(UART0_BASE,'a');*/
	//SysCtlDelay(2000000);



	 count++;
/*
	 ADCIntClear(ADC0_BASE, 1);
	 ADCProcessorTrigger(ADC0_BASE, 1);

	 while(!ADCIntStatus(ADC0_BASE, 1, false))
	 {
	 }

	 ADCSequenceDataGet(ADC0_BASE, 1, ulADC0Value);
	 ulTempAvg = (ulADC0Value[0] + ulADC0Value[1] + ulADC0Value[2] + ulADC0Value[3] + 2)/4;
	 ulTempValueC = (1475 - ((2475 * ulTempAvg)) / 4096)/10;
	 ulTempValueF = ((ulTempValueC * 9) + 160) / 5;
	 */
}


void interruptFuncs::uart0rxtx_isr()
{
	unsigned long ulstatus;
	unsigned char foo;

	ulstatus = UARTIntStatus(UART0_BASE,true);
	UARTIntClear(UART0_BASE,ulstatus);

	//foo = UARTCharGet(UART0_BASE);
	foo = uart0::receiveByte();
	//UARTCharPut(UART0_BASE,foo);
	uart0::sendByte(foo);

	*puntero = LED;
	if(LED == 8){
		LED = 2;
	}
	else {
		LED = LED*2;
	}
}
