/*
 * Demo: Env�o de una se�al sinusoidal a labview usando el UART0
 * 	M�dulo UART a usar -> UART0
 * 	Probado, se ve la funci�n seno en el .vi de labview adjuntado en la carpeta labview
 * 	A veces se pierden los datos, y si notan, a veces el valor recibido en foo no es 10, como
 * 	se supondr�a ya que en labview se est� enviando este valor para iniciar la comunicaci�n.
 *
 * 	Posible causa: Es muy probable que los drivers para labview no est�n funcionando correctamente
 * 	Estoy probando con labview 2010, y no me causan tantos problemas.
 */

#include "include/Gpio.hpp"
#include "include/clock.hpp"
#include "include/Uart.h"
#include "delays.hpp"
#include "include/Core.hpp"
#include "interrupts.hpp"
#include "math.h"

// Rutina de preprocesador permite reemplazar donde sea que se vea ...
// PI el valor 3.141592653589793 (observar que no se est� gastango memoria en crear una variable)
#define PI 3.141592653589793

// Creamos un UNION llamado "data"
// Un union es tambi�n un tipo de variable definido por el usuario ...
// el cu�l permite poder englobar 2 o m�s variables que representan ...
// lo mismo en distintos formatos
union data
{
	float floatData;// Representaci�n en punto flotante de nuestro union
	u8 bytes[4];// Representaci�n en bytes de nuestro union (un float tiene 4 bytes)
	// u8 = unsigned char = 1 byte
};
// Esto es bastante bueno ya que nos simplifica el tener que extraer cada byte ...
// del n�mero en punto flotante al querer enviarlo usando el UART

// Definido nuestro tipo de variable (de nombre data) procedemos a crear una variable ...
// de este tipo, la cu�l llamamos buff
data buff;

// Variable que almacenar� el valor que recibamos por puerto serie que a la larga no usaremos
u8 foo = 0;

float t = 0.0;


int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
								 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOA);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PA0::enableAsDigital();
	PA0::setMode(Gpio::options::mode::alternate);
	PA0::setAlternateMode(Gpio::options::altModes::alt1);

	PA1::enableAsDigital();
	PA1::setMode(Gpio::options::mode::alternate);
	PA1::setAlternateMode(Gpio::options::altModes::alt1);

	UART0::enableClock();
	UART0::configUart(uart::configOptions::baudrate::baud_1000000);

	core::IntEnableMaster();
	core::enableInterrupt(core::interrupts::uart0);

	while(1)
	{
		PF2::toogle();
		delays::delay_ms(100);
	}
}

void interruptFuncs::uart0rxtx_isr()
{
	UART0::clearInterrupts(uart::configOptions::interrupts::receiveInt);
	foo = UART0::receiveByte();
	t += 0.01;
	// Accedemos al elemento floatData de nuestra variable buff ...
	// y escribimos el valor del seno de 2*pi*t
	buff.floatData = std::sinf(2*PI*t);
	// Enviamos cada byte de la representaci�n del n�mero en punto flotante ...
	// por puerto serie usando la representaci�n equivalente en bytes
	UART0::sendByte(buff.bytes[3]);
	UART0::sendByte(buff.bytes[2]);
	UART0::sendByte(buff.bytes[1]);
	UART0::sendByte(buff.bytes[0]);
}

