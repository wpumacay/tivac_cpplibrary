/*
 * Demo: Uso del puerto serie con un terminal (putty)
 * 	M�dulo UART a usar -> UART0
 */

/*
 * Inclu�mos las librer�as necesarias
 */

// Librer�a de clock -> funciones que permiten configurar el clock
#include "include/clock.hpp"
// Librer�a de los GPIOs -> funciones que permiten configurar los GPIOs
#include "include/Gpio.hpp"
// Librer�a para delays -> funciones para generar delays por software
#include "delays.hpp"
// Librer�a para manejar los m�dulos UART
#include "include/Uart.h"

int main()
{
	// Configuramos el clock a 80Mhz usando el oscilador principal (oscilador externo de 16Mhz)
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
								 clock::configOptions::clockRate::clock_80Mhz);

	// Habilitamos el clock para el perif�rico GPIOF, lo cu�l nos permite configurar sus registros.
	Gpio::enableClock(Gpio::peripheral::GPIOF);
	// Habilitamos el clock para el perif�rico GPIOA (puero que contiene los pines dle UART0) ...
	// ... lo cu�l nos permite configurar sus registros.
	Gpio::enableClock(Gpio::peripheral::GPIOA);

	// Configuramos el pin PF2 como Gpio y en modo salida
	PF2::enableAsDigital();// Habilitamos el pin con funci�n digital (no la anal�gica, en caso la haya tenido)
	PF2::setMode(Gpio::options::mode::gpio);// Configuramos el modo digital a GPIO (no alterno, siendo estos 15 modos)
	PF2::setIOmode(Gpio::options::IOmode::output);// Configuramos el pin como salida

	// Configuramos los pines PA0 y PA1  a su funci�n alterna 1 (UART0 rx y tx respectivamente)
	PA0::enableAsDigital();// Habilitamos la funci�n digital de este pin
	PA0::setMode(Gpio::options::mode::alternate); // Elegimos la opci�n alterna para este pin
	PA0::setAlternateMode(Gpio::options::altModes::alt1);// Elegimos la funci�n alterna n�mero 1 (se�al RX del UART0)
	// De manera similar configuramos el pin PA1
	PA1::enableAsDigital();// Habilitamos la funci�n digital de este pin
	PA1::setMode(Gpio::options::mode::alternate); // Elegimos la opci�n alterna para este pin
	PA1::setAlternateMode(Gpio::options::altModes::alt1);// Elegimos la funci�n alterna n�mero 1 (se�al TX del UART0)

	// Configuramos el m�dulo UART0 usando las librer�as

	// Habilitamos el clock para este m�dulo
	// (Sin esto, no podremos configurar sus registros-> Salto a rutina de error Fault_ISR)
	UART0::enableClock();
	// Configuramos el UART0 a un baudRate de 1 000 000 bps (1Mbps)
	// Por default se configura a 8 bits por dato, 1 bit de parada y sin paridad ni control de flujo
	UART0::configUart(uart::configOptions::baudrate::baud_1000000);

	while(1)
	{
		// Toogleamos el LED cada 500 milisegundos
		PF2::toogle();
		// Enviamos un mensaje deacuerdo al estado del led
		if(PF2::isHigh())
		{
			// Si el nivel del pin PF2 est� en alto (3.3v = led prendido) ...
			// enviamos que el led est� prendido
			UART0::sendString("led en PF2 prendido");
		}
		else
		{
			// Caso contrario, enviamos que el led est� apagado
			UART0::sendString("led en PF2 apagado");
		}
		delays::delay_ms(500);
	}
}
