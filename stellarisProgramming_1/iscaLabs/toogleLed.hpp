/*
 * Demo: Prender y apagar un LED
 * 	Led a usar -> interno en la placa en el pin PF2
 */

/*
 * Inclu�mos las librer�as necesarias
 */

// Librer�a de clock -> funciones que permiten configurar el clock
#include "include/clock.hpp"
// Librer�a de los GPIOs -> funciones que permiten configurar los GPIOs
#include "include/Gpio.hpp"
// Librer�a para delays -> funciones para generar delays por software
// (hacer Ctrl + click sobre el nombre de la librer�a para ver su contenido)
#include "delays.hpp"

int main()
{
	// Configuramos el clock a 80Mhz usando el oscilador principal (oscilador externo de 16Mhz)
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
								 clock::configOptions::clockRate::clock_80Mhz);

	// Habilitamos el clock para el perif�rico GPIOF, lo cu�l nos permite configurar sus registros.
	Gpio::enableClock(Gpio::peripheral::GPIOF);

	// Configuramos el pin PF2 como Gpio y en modo salida
	PF2::enableAsDigital();// Habilitamos el pin con funci�n digital (no la anal�gica, en caso la haya tenido)
	PF2::setMode(Gpio::options::mode::gpio);// Configuramos el modo digital a GPIO (no alterno, siendo estos 15 modos)
	PF2::setIOmode(Gpio::options::IOmode::output);// Configuramos el pin como salida

	while(1)
	{
		// Toogleamos el LED cada 500 milisegundos
		PF2::toogle();
		delays::delay_ms(500);
	}
}
