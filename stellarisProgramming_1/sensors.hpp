/*
 * sensors.hpp
 *
 *  Created on: Aug 21, 2013
 *      Author: wilbert
 */


#include "include/SPI.hpp"
#include "include/I2C.h"
#include "include/Gpio.hpp"

namespace sensors
{
	namespace ivensense
	{

		namespace mpu9150
		{

			namespace axis
			{
				enum _axis
				{
					xAxis = 0,
					yAxis = 2,
					zAxis = 4
				};
			}

			namespace configOptions
			{
				namespace sampleRate
				{
					enum _sampleRate
					{
						sample_1000hz 	= 7,
						sample_500hz	= 15
					};
				}

				namespace digitalLPF
				{
					enum _digitalLPF
					{
						opt0 = 0,
						opt1 = 1,
						opt2 = 2,
						opt3 = 3,
						opt4 = 4,
						opt5 = 5,
						opt6 = 6
					};
				}

				namespace gyroScale
				{
					enum _gyroScale
					{
						scale_250dps	= 0,
						scale_500dps	= 1,
						scale_1000dps	= 2,
						scale_2000dps	= 3
					};
				}

				namespace accelScale
				{
					enum _accelScale
					{
						scale_2g 	= 0,
						scale_4g 	= 1,
						scale_8g 	= 2,
						scale_16g 	= 3
					};
				}

				namespace accelDHPF
				{
					enum _accelDHPF
					{
						none 			= 0,
						cutoff_5_00hz 	= 1,
						cutoff_2_50hz 	= 2,
						cutoff_1_25hz 	= 3,
						cutoff_0_63hz 	= 4,
						hold = 7
					};
				}

				namespace tempSensor
				{
					enum _tempSensor
					{
						enabled 	= 8,
						disabled	= 0
					};
				}
				namespace clockSel
				{
					enum _clockSel
					{
						internal_8Mhz 		= 0,
						pll_xaxis_gyro_ref	= 1,
						pll_yaxis_gyro_ref	= 2,
						pll_zaxis_gyro_ref	= 3
					};
				}

			}


			template<i2c::module::_module module_>
			class sensor
			{
				public:

				static inline u8 readRegister(u8 reg)
				{
					u8 data;
					i2c::i2cMod<module_>::loadSlaveAddress(0x68,i2c::options::rw::write);
					i2c::i2cMod<module_>::loadDataRegister(reg);
					i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::runMasterEnable|
													   i2c::configOptions::operation::startCondition);
					while(i2c::i2cMod<module_>::isBusBusy());
					if(i2c::i2cMod<module_>::isThereAnError())
					{
						while(1);//check errors....
					}
					i2c::i2cMod<module_>::loadSlaveAddress(0x68,i2c::options::rw::read);
					i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::runMasterEnable|
													   i2c::configOptions::operation::startCondition|
													   i2c::configOptions::operation::stopCondition);
					while(i2c::i2cMod<module_>::isBusBusy());
					if(i2c::i2cMod<module_>::isThereAnError())
					{
						while(1);//check errors....
					}

					data = i2c::i2cMod<module_>::getDataRegister();
					/*i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::stopCondition);
					while(i2c::i2cMod<module_>::isBusBusy());
					if(i2c::i2cMod<module_>::isThereAnError())
					{
						while(1);//check errors....
					}*/
					return data;
				}

				static inline void writeReg(u8 reg,u8 val)
				{
					i2c::i2cMod<module_>::loadSlaveAddress(0x68,
														   i2c::options::rw::write);
					i2c::i2cMod<module_>::loadDataRegister(reg);
					i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::runMasterEnable|
													   i2c::configOptions::operation::startCondition);
					while(i2c::i2cMod<module_>::isBusBusy());
					if(i2c::i2cMod<module_>::isThereAnError())
					{
						while(1);//check errors....
					}
					i2c::i2cMod<module_>::loadDataRegister(val);
					i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::runMasterEnable|
													   i2c::configOptions::operation::stopCondition);
					while(i2c::i2cMod<module_>::isBusBusy());
					if(i2c::i2cMod<module_>::isThereAnError())
					{
						while(1);//check errors....
					}
				}

				static inline void writeMagReg(u8 reg,u8 val)
				{
					i2c::i2cMod<module_>::loadSlaveAddress(0x0c,
														   i2c::options::rw::write);
					i2c::i2cMod<module_>::loadDataRegister(reg);
					i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::runMasterEnable|
													   i2c::configOptions::operation::startCondition);
					while(i2c::i2cMod<module_>::isBusBusy());
					if(i2c::i2cMod<module_>::isThereAnError())
					{
						while(1);//check errors....
					}
					i2c::i2cMod<module_>::loadDataRegister(val);
					i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::runMasterEnable|
													   i2c::configOptions::operation::stopCondition);
					while(i2c::i2cMod<module_>::isBusBusy());
					if(i2c::i2cMod<module_>::isThereAnError())
					{
						while(1);//check errors....
					}
				}

				static inline u8 readMagReg(u8 reg)
				{
					u8 data;
					i2c::i2cMod<module_>::loadSlaveAddress(0x0c,i2c::options::rw::write);
					i2c::i2cMod<module_>::loadDataRegister(reg);
					i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::runMasterEnable|
													   i2c::configOptions::operation::startCondition);
					while(i2c::i2cMod<module_>::isBusBusy());
					if(i2c::i2cMod<module_>::isThereAnError())
					{
						while(1);//check errors....
					}
					i2c::i2cMod<module_>::loadSlaveAddress(0x0c,i2c::options::rw::read);
					i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::runMasterEnable|
													   i2c::configOptions::operation::startCondition|
													   i2c::configOptions::operation::stopCondition);
					while(i2c::i2cMod<module_>::isBusBusy());
					if(i2c::i2cMod<module_>::isThereAnError())
					{
						while(1);//check errors....
					}

					data = i2c::i2cMod<module_>::getDataRegister();
					/*i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::stopCondition);
					while(i2c::i2cMod<module_>::isBusBusy());
					if(i2c::i2cMod<module_>::isThereAnError())
					{
						while(1);//check errors....
					}*/
					return data;
				}

				static inline void config(configOptions::sampleRate::_sampleRate sampleRate_,
										  configOptions::digitalLPF::_digitalLPF dpf,
										  configOptions::gyroScale::_gyroScale gyroScale_,
										  configOptions::accelScale::_accelScale accelScale_,
										  configOptions::accelDHPF::_accelDHPF accelDHPF_,
										  configOptions::tempSensor::_tempSensor tempSensor_,
										  configOptions::clockSel::_clockSel clockSel_)
				{
					u8 regVal = 0;
					if(dpf == configOptions::digitalLPF::opt0)
					{
						regVal = sampleRate_;
					}
					else
					{
						regVal = (sampleRate_+1)/8 - 1;
					}
					sensors::ivensense::mpu9150::sensor<module_>::writeReg(25,regVal);
					regVal = dpf;
					sensors::ivensense::mpu9150::sensor<module_>::writeReg(26,regVal);
					regVal = (u8)(gyroScale_<<3);
					sensors::ivensense::mpu9150::sensor<module_>::writeReg(27,regVal);
					regVal = (u8)(accelScale_<<3 | accelDHPF_);
					sensors::ivensense::mpu9150::sensor<module_>::writeReg(28,regVal);
					regVal = (u8)(tempSensor_<<3 | clockSel_);
					sensors::ivensense::mpu9150::sensor<module_>::writeReg(107,regVal);
					regVal = (u8)(0x2);
					sensors::ivensense::mpu9150::sensor<module_>::writeReg(55,regVal);
					regVal = (u8)(1);
					sensors::ivensense::mpu9150::sensor<module_>::writeReg(10,regVal);

				}
				static inline i16 readGyro(axis::_axis axis_)
				{
					u8 buff[2];
					buff[0] = sensors::ivensense::mpu9150::sensor<module_>::readRegister(67+axis_);
					buff[1] = sensors::ivensense::mpu9150::sensor<module_>::readRegister(68+axis_);
					return (i16)(buff[0]<<8 | buff[1]);

				}
				static inline i16 readAccel(axis::_axis axis_)
				{
					u8 buff[2];
					buff[0] = sensors::ivensense::mpu9150::sensor<module_>::readRegister(59+axis_);
					buff[1] = sensors::ivensense::mpu9150::sensor<module_>::readRegister(60+axis_);
					return (i16)(buff[0]<<8 | buff[1]);
				}

				static inline void startMag()
				{
					sensors::ivensense::mpu9150::sensor<module_>::writeReg(55,2);
					sensors::ivensense::mpu9150::sensor<module_>::writeMagReg(10,1);
					while(sensors::ivensense::mpu9150::sensor<module_>::readMagReg(2) != 1);
				}

				static inline i16 readMagnt(axis::_axis axis_)
				{
					u8 buff[2];
					//sensors::ivensense::mpu9150::sensor<module_>::writeReg(10,1);
					//while(sensors::ivensense::mpu9150::sensor<module_>::readRegister(2) != 1);
					buff[0] = sensors::ivensense::mpu9150::sensor<module_>::readMagReg(3+axis_);
					buff[1] = sensors::ivensense::mpu9150::sensor<module_>::readMagReg(4+axis_);
					return (i16)(buff[1]<<8 | buff[0]);
				}
			};
		}




	}



	namespace temperature
	{
		namespace tmp102
		{
			namespace pointerReg
			{
				enum _pointerReg
				{
					temperatureRegister 	= 0x00,
					configurationRegister	= 0x01,
					TlowRegister			= 0x02,
					ThighRegister			= 0x03
				};
			}

			namespace configOptions
			{
				namespace nBits
				{
					enum _nBits
					{
						bits_13 = 0x0010,
						bits_12 = 0
					};
				}
				namespace conversionRate
				{
					enum _conversionRate
					{
						rate_0_25Hz = 0,
						rate_1_00Hz = 0x0040,
						rate_4_00Hz = 0x0080,
						rate_8_00Hz = 0x00c0
					};
				}
				namespace shutdown
				{
					enum _shutdown
					{
						no 	= 0,
						yes = 0x0100
					};
				}
				namespace termostatMode
				{
					enum _termostatMode
					{
						comparator_mode = 0,
						interrupt_mode	= 0x0200
					};
				}
				namespace oneShot
				{
					enum _oneShot
					{
						useOneShotConversion 		= 0x8000,
						dontUseOneShotConversion	= 0
					};
				}
			}

			template<i2c::module::_module module_>
			class sensor
			{
				public:

				static inline void configSensor(configOptions::nBits::_nBits nBits_,
												configOptions::conversionRate::_conversionRate conversionRate_,
												configOptions::shutdown::_shutdown shutdown_,
												configOptions::termostatMode::_termostatMode termostatMode_,
												configOptions::oneShot::_oneShot oneShot_)
				{
					u16 configVal = nBits_ | conversionRate_ | shutdown_ | termostatMode_ | oneShot_;
					i2c::i2cMod<module_>::loadSlaveAddress(0x48,
														   i2c::options::rw::write);
					i2c::i2cMod<module_>::loadDataRegister(pointerReg::configurationRegister);
					i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::runMasterEnable|
																		   i2c::configOptions::operation::startCondition);
					while(i2c::i2cMod<module_>::isBusBusy());
					if(i2c::i2cMod<module_>::isThereAnError())
					{
						while(1);//check the error
					}
					i2c::i2cMod<module_>::loadDataRegister((u8)((configVal>>8)&(0x00ff)));
					i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::runMasterEnable);
					while(i2c::i2cMod<module_>::isBusBusy());
					if(i2c::i2cMod<module_>::isThereAnError())
					{
						while(1);//check the error
					}
					i2c::i2cMod<module_>::loadDataRegister((u8)((configVal>>0)&(0x00ff)));
					i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::runMasterEnable|
													   i2c::configOptions::operation::stopCondition);
					while(i2c::i2cMod<module_>::isBusBusy());
					if(i2c::i2cMod<module_>::isThereAnError())
					{
						while(1);//check the error
					}
				}

				static inline u16 getConfigRegister()
				{
					u8 buff[2];
					i2c::i2cMod<module_>::loadSlaveAddress(0x48,
														   i2c::options::rw::write);
					i2c::i2cMod<module_>::loadDataRegister(pointerReg::configurationRegister);
					i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::runMasterEnable|
													   i2c::configOptions::operation::startCondition|
													   i2c::configOptions::operation::stopCondition);
					while(i2c::i2cMod<module_>::isBusBusy());
					if(i2c::i2cMod<module_>::isThereAnError())
					{
						while(1);//check the error
					}
					i2c::i2cMod<module_>::loadSlaveAddress(0x48,
														   i2c::options::rw::read);
					i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::runMasterEnable|
													   i2c::configOptions::operation::startCondition|
													   i2c::configOptions::operation::generateACK);
					while(i2c::i2cMod<module_>::isBusBusy());
					if(i2c::i2cMod<module_>::isThereAnError())
					{
						while(1);//check the error
					}
					buff[0] = i2c::i2cMod<module_>::getDataRegister();
					i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::runMasterEnable|
													   i2c::configOptions::operation::generateACK|
													   i2c::configOptions::operation::stopCondition);
					while(i2c::i2cMod<module_>::isBusBusy());
					if(i2c::i2cMod<module_>::isThereAnError())
					{
						while(1);//check the error
					}
					buff[1] = i2c::i2cMod<module_>::getDataRegister();
					return (u16)(((buff[0]<<8)|(buff[1])));
				}

				static inline i16 getTemperature()
				{
					u8 buff[2];
					i2c::i2cMod<module_>::loadSlaveAddress(0x48,
														   i2c::options::rw::write);
					i2c::i2cMod<module_>::loadDataRegister(pointerReg::temperatureRegister);
					i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::runMasterEnable|
													   i2c::configOptions::operation::startCondition|
													   i2c::configOptions::operation::stopCondition);
					while(i2c::i2cMod<module_>::isBusBusy());
					if(i2c::i2cMod<module_>::isThereAnError())
					{
						while(1);//check the error
					}
					i2c::i2cMod<module_>::loadSlaveAddress(0x48,
														   i2c::options::rw::read);
					i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::runMasterEnable|
													   i2c::configOptions::operation::startCondition|
													   i2c::configOptions::operation::generateACK);
					while(i2c::i2cMod<module_>::isBusBusy());
					if(i2c::i2cMod<module_>::isThereAnError())
					{
						while(1);//check the error
					}
					buff[0] = i2c::i2cMod<module_>::getDataRegister();
					i2c::i2cMod<module_>::setOperation(i2c::configOptions::operation::runMasterEnable|
													   i2c::configOptions::operation::generateACK|
													   i2c::configOptions::operation::stopCondition);
					while(i2c::i2cMod<module_>::isBusBusy());
					if(i2c::i2cMod<module_>::isThereAnError())
					{
						while(1);//check the error
					}
					buff[1] = i2c::i2cMod<module_>::getDataRegister();
					return (i16)(((buff[0]<<8)|(buff[1]))>>4);
				}
			};
		}



	}



	namespace chRobotics
	{


		namespace configOptions{
			enum{
				eulerAngleTransmissionEnabled = 4194304,
				quaternionTransmissionEnabled = 8388608,
				proccesedMagnetometerEnabled = 16777216,
				proccesedAccelerometerEnabled = 33554432,
				proccesedGyroscopeEnabled = 67108864,
				rawMagnetometerEnabled = 134217728,
				rawAccelerometerEnabled = 268435456,
				rawGyroscopeEnabled = 536870912
			};
		}
		namespace miscOptions{
			enum{
				quaternionEstimationEnabled = 268435456,
				gyroCalibrationEnabled = 536870912,
				EKFusesAcceletometer = 1073741824,
				EKFusesMagnetometer = 2147483648
			};
		}


		namespace registers
		{
			enum _registers
			{
				configRegister = 0x0000,
				miscConfigRegister = 0x0001,
				rawGyroxy = 0x0056,
				rawGyroz  = 0x0057,
				rawAccelxy = 0x0058,
				rawAccelz  = 0x0059,
				rawMagnetxy = 0x005a,
				rawMagnetz  = 0x005b,
				proccesedGyroxy = 0x005c,
				proccesedGyroz  = 0x005d,
				proccesedAccelxy = 0x005e,
				proccesedAccelz  = 0x005f,
				proccesedMagnetxy = 0x0060,
				proccesedMagnetz  = 0x0061,
				eulerRollPitch = 0x0062,
				eulerYaw       = 0x0063,
				quatAB = 0x0064,
				quatCD = 0x0065
			};
		}
		template<spi::module::_module spiMod,Gpio::Port::_Port port,Gpio::Pin::_Pin pin>
		class chRoboticsIMU
		{
			public:


				static inline void writeRegister(registers::_registers reg,
												 u32 data)
				{
					//spi::spi<spiMod>::
					//GpioDataRegs.GPACLEAR.bit.GPIO12 = 1;
					Gpio::gpio<port,pin>::setLow();
					spi::spi<spiMod>::sendByte(0x01);
					spi::spi<spiMod>::sendByte(reg);
					spi::spi<spiMod>::sendByte((u32)((data&0xff000000)>>24));
					spi::spi<spiMod>::sendByte((u32)((data&0x00ff0000)>>16));
					spi::spi<spiMod>::sendByte((u32)((data&0x0000ff00)>>8));
					spi::spi<spiMod>::sendByte((u32)((data&0x000000ff)>>0));
					Gpio::gpio<port,pin>::setHigh();
					//GpioDataRegs.GPASET.bit.GPIO12 = 1;
				}
				static inline u32 readRegister(registers::_registers reg)
				{
					u32 buffer = 0;
					//GpioDataRegs.GPACLEAR.bit.GPIO12 = 1;
					Gpio::gpio<port,pin>::setLow();
					spi::spi<spiMod>::sendByte(0x00);
					spi::spi<spiMod>::sendByte(reg);
					spi::spi<spiMod>::sendByte(0x00);
					buffer |= (spi::spi<spiMod>::readByte())<<24;
					spi::spi<spiMod>::sendByte(0x00);
					buffer |= (spi::spi<spiMod>::readByte())<<16;
					spi::spi<spiMod>::sendByte(0x00);
					buffer |= (spi::spi<spiMod>::readByte())<<8;
					spi::spi<spiMod>::sendByte(0x00);
					buffer |= (spi::spi<spiMod>::readByte())<<0;
					Gpio::gpio<port,pin>::setHigh();
					//GpioDataRegs.GPASET.bit.GPIO12 = 1;
					return buffer;
				}
				static inline void config()
				{
					writeRegister(registers::configRegister,
								  configOptions::eulerAngleTransmissionEnabled|
								  configOptions::proccesedAccelerometerEnabled|
								  configOptions::proccesedGyroscopeEnabled|
								  configOptions::proccesedMagnetometerEnabled|
								  configOptions::quaternionTransmissionEnabled|
								  configOptions::rawAccelerometerEnabled|
								  configOptions::rawGyroscopeEnabled|
								  configOptions::rawMagnetometerEnabled);
					writeRegister(registers::miscConfigRegister,
								  miscOptions::quaternionEstimationEnabled|
								  miscOptions::gyroCalibrationEnabled|
								  miscOptions::EKFusesAcceletometer|
								  miscOptions::EKFusesMagnetometer);

				}
		};
	}




}
