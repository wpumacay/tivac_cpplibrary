/*
 * analogSensors.hpp
 *
 *  Created on: Sep 30, 2013
 *      Author: wilbert
 */

#include "include/Adc.h"
#include "include/Gpio.hpp"

namespace analogSensors
{


	template<adc::converter::_converter converter_,
			 adc::sequencer::_sequencer sequencer_,
			 adc::configOptions::inputpin::_inputpin inputPin_,
			 Gpio::Port::_Port port,
			 Gpio::Pin::_Pin pin>
	class analogSensor
	{
	protected:
		double vmax;
	public:
		analogSensor()
		{
			this->vmax = 3.3;
		}

		void config(double vmax,adc::configOptions::sampleRate::_sampleRate sampleRate_)
		{
			this->vmax = vmax;
			Gpio::gpio<port,pin>::setMode(Gpio::options::mode::alternate);
			Gpio::gpio<port,pin>::disableDigitalFunc();
			Gpio::gpio<port,pin>::setAnalogMode();
			adc::enableClock(converter_);
			adc::setSampleRate(converter_,sampleRate_);
			adc::adcMod<converter_,sequencer_>::disableSequencer();
			adc::adcMod<converter_,sequencer_>::selectTrigger(adc::configOptions::trigger::software);
			adc::adcMod<converter_,sequencer_>::sampleConfigure(adc::configOptions::sample::sample_1st,
																inputPin_,
																adc::configOptions::tempsensor::ommitTempSensor,
																adc::configOptions::interruptEnable::sampleIntEnable,
																adc::configOptions::endOfSequence::isEndOfSample,
																adc::configOptions::differentialMode::ommitDiffMode);
			adc::adcMod<converter_,sequencer_>::enableSequencer();
		}
		u16 takeSample()
		{
			u16 buff[1];
			adc::adcMod<converter_,sequencer_>::clearInterrupt();
			adc::adcMod<converter_,sequencer_>::softBeginSampling();
			while(!adc::adcMod<converter_,sequencer_>::finishedSampling());
			adc::adcMod<converter_,sequencer_>::takeData(buff);
			return buff[0];
		}
	};

	template<adc::converter::_converter converter_,
			 adc::sequencer::_sequencer sequencer_,
			 adc::configOptions::inputpin::_inputpin inputPin_,
			 Gpio::Port::_Port port,
			 Gpio::Pin::_Pin pin>
	class potentiometer : public analogSensor<converter_,sequencer_,inputPin_,port,pin>
	{
	public:

		double voltage;

		double getVoltage()
		{
			u16 volt_u16 = this->takeSample();
			this->voltage =(((double)(volt_u16))/4095.0)*this->vmax;
			return this->voltage;
		}
	};


	template<adc::converter::_converter converter_,
			 adc::sequencer::_sequencer sequencer_,
			 adc::configOptions::inputpin::_inputpin inputPin_,
			 Gpio::Port::_Port port,
			 Gpio::Pin::_Pin pin>
	class lm35 : public analogSensor<converter_,sequencer_,inputPin_,port,pin>
	{
	public:
		double getTemperature()
		{
			u16 volt_u16 = this->takeSample();
			double voltage = (((double)(volt_u16))/4095.0)*this->vmax;
			return ((voltage*1000.0)/10.0);
		}
	};

	template<adc::converter::_converter converter_,
			 adc::sequencer::_sequencer sequencer_,
			 adc::configOptions::inputpin::_inputpin inputPin_,
			 Gpio::Port::_Port port,
			 Gpio::Pin::_Pin pin>
	class ldr : public potentiometer<converter_,sequencer_,inputPin_,port,pin>
	{
	public:

	};



}
