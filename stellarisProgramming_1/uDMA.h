/*
 * uDMA.h
 *
 *  Created on: Aug 17, 2013
 *      Author: wilbert
 */

#ifndef UDMA_H_
#define UDMA_H_

#include "memorymap/mapClock.hpp"
#include "mapUdma.h"

#pragma DATA_ALIGN(1024)
unsigned char ucControlTable[1024];



namespace uDMA
{



	struct controlStructure
	{
		u32 sourceEndPointer;
		u32 destinationEndPointer;
		u32 controlWord;
		u32 reserved;
	};

	namespace channels
	{
		enum _channel
		{
			channel0	= 0,
			channel1	= 1,
			channel2	= 2,
			channel3	= 3,
			channel4	= 4,
			channel5	= 5,
			channel6	= 6,
			channel7	= 7,
			channel8	= 8,
			channel9	= 9,
			channel10	= 10,
			channel11	= 11,
			channel12	= 12,
			channel13	= 13,
			channel14	= 14,
			channel15	= 15,
			channel16	= 16,
			channel17	= 17,
			channel18	= 18,
			channel19	= 19,
			channel20	= 20,
			channel21	= 21,
			channel22	= 22,
			channel23	= 23,
			channel24	= 24,
			channel25	= 25,
			channel26	= 26,
			channel27	= 27,
			channel28	= 28,
			channel29	= 29,
			channel30	= 30,
			channel31	= 31
		};
	}

	namespace configOptions
	{
		namespace typeTransfer
		{
			enum _typeTransfer
			{
				memory2memory = 0,
				memory2peripheral = 1,
				peripheral2memory = 2
			};
		}
	}

	inline void enableClock()
	{
		reinterpret_cast<mapClock::ClockRegs *>
			(mapClock::moduleAddresses::clockAddress)
			->RCGCDMA = 1;
	}

	template<channels::_channel channel>
	class uDMA
	{
		public:

			static inline void enableChannel()
			{
				reinterpret_cast<mapudma::uDMAregs *>
					(mapudma::udmaAddress::dmaRegsAddress)
					->DMAENASET |= 1<<channel;
			}
			static inline void disableChannel()
			{
				reinterpret_cast<mapudma::uDMAregs *>
					(mapudma::udmaAddress::dmaRegsAddress)
					->DMAENACLR |= 1<<channel;
			}

			static inline void config(void *dataAddress,
									void *peripheralAddress,
									unsigned int numOfBytes,
									unsigned int mapAssignment,
									configOptions::typeTransfer::_typeTransfer typeTransfer_)
			{


				reinterpret_cast<mapudma::uDMAregs *>
					(mapudma::udmaAddress::dmaRegsAddress)
					->DMACFG = 1;//enable the uDMA controller
				//set the address of the structure control table
				reinterpret_cast<mapudma::uDMAregs *>
					(mapudma::udmaAddress::dmaRegsAddress)
					->DMACTLBASE = (u32)(&ucControlTable);

				if (channel<=channels::channel7)
				{
					reinterpret_cast<mapudma::uDMAregs *>
						(mapudma::udmaAddress::dmaRegsAddress)
						->DMACHMAP0 |= (mapAssignment<<((channel-8*0)*4));
				}
				else if(channel<=channels::channel15)
				{
					reinterpret_cast<mapudma::uDMAregs *>
						(mapudma::udmaAddress::dmaRegsAddress)
						->DMACHMAP1 |= (mapAssignment<<((channel-8*1)*4));
				}
				else if(channel<=channels::channel23)
				{
					reinterpret_cast<mapudma::uDMAregs *>
						(mapudma::udmaAddress::dmaRegsAddress)
						->DMACHMAP2 |= (mapAssignment<<((channel-8*2)*4));
				}
				else if(channel<=channels::channel31)
				{
					reinterpret_cast<mapudma::uDMAregs *>
						(mapudma::udmaAddress::dmaRegsAddress)
						->DMACHMAP3 |= (mapAssignment<<((channel-8*3)*4));
				}

				reinterpret_cast<mapudma::uDMAregs *>
					(mapudma::udmaAddress::dmaRegsAddress)
					->DMAPRIOSET |= 1<<channel;

				reinterpret_cast<mapudma::uDMAregs *>
					(mapudma::udmaAddress::dmaRegsAddress)
					->DMAALTCLR |= 1<<channel;

				reinterpret_cast<mapudma::uDMAregs *>
					(mapudma::udmaAddress::dmaRegsAddress)
					->DMAUSEBURSTCLR |= 1<<channel;

				reinterpret_cast<mapudma::uDMAregs *>
					(mapudma::udmaAddress::dmaRegsAddress)
					->DMAREQMASKCLR |= 1<<channel;

				switch(typeTransfer_)
				{
					case configOptions::typeTransfer::memory2memory :

					break;
					case configOptions::typeTransfer::memory2peripheral :
						reinterpret_cast<controlStructure *>
							((u32)(&ucControlTable) + channel*16)
							->sourceEndPointer = (u32)(dataAddress) + numOfBytes - 1;

						reinterpret_cast<controlStructure *>
							((u32)(&ucControlTable) + channel*16)
							->destinationEndPointer = (u32)(peripheralAddress);

						reinterpret_cast<controlStructure *>
							((u32)(&ucControlTable) + channel*16)
							->controlWord = (0x3<<30) | (0<<28) | (0<<26) | (0<<24) | (0x2<<14) | ((numOfBytes-1)<<4) | (0<<3) | (1);
					break;
					case configOptions::typeTransfer::peripheral2memory :
						reinterpret_cast<controlStructure *>
							((u32)(&ucControlTable) + channel*16)
							->destinationEndPointer = (u32)(dataAddress) + numOfBytes - 1;

						reinterpret_cast<controlStructure *>
							((u32)(&ucControlTable) + channel*16)
							->sourceEndPointer = (u32)(peripheralAddress);

						reinterpret_cast<controlStructure *>
							((u32)(&ucControlTable) + channel*16)
							->controlWord = (0x0<<30) | (0<<28) | (0x3<<26) | (0<<24) | (0x1<<14) | ((numOfBytes-1)<<4) | (0<<3) | (1);
					break;
				}


			}

			static inline void startTransfer()
			{
				reinterpret_cast<mapudma::uDMAregs *>
					(mapudma::udmaAddress::dmaRegsAddress)
					->DMAENASET |= 1<<channel;
			}
			static inline bool isTransferCompleted()
			{
				return (((reinterpret_cast<mapudma::uDMAregs *>
							(mapudma::udmaAddress::dmaRegsAddress)
							->DMACHIS)>>channel)&0x00000001 == 1);
			}
			static inline void clearFlag()
			{
				reinterpret_cast<mapudma::uDMAregs *>
					(mapudma::udmaAddress::dmaRegsAddress)
					->DMACHIS |= 1<<channel;
			}
	};
}


#endif /* UDMA_H_ */
