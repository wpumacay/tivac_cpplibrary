/*
 * ledBar.hpp
 *
 *  Created on: 04/10/2013
 *      Author: wilbert
 */
#include "include/Gpio.hpp"


//he creado una clase similar a lo del GPIO q usabamos..
//esta desperdicia memoria xD...
//guardadndo el pin y el puerto...
//pero ser� �til...

class simplePin
{
public:
	Gpio::Pin::_Pin pin;
	Gpio::Port::_Port port;

	simplePin()
	{

	}

	simplePin(Gpio::Pin::_Pin pin,Gpio::Port::_Port port)
	{
		this->pin = pin;
		this->port = port;
	}
	void config()
	{
		reinterpret_cast<mapGpio::GpioRegs *>
			(port)->GPIODEN |= 1<<pin;
		reinterpret_cast<mapGpio::GpioRegs *>
			(port)->GPIOAFSEL &= (0xffff - 1<<pin);
		reinterpret_cast<mapGpio::GpioRegs *>
			(port)->GPIODIR |= 1<<pin;
	}
	void setHigh()
	{
		reinterpret_cast<mapGpio::GpioRegs *>
			(port+0x000003fc)->GPIODATA |= 1<<pin;
	}
	void setLow()
	{
		reinterpret_cast<mapGpio::GpioRegs *>
			(port+0x000003fc)->GPIODATA &= (0x00ff -  1<<pin);
	}
	bool isHigh()
	{
		return ((reinterpret_cast<mapGpio::GpioRegs *>
						(port+0x000003fc)->GPIODATA & (1<<pin)) == (1<<pin));
	}
	void toogle()
	{
		if(isHigh())
		{
			setLow();
		}
		else
		{
			setHigh();
		}
	}
};


//tmb cree una clase para manejar una barra de leds, basada en la clase anterior...
class ledbar
{
public:


	simplePin pins[10];//array de objetos... cada elemento es un objeto de la clase simplePin
	u8 current;//la posici�n actual del led prendido... lo entender�n m�s adelante

	ledbar(Gpio::Port::_Port port1,Gpio::Pin::_Pin pin1,
			   Gpio::Port::_Port port2,Gpio::Pin::_Pin pin2,
			   Gpio::Port::_Port port3,Gpio::Pin::_Pin pin3,
			   Gpio::Port::_Port port4,Gpio::Pin::_Pin pin4,
			   Gpio::Port::_Port port5,Gpio::Pin::_Pin pin5,
			   Gpio::Port::_Port port6,Gpio::Pin::_Pin pin6,
			   Gpio::Port::_Port port7,Gpio::Pin::_Pin pin7,
			   Gpio::Port::_Port port8,Gpio::Pin::_Pin pin8,
			   Gpio::Port::_Port port9,Gpio::Pin::_Pin pin9,
			   Gpio::Port::_Port port10,Gpio::Pin::_Pin pin10)
	{//constructor!!!
		pins[0].pin = pin1;
		pins[0].port = port1;
		pins[1].pin = pin2;
		pins[1].port = port2;
		pins[2].pin = pin3;
		pins[2].port = port3;
		pins[3].pin = pin4;
		pins[3].port = port4;
		pins[4].pin = pin5;
		pins[4].port = port5;
		pins[5].pin = pin6;
		pins[5].port = port6;
		pins[6].pin = pin7;
		pins[6].port = port7;
		pins[7].pin = pin8;
		pins[7].port = port8;
		pins[8].pin = pin9;
		pins[8].port = port9;
		pins[9].pin = pin10;
		pins[9].port = port10;
		current = 0;
	}

	void init()
	{
		u8 i;
		for(i=0;i<10;i++)
		{
			pins[i].config();
			pins[i].setLow();
		}
	}

	void step()
	{
		u8 i;
		for(i=0;i<10;i++)
		{
			pins[i].setLow();
		}
		pins[current].setHigh();
		current = (current + 1)%10;
	}

	void funkyStep()
	{
		u8 i;
		for(i=0;i<10;i++)
		{
			pins[i].setLow();
		}
		pins[current].setHigh();
		pins[9-current].setHigh();
		current = (current + 1)%10;
	}
	//veamos q hace xD
};


