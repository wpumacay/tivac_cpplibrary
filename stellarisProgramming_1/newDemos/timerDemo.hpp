/*
 * timerDemo.hpp
 *
 *  Created on: 10/10/2013
 *      Author: wilbert
 */
#include "include/Gptimer.hpp"
#include "include/Gpio.hpp"
#include "include/Core.hpp"
#include "include/clock.hpp"
#include "interrupts.hpp"

int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
								 clock::configOptions::clockRate::clock_80Mhz);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	//configurando el timer....
	timer0::enableClock();
	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);//vamos a usar el subtimer A del timer0
	timer0::config(gpTimer::subTimers::subTimerA,
						  500,
						  gpTimer::options::timerOptions::joiningMode::timers_joinTimers,//usaremos los subtimers juntos para poder obtener un s�lo timer de 32 bits... para llegar a 500 ms...
						  gpTimer::options::timerOptions::timerMode::periodic,//usemos el modo peri�dico
						  gpTimer::options::timerOptions::countDirection::down,
						  gpTimer::options::timerOptions::interrupts::timerAtimeout);
	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);
	//timerWide0->timer0a_wide
	core::IntEnableMaster();//enable general
	core::enableInterrupt(core::interrupts::timer0a_simple);


	while(1)
	{

	}
}


//usemos el prototipo de interrupci�n ...
void interruptFuncs::timer0SubA_isr()
{
	//limpiamos la flag de interrupci�n
	timer0::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);
	//hacemos algo....
	PF2::toogle();
	//ok, listo...
	//no hemos incluido el header interrupts.hpp, en donde est� este prototipo...
	//ok, eso es todo... a probar....
}
//veamos los errores...
