//usando el ADC ... y un potenci�metro


#include "include/clock.hpp"
#include "include/Gpio.hpp"
#include "include/Adc.h"
#include "delays.hpp"


//son 2 muestras a tomar...
u16 buffADC[2] = {0,0};
//TENGO UN POTENCI�METRO CONECTADO AL STELLARIS.... 0 A 3.3V....
//voy a bajar el voltaje poco a poco.... -> 0v 	-> 0
//ahora a subir el voltaje....					-> 3.3v	-> 4095
//cada convertidor tiene una resoluci�n de 12 bits... -> 2^12-1 -> 4095
int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
								 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOE);//puerto del pin PE2 y PE3... ain1 y ain0 del ADC

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);
	//CONFIGURANDO EL PIN PE3 (AIN0-analog input 0) del ADC
	PE3::setMode(Gpio::options::mode::alternate);
	PE3::disableDigitalFunc();
	PE3::setAnalogMode();

	//configurando el m�dulo adc....
	adc::enableClock(adc::converter::adc0);
	adc::setSampleRate(adc::converter::adc0,adc::configOptions::sampleRate::sample_1msps);
	//ahora... el stellaris tiene 2 convertidores (m�dulos) ADC...
	//estos a su vez tienen 4 sequenciadores cada uno... secuenciador0 al secuenciador3
	//estos secuenciadores son los responsables de controlar a los convertidores...
	//piden muestras, inician conversiones, finalizan conversiones...
	//en nuestro caso usaremos el convertidor 0 (adc0) y el secuenciador 0
	adc0seq0::disableSequencer();//deshabilitamos el secuenciador para poder configurarlo
	adc0seq0::selectTrigger(adc::configOptions::trigger::software);//elegimos el trigger/iniciador de la toma de muestras
	//iniciaremos la toma de muestras por software, seteando un bit en un registro del adc0
	//vamos a configurar las muestras que va a tomar el secuenciador 0
	//este puede configurar/almacenar hasta 8 muestras, el seq1 hasta 4, el seq2 hasta 2 y el seq3 s�lo 1 muestra
	adc0seq0::sampleConfigure(adc::configOptions::sample::sample_1st,//esta es la primera muestra a tomar
											 adc::configOptions::inputpin::ain0,//vamos a tomar una muestra del pin anal�gico 0 (ain0 = pe3)
											 adc::configOptions::tempsensor::ommitTempSensor,//el secuenciador puede tomar esta muestra del sensor de temperatura interno del microcontorlador, en este caso no usaremos el sensor de temperatura
											 adc::configOptions::interruptEnable::sampleIntDisabled,//esta muestra no va activar una flag al culminar de procesarse...
											 adc::configOptions::endOfSequence::isntEndOfSample,//en este ejemplo vamos a tomar 2 muestras con el secuenciador 0, y del mismo pin... por eso esta muestra no es la �ltima muestra....
											 adc::configOptions::differentialMode::ommitDiffMode);//omitimos el modo diferencial.... el gndA de referencia es el gnd del micro, y el vccA de referencia es el vcc del micro (3.3v)
	//ahora configuremos la siguiente muestra
	adc0seq0::sampleConfigure(adc::configOptions::sample::sample_2nd,//esta es la segunda muestra a tomar
											 adc::configOptions::inputpin::ain0,//vamos a tomar esta muestra del mismo pin.. aunque pudimos haber elegido otro.... o tmb el sensor de temperatura interno
											 adc::configOptions::tempsensor::ommitTempSensor,//tampoco usamos el sensor de temperatura
											 adc::configOptions::interruptEnable::sampleIntEnable,//esta muestra SI va activar una flag al culminar de procesarse... esto nos servir� para hacer polling y chequear que se haya acabado la conversi�n de las 2 muestras....
											 adc::configOptions::endOfSequence::isEndOfSample,//esta es la �ltima muestra de las 2 que vamos a tomar del pin.... por lo que seteamos que esta es la �ltima...
											 adc::configOptions::differentialMode::ommitDiffMode);//omitimos el modo diferencial.... el gndA de referencia es el gnd del micro, y el vccA de referencia es el vcc del micro (3.3v)
	//listo, ahora habilitemos el secuenciador...
	adc0seq0::enableSequencer();

	while(1)
	{
		//se puede observar que en el buffer se almacenar estas 2 muestras....
		//esto permite hacer resampling.... ok... eso es todo....
		PF2::toogle();
		//iniciamos la conversi�n....
		adc0seq0::softBeginSampling();//esta funci�n escribe un 1 en un bit de un registro del adc0 para iniciar la conversi�n y toma de muestras...
		//hacemos polling para chequear que se haya acabado de tomar las muestras... liberamos cuando se haya acabado
		while(!adc0seq0::finishedSampling());//esta funci�n devuelve true o false en caso se haya acabado de tomar todas las muestras configuradas para el secuenciador
		adc0seq0::takeData(buffADC);//esta funci�n devuelve las muestras tomadas.... tenemos que pasarle un array
		adc0seq0::clearInterrupt();//limpiamos las flags para poder volver a hacer polling en la siguiente toma de muestras...
		delays::delay_ms(100);//un peque�o delay....
	}//listo, a probar....
}
