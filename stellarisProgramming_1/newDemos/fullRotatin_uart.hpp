#include "include/Gpio.hpp"
#include "include/clock.hpp"
#include "delays.hpp"
#include "include/Gptimer.hpp"
#include "include/Uart.h"
#include "interrupts.hpp"
#include "include/Core.hpp"

float duty = 0.075;
u16 speed = 0;

enum direc
{
	horaria 		= 0,
	antihoraria 	= 1
};

direc direccion;
u8 data = 0;
bool go = false;

int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
								   clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOC);
	Gpio::enableClock(Gpio::peripheral::GPIOA);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PA0::enableAsDigital();
	PA0::setMode(Gpio::options::mode::alternate);
	PA0::setAlternateMode(Gpio::options::altModes::alt1);

	PA1::enableAsDigital();
	PA1::setMode(Gpio::options::mode::alternate);
	PA1::setAlternateMode(Gpio::options::altModes::alt1);

	UART0::enableClock();
	UART0::configUart(uart::);

	PC6::enableAsDigital();
	PC6::setMode(Gpio::options::mode::alternate);
	PC6::setAlternateMode(Gpio::options::altModes::alt7);

	pwmW_1A::enableClock();
	pwmW_1A::config(20,0.075,gpTimer::options::pwmOptions::outputLevel::inverted);

	core::IntEnableMaster();
	core::enableInterrupt(core::interrupts::uart0);

	while(1)
	{

		unsigned int foo = direccion*2 + 5*antihoraria;

		if(go)
		{
			foo = 2*foo;
			switch (data) {
				case 'w'://aumentar velocidad
					if(direccion == horaria)
					{
						duty += 0.0015;
						UART0::sendString("velocidad: ");
						speed = (u16)((duty-0.075)/0.015 * 100.0);
						UART0::sendUintNum(speed);
						UART0::sendString("\r\n");
						UART0::sendString("direcci�n: horaria\n\r");
					}
					else if(direccion == antihoraria)
					{
						duty -= 0.0015;
						UART0::sendString("velocidad: ");
						speed = (u16)((0.075-duty)/0.015 * 100.0);
						UART0::sendUintNum(speed);
						UART0::sendString("\r\n");
						UART0::sendString("direcci�n: antihoraria\n\r");
					}
					break;
				case 's'://disminuir velocidad
					if(direccion == horaria)
					{
						duty -= 0.0015;
						UART0::sendString("velocidad: ");
						speed = (u16)((duty-0.075)/0.015*100.0);
						UART0::sendUintNum(speed);
						UART0::sendString("\r\n");
						UART0::sendString("direcci�n: horaria\n\r");
					}
					else if(direccion == antihoraria)
					{
						duty += 0.0015;
						UART0::sendString("velocidad: ");
						speed = (u16)((0.075-duty)/0.015*100.0);
						UART0::sendUintNum(speed);
						UART0::sendString("\r\n");
						UART0::sendString("direcci�n: antihoraria\n\r");
					}
					break;
				case 'a'://rotaci�n horaria
					if(direccion == horaria)
					{
						duty = 0.075 - (duty-0.075);
						UART0::sendString("direcci�n cambiada a antihoraria\n\r");
						direccion = antihoraria;
					}
					else if(direccion == antihoraria)
					{
						UART0::sendString("la direcci�n ya es antihoraria\n\r");
					}
					break;
				case 'd'://rotaci�n antihoraria
					if(direccion == horaria)
					{
						UART0::sendString("la direcci�n ya es horaria\n\r");
					}
					else if(direccion == antihoraria)
					{
						duty = 0.075 - (duty-0.075);
						UART0::sendString("direcci�n cambiada a horaria\n\r");
						direccion = horaria;
					}
					break;
			}
			pwmW_1A::setDuty(duty);
			PF2::toogle();
			go = false;
		}
	}
}


void interruptFuncs::uart0rxtx_isr()
{
	UART0::clearInterrupts(uart::configOptions::interrupts::receiveInt);
	data = UART0::receiveByte();
	go = true;
}
