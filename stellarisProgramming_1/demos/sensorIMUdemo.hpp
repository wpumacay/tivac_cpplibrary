/*
 * main.cpp
 */



//nuestras libs
#include "Core.h"
#include "SysCtrl.h"
#include "include/Gpio.hpp"
#include "interrupts.hpp"
#include "clock.h"
#include "include/Gptimer.hpp"
#include "Adc.h"
#include "SPI.h"
#include "sensors.hpp"



typedef Gpio::gpio<Gpio::Port::GPIOPortF_APB,Gpio::Pin::pin1> ledr;
typedef Gpio::gpio<Gpio::Port::GPIOPortF_APB,Gpio::Pin::pin2> ledb;
typedef Gpio::gpio<Gpio::Port::GPIOPortF_APB,Gpio::Pin::pin3> ledg;
typedef Gpio::gpio<Gpio::Port::GPIOPortF_APB,Gpio::Pin::pin4> spi0ChipSelect;
typedef Gpio::gpio<Gpio::Port::GPIOPortA_APB,Gpio::Pin::pin2> spi0Clock;
typedef Gpio::gpio<Gpio::Port::GPIOPortA_APB,Gpio::Pin::pin3> spi0FrameSignal;
typedef Gpio::gpio<Gpio::Port::GPIOPortA_APB,Gpio::Pin::pin4> spi0Rx;
typedef Gpio::gpio<Gpio::Port::GPIOPortA_APB,Gpio::Pin::pin5> spi0Tx;



typedef gpTimer::gpTimerMod<gpTimer::timer0_simple> timer0;
typedef spi::spi<spi::module::spi0> spi0;
typedef sensors::chRobotics::chRoboticsIMU
		<spi::module::spi0,
		 Gpio::Port::GPIOPortF_APB,
		 Gpio::Pin::pin4> imu;


unsigned long long foo = 0;
unsigned int voltage[1] = {0};
unsigned long buffer[50];

u32 dummy[11];
float yaw = 0.0;

int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz,
						 clock::configOptions::enablePIOSC::enablePinternalOsc,
						 clock::configOptions::enableMOSC::enableMainOsc);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOA);

	ledr::enableAsDigital();
	ledr::setMode(Gpio::options::mode::gpio);
	ledr::setIOmode(Gpio::options::IOmode::output);

	ledb::enableAsDigital();
	ledb::setMode(Gpio::options::mode::gpio);
	ledb::setIOmode(Gpio::options::IOmode::output);

	ledg::enableAsDigital();
	ledg::setMode(Gpio::options::mode::gpio);
	ledg::setIOmode(Gpio::options::IOmode::output);

	spi0ChipSelect::enableAsDigital();
	spi0ChipSelect::setMode(Gpio::options::mode::gpio);
	spi0ChipSelect::setIOmode(Gpio::options::IOmode::output);
	//spi0ChipSelect::setPullUp();
	spi0ChipSelect::setHigh();

	spi0Clock::enableAsDigital();
	spi0Clock::setMode(Gpio::options::mode::alternate);
	spi0Clock::setAlternateMode(Gpio::options::altModes::alt2);

	spi0Rx::enableAsDigital();
	spi0Rx::setMode(Gpio::options::mode::alternate);
	spi0Rx::setAlternateMode(Gpio::options::altModes::alt2);

	spi0Tx::enableAsDigital();
	spi0Tx::setMode(Gpio::options::mode::alternate);
	spi0Tx::setAlternateMode(Gpio::options::altModes::alt2);

	spi0::enableClock();
	spi0::config(spi::configOptions::mode::master,
				 spi::configOptions::clockRate::spiclock_200khz,
				 spi::configOptions::clockPolarity::low,
				 spi::configOptions::clockPhase::capSecondClock,
				 spi::configOptions::frameFormat::freescale,
				 spi::configOptions::dataSize::bits_8);

	imu::config();
	dummy[0] = imu::readRegister(sensors::
			                     chRobotics::
			                     registers::configRegister);
	dummy[1] = imu::readRegister(sensors::
				                     chRobotics::
				                     registers::miscConfigRegister);


	timer0::enableClock();
	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);
	timer0::config(gpTimer::subTimers::subTimerA,
				   100,
				   gpTimer::options::timerOptions::joiningMode::timers_joinTimers,
				   gpTimer::options::timerOptions::timerMode::periodic,
				   gpTimer::options::timerOptions::countDirection::down,
				   gpTimer::options::timerOptions::interrupts::timerAtimeout);

	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);
	sysCtrl::IntEnableMaster();
	core::enableInterrupt(core::interrupts::timer0a_simple);



	while(1){

	}

}



void interruptFuncs::timer0SubA_isr()
{
	timer0::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);
	foo++;
	switch(foo%2)
	{
		case 0:
			ledr::setHigh();
			ledg::setLow();
		break;
		case 1:
			ledr::setLow();
			ledg::setHigh();
		break;
	}
	dummy[2] = imu::readRegister(sensors::
				                 chRobotics::
				                 registers::proccesedAccelz);
	yaw = (float)((dummy[2]>>16)*(0.000183105));
}

