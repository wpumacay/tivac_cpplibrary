/*
 * main.cpp
 */



//nuestras libs
#include "Core.h"
#include "SysCtrl.h"
#include "include/Gpio.hpp"
#include "interrupts.hpp"
#include "clock.h"
#include "include/Gptimer.hpp"
#include "include/Adc.h"
#include "include/Uart.h"


typedef Gpio::gpio<Gpio::Port::GPIOPortF_APB,Gpio::Pin::pin1> ledr;
typedef Gpio::gpio<Gpio::Port::GPIOPortF_APB,Gpio::Pin::pin2> ledb;
typedef Gpio::gpio<Gpio::Port::GPIOPortF_APB,Gpio::Pin::pin3> ledg;
typedef Gpio::gpio<Gpio::Port::GPIOPortE_APB,Gpio::Pin::pin3> joystick_x;
typedef Gpio::gpio<Gpio::Port::GPIOPortA_APB,Gpio::Pin::pin0> u0rx;
typedef Gpio::gpio<Gpio::Port::GPIOPortA_APB,Gpio::Pin::pin1> u0tx;

typedef gpTimer::gpTimerMod<gpTimer::timer0_simple> timer0;
typedef adc::adcMod<adc::converter::adc0,adc::sequencer::sequencer0> adc0Seq0;
typedef uart::uartMod<uart::module::uart0> port;

unsigned long long foo = 0;
bool switchPressed = false;
unsigned int buffer[1] = {0};
unsigned char buffRx = 0;


int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz,
						 clock::configOptions::enablePIOSC::enablePinternalOsc,
						 clock::configOptions::enableMOSC::enableMainOsc);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOE);
	Gpio::enableClock(Gpio::peripheral::GPIOA);

	ledr::enableAsDigital();
	ledr::setMode(Gpio::options::mode::gpio);
	ledr::setIOmode(Gpio::options::IOmode::output);

	ledb::enableAsDigital();
	ledb::setMode(Gpio::options::mode::gpio);
	ledb::setIOmode(Gpio::options::IOmode::output);

	ledg::enableAsDigital();
	ledg::setMode(Gpio::options::mode::gpio);
	ledg::setIOmode(Gpio::options::IOmode::output);

	joystick_x::setMode(Gpio::options::mode::alternate);
	joystick_x::disableDigitalFunc();
	joystick_x::setAnalogMode();

	adc::enableClock(adc::converter::adc0);
	adc::setSampleRate(adc::converter::adc0,adc::configOptions::sampleRate::sample_1msps);
	adc0Seq0::disableSequencer();
	adc0Seq0::selectTrigger(adc::configOptions::trigger::software);
	adc0Seq0::sampleConfigure(adc::configOptions::sample::sample_1st,
							  adc::configOptions::inputpin::ain0,
							  adc::configOptions::tempsensor::ommitTempSensor,
							  adc::configOptions::interruptEnable::sampleIntEnable,
							  adc::configOptions::endOfSequence::isEndOfSample,
							  adc::configOptions::differentialMode::ommitDiffMode);
	adc0Seq0::enableSequencer();

	u0rx::enableAsDigital();
	u0rx::setMode(Gpio::options::mode::alternate);
	u0rx::setAlternateMode(Gpio::options::altModes::alt1);

	u0tx::enableAsDigital();
	u0tx::setMode(Gpio::options::mode::alternate);
	u0tx::setAlternateMode(Gpio::options::altModes::alt1);


	port::enableClock();
	port::configUart(uart::configOptions::baudrate::baud_115200,
					 uart::configOptions::nbits::bits_8,
					 uart::configOptions::parity::none,
					 uart::configOptions::stopbits::stopBits_one,
					 //uart::configOptions::interrupts::receiveTimeOut|
					 uart::configOptions::interrupts::receiveInt,
					 uart::configOptions::fifo::fifoDisabled,
					 uart::clockSource::pioscClock);

	foo = std::strlen("hola");

	timer0::enableClock();
	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);
	timer0::config(gpTimer::subTimers::subTimerA,
				   250,
				   gpTimer::options::timerOptions::joiningMode::timers_joinTimers,
				   gpTimer::options::timerOptions::timerMode::periodic,
				   gpTimer::options::timerOptions::countDirection::down,
				   gpTimer::options::timerOptions::interrupts::timerAtimeout);

	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);
	sysCtrl::IntEnableMaster();
	core::enableInterrupt(core::interrupts::timer0a_simple);
	core::enableInterrupt(core::interrupts::uart0);

	while(1)
	{

	}
}

void interruptFuncs::uart0rxtx_isr()
{
	port::clearInterrupts(uart::configOptions::interrupts::receiveInt);
	buffRx = port::receiveByte();
	port::sendString("hola xD!!!");
	ledg::toogle();
}

void interruptFuncs::timer0SubA_isr()
{
	timer0::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);
	foo++;
	ledb::toogle();
	adc0Seq0::clearInterrupt();
	adc0Seq0::softBeginSampling();
	while(!adc0Seq0::finishedSampling())
	{
	}
	adc0Seq0::takeData(buffer);//value in the range [0-4096] <> [0.0 - 3.3V]
}
