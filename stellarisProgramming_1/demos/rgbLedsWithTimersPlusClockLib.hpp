/*
 * main.cpp
 */
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/timer.h"

#include "interrupts.hpp"
#include "Gpio.h"
#include "include/Gptimer.hpp"
#include "SysCtrl.h"
#include "Core.h"
#include "clock.h"

typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio1> led1;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio2> led2;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio3> led3;

typedef gpTimer::timer<gpTimer::timer0_simple,
					   gpTimer::options::operationMode::oneshot_or_periodic> timer0;



unsigned int count = 0;
unsigned int foo = 0;


int main(void) {

	//SysCtlClockSet(SYSCTL_SYSDIV_2_5|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz,
						 clock::configOptions::enablePIOSC::enablePinternalOsc,
						 clock::configOptions::enableMOSC::enableMainOsc);
	//enable the gpio port related to the rgb leds
	//SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOF);
	//Gpio::enableClock(Gpio::peripheral::GPIOE);

	//RGB LEDS configuration******************************************************************
	led1::setIOmode(mapGpio::options::IOmode::output);
	led2::setIOmode(mapGpio::options::IOmode::output);
	led3::setIOmode(mapGpio::options::IOmode::output);

	led1::enableAsDigital();
	led2::enableAsDigital();
	led3::enableAsDigital();
	//****************************************************************************************
	//TIMER0 configuration********************************************************************
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
	foo = timer0::configTimer(gpTimer::subTimers::subTimerA,
						      1000,
						      gpTimer::options::joiningMode::timers_joinTimers,
						      gpTimer::options::timerMode::periodic,
						      gpTimer::options::countDirection::down,
						      gpTimer::options::interrupts::timerAtimeout);

	//INTERRUPTS configuration****************************************************************
	core::enableInterrupt(core::interrupts::timer0a_simple);
	foo = sysCtrl::IntEnableMaster();
	//****************************************************************************************
	while(1)
	{

	}
}


void interruptFuncs::timer0SubA_isr()
{
	TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
	count++;
	switch(count%3)
	{
		case 0:
			led1::setHigh();
			led2::setLow();
			led3::setLow();
			break;
		case 1:
			led2::setHigh();
			led1::setLow();
			led3::setLow();
			break;
		case 2:
			led3::setHigh();
			led2::setLow();
			led1::setLow();
			break;
	}
}
