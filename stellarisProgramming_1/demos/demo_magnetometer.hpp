/*
 * main.cpp
 */



//nuestras libs
#include "Core.h"
#include "SysCtrl.h"
#include "Gpio.h"
#include "Uart.h"
#include "interrupts.hpp"
#include "include/Gptimer.hpp"
//del stellarisware
#include "driverlib/sysctl.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/timer.h"
#include "I2C.h"

typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio1> ledr;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio2> ledb;
typedef Gpio::gpio<mapGpio::GPIOPortF_APB,mapGpio::gpio3> ledg;
typedef Gpio::gpio<mapGpio::GPIOPortB_APB,mapGpio::gpio2> i2c0scl;
typedef Gpio::gpio<mapGpio::GPIOPortB_APB,mapGpio::gpio3> i2c0sda;

typedef Gpio::gpio<mapGpio::GPIOPortA_APB,mapGpio::gpio0> u0rx;
typedef Gpio::gpio<mapGpio::GPIOPortA_APB,mapGpio::gpio1> u0tx;


typedef gpTimer::timer<gpTimer::timer0_simple,
					   gpTimer::options::operationMode::oneshot_or_periodic>
					   timer0;

typedef uart::uart<uart::module::uart0> port;
typedef i2c::i2c<i2c::module::i2c0> i2c0;


unsigned int foo = 0;
unsigned char buffDataI2C[2] = {0x10,0x01};
unsigned char i2cReadBuffer = 0;

int magx;
int magy;
int magz;

unsigned char var = 0;

int main()
{
	SysCtlClockSet(SYSCTL_SYSDIV_2_5|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);

	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);//enable the gpio port related to i2c0 pins
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
//	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);

	ledr::enableAsDigital();
	ledr::setMode(mapGpio::options::mode::gpio);
	ledr::setIOmode(mapGpio::options::IOmode::output);

	ledb::enableAsDigital();
	ledb::setMode(mapGpio::options::mode::gpio);
	ledb::setIOmode(mapGpio::options::IOmode::output);

	ledg::enableAsDigital();
	ledg::setMode(mapGpio::options::mode::gpio);
	ledg::setIOmode(mapGpio::options::IOmode::output);


	SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);
	i2c0scl::enableAsDigital();
	i2c0scl::setMode(mapGpio::options::mode::alternate);
	i2c0scl::setAlternateMode(mapGpio::options::altModes::alt3);
	i2c0scl::setPullUp();

	i2c0sda::enableAsDigital();
	i2c0sda::setMode(mapGpio::options::mode::alternate);
	i2c0sda::setAlternateMode(mapGpio::options::altModes::alt3);
	i2c0sda::setOpenDrain();

	i2c0::config(i2c::configOptions::mode::master,
				 i2c::configOptions::loopback::disabled,
				 i2c::configOptions::speed::standarMode);

	u0rx::enableAsDigital();
	u0rx::setMode(mapGpio::options::mode::alternate);
	u0rx::setAlternateMode(mapGpio::options::altModes::alt1);

	u0tx::enableAsDigital();
	u0tx::setMode(mapGpio::options::mode::alternate);
	u0tx::setAlternateMode(mapGpio::options::altModes::alt1);

	port::configUart(uart::configOptions::baudrate::baud_9600,
					 uart::configOptions::nbits::bits_8,
					 uart::configOptions::parity::none,
					 uart::configOptions::stopbits::stopBits_one,
					 uart::configOptions::interrupts::receiveInt,
					 uart::configOptions::fifo::fifoDisabled,
					 uart::clockSource::pioscClock);

/*	timer0::configTimer(gpTimer::subTimers::subTimerA,
						500,
						gpTimer::options::joiningMode::timers_joinTimers,
						gpTimer::options::timerMode::periodic,
						gpTimer::options::countDirection::down,
						gpTimer::options::interrupts::timerAtimeout);
*/
	//core::enableInterrupt(core::interrupts::timer0a_simple);
	core::enableInterrupt(core::interrupts::uart0);
	sysCtrl::IntEnableMaster();

	i2c0::send(0x0e,buffDataI2C,2);
	buffDataI2C[0] = 0x11;
	buffDataI2C[1] = 0x80;
	i2c0::send(0x0e,buffDataI2C,2);

	while(1){

	}

}

void interruptFuncs::uart0rxtx_isr()
{
	var = port::receiveByte();
	unsigned char low;
	unsigned char high;
	foo++;
	switch(foo%3)
	{
		case 0:
			ledr::setHigh();
			ledg::setLow();
			ledb::setLow();
		break;
		case 1:
			ledr::setLow();
			ledg::setHigh();
			ledb::setLow();
		break;
		case 2:
			ledr::setLow();
			ledg::setLow();
			ledb::setHigh();
		break;
	}
	if(var==114)
	{
		high = i2c0::readByteReg(0x0e,0x01);
		low = i2c0::readByteReg(0x0e,0x02);
		magx = (int)(((high<<8)&0xff00)|((low)&0x00ff));
		high = i2c0::readByteReg(0x0e,0x03);
		low = i2c0::readByteReg(0x0e,0x04);
		magy = (int)(((high<<8)&0xff00)|((low)&0x00ff));
		high = i2c0::readByteReg(0x0e,0x05);
		low = i2c0::readByteReg(0x0e,0x06);
		magz = (int)(((high<<8)&0xff00)|((low)&0x00ff));

		port::sendByte((magx>>8)&0x00ff);
		port::sendByte((magx>>0)&0x00ff);

		port::sendByte((magy>>8)&0x00ff);
		port::sendByte((magy>>0)&0x00ff);

		port::sendByte((magz>>8)&0x00ff);
		port::sendByte((magz>>0)&0x00ff);
	}
	port::clearInterrupts(uart::configOptions::interrupts::receiveInt);
}



/*
void interruptFuncs::timer0SubA_isr()
{
	TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
	foo++;
	switch(foo%3)
	{
		case 0:
			ledr::setHigh();
			ledg::setLow();
			ledb::setLow();
		break;
		case 1:
			ledr::setLow();
			ledg::setHigh();
			ledb::setLow();
		break;
		case 2:
			ledr::setLow();
			ledg::setLow();
			ledb::setHigh();
		break;
	}
	i2cReadBuffer = i2c0::readByteReg(0x0e,0x07);
}

*/
