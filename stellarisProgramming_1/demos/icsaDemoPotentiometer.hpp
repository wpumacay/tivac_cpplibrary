/*
 * main.cpp
 */



//nuestras libs
#include "Core.h"
#include "SysCtrl.h"
#include "include/Gpio.hpp"
#include "interrupts.hpp"
#include "clock.h"
#include "include/Gptimer.hpp"
#include "include/Adc.h"

typedef Gpio::gpio<Gpio::Port::GPIOPortF_APB,Gpio::Pin::pin1> ledr;
typedef Gpio::gpio<Gpio::Port::GPIOPortF_APB,Gpio::Pin::pin2> ledb;
typedef Gpio::gpio<Gpio::Port::GPIOPortF_APB,Gpio::Pin::pin3> ledg;
typedef Gpio::gpio<Gpio::Port::GPIOPortE_APB,Gpio::Pin::pin3> joystick_x;


typedef gpTimer::gpTimerMod<gpTimer::timer0_simple> timer0;
typedef adc::adcMod<adc::converter::adc0,adc::sequencer::sequencer0> adc0Seq0;

unsigned long long foo = 0;
bool switchPressed = false;
unsigned int buffer[1] = {0};

int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz,
						 clock::configOptions::enablePIOSC::enablePinternalOsc,
						 clock::configOptions::enableMOSC::enableMainOsc);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOE);

	ledr::enableAsDigital();
	ledr::setMode(Gpio::options::mode::gpio);
	ledr::setIOmode(Gpio::options::IOmode::output);

	ledb::enableAsDigital();
	ledb::setMode(Gpio::options::mode::gpio);
	ledb::setIOmode(Gpio::options::IOmode::output);

	ledg::enableAsDigital();
	ledg::setMode(Gpio::options::mode::gpio);
	ledg::setIOmode(Gpio::options::IOmode::output);

	joystick_x::setMode(Gpio::options::mode::alternate);
	joystick_x::disableDigitalFunc();
	joystick_x::setAnalogMode();

	adc::enableClock(adc::converter::adc0);
	adc::setSampleRate(adc::converter::adc0,adc::configOptions::sampleRate::sample_1msps);
	adc0Seq0::disableSequencer();
	adc0Seq0::selectTrigger(adc::configOptions::trigger::software);
	adc0Seq0::sampleConfigure(adc::configOptions::sample::sample_1st,
							  adc::configOptions::inputpin::ain0,
							  adc::configOptions::tempsensor::ommitTempSensor,
							  adc::configOptions::interruptEnable::sampleIntEnable,
							  adc::configOptions::endOfSequence::isEndOfSample,
							  adc::configOptions::differentialMode::selectDiffMode);
	adc0Seq0::enableSequencer();

	timer0::enableClock();
	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);
	timer0::config(gpTimer::subTimers::subTimerA,
				   250,
				   gpTimer::options::timerOptions::joiningMode::timers_joinTimers,
				   gpTimer::options::timerOptions::timerMode::periodic,
				   gpTimer::options::timerOptions::countDirection::down,
				   gpTimer::options::timerOptions::interrupts::timerAtimeout);

	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);
	sysCtrl::IntEnableMaster();
	core::enableInterrupt(core::interrupts::timer0a_simple);

	while(1)
	{

	}
}


void interruptFuncs::timer0SubA_isr()
{
	timer0::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);
	foo++;
	ledb::toogle();
	adc0Seq0::clearInterrupt();
	adc0Seq0::softBeginSampling();
	while(!adc0Seq0::finishedSampling())
	{
	}
	adc0Seq0::takeData(buffer);//value in the range [0-4096] <> [0.0 - 3.3V]
}
