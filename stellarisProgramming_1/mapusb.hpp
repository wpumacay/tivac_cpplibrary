/*
 * mapusb.hpp
 *
 *  Created on: Sep 12, 2013
 *      Author: wilbert
 */
#include "common.h"

namespace mapusb
{
	u32 usbModAddress = 0x40050000;


	struct USBREGS
	{
		u8 USBFADDR;//0x000
		u8 USBPOWER;//0x001
		u16 USBTXIS;//0x002 - 0x003 -> 16 bits
		u16 USBRXIS;//0x004 - 0x005 -> 16 bits
		u16 USBTXIE;//0x006 - 0x007 -> 16 bits
		u16 USBRXIE;//0x008 - 0x009 -> 16 bits
		u8 USBIS;//0x00a
		u8 USBIE;//0x00b
		u16 USBFRAME;//0x00c- 0x00d -> 16 bits
		u8 USBEPIDX;//0x00e
		u8 USBTEST;//0x00f

		u8 reserved8_1[16];//[0x010 - 0x019]

		u32 USBFIFO0;//0x020-0x021-0x022-0x023 -> 32 bits
		u32 USBFIFO1;//0x024-0x025-0x026-0x027 -> 32 bits
		u32 USBFIFO2;//0x028-0x029-0x02a-0x02b -> 32 bits
		u32 USBFIFO3;//0x02c-0x02d-0x02e-0x02f -> 32 bits
		u32 USBFIFO4;//0x030-0x031-0x032-0x033 -> 32 bits
		u32 USBFIFO5;//0x034-0x035-0x036-0x037 -> 32 bits
		u32 USBFIFO6;//0x038-0x039-0x03a-0x03b -> 32 bits
		u32 USBFIFO7;//0x03c-0x03d-0x03e-0x03f -> 32 bits

		u8 reserved8_2[34];//[0x040 - 0x061]

		u8 USBTXFIFOSZ;//0x062
		u8 USBRXFIFOSZ;//0x063
		u16 USBTXFIFOADD;//0x064 - 0x065 -> 16 bits
		u16 USBRXFIFOADD;//0x066 - 0x067 -> 16 bits

		u8 reserved8_3[18];//[0x068 - 0x079]

		u8 USBCONTIM;//0x07a
		u8 reserved8_4[2];//[0x07b - 0x07c]
		u8 USBFSEOF;//0x07d
		u8 USBLSEOF;//0x07e
		u8 reserved8_5[131];//[0x07f - 0x101]
		u8 USBCSRL0;//0x102
		u8 USBCSRH0;//0x103
		u8 reserved8_6[4];//[0x104 - 0x107]
		u8 USBCOUNT0;//0x108

		u8 reserved8_7[7];//[0x109 - 0x10f]

		u16 USBTXMAXP1;//0x110 - 0x111 -> 16 bits
		u8 USBTXCSRL1;//0x112
		u8 USBTXCSRH1;//0x113
		u16 USBRXMAXP1;//0x114 - 0x115 -> 16 bits
		u8 USBRXCSRL1;//0x116
		u8 USBRXCSRH1;//0x117
		u16 USBRXCOUNT1;//0x118 - 0x119 -> 16 bits

		u8 reserved8_8[6];//[0x11a - 0x11f]

		u16 USBTXMAXP2;//0x120 - 0x121 -> 16 bits
		u8 USBTXCSRL2;//0x122
		u8 USBTXCSRH2;//0x123
		u16 USBRXMAXP2;//0x124 - 0x125 -> 16 bits
		u8 USBRXCSRL2;//0x126
		u8 USBRXCSRH2;//0x127
		u16 USBRXCOUNT2;//0x128 - 0x129 -> 16 bits

		u8 reserved8_9[6];//[0x12a - 0x12f]

		u16 USBTXMAXP3;//0x130 - 0x131 -> 16 bits
		u8 USBTXCSRL3;//0x132
		u8 USBTXCSRH3;//0x133
		u16 USBRXMAXP3;//0x134 - 0x135 -> 16 bits
		u8 USBRXCSRL3;//0x136
		u8 USBRXCSRH3;//0x137
		u16 USBRXCOUNT3;//0x138 - 0x139 -> 16 bits

		u8 reserved8_10[6];//[0x13a - 0x13f]

		u16 USBTXMAXP4;//0x140 - 0x141 -> 16 bits
		u8 USBTXCSRL4;//0x142
		u8 USBTXCSRH4;//0x143
		u16 USBRXMAXP4;//0x144 - 0x145 -> 16 bits
		u8 USBRXCSRL4;//0x146
		u8 USBRXCSRH4;//0x147
		u16 USBRXCOUNT4;//0x148 - 0x149 -> 16 bits

		u8 reserved8_11[6];//[0x14a - 0x14f]

		u16 USBTXMAXP5;//0x150 - 0x151 -> 16 bits
		u8 USBTXCSRL5;//0x152
		u8 USBTXCSRH5;//0x153
		u16 USBRXMAXP5;//0x154 - 0x155 -> 16 bits
		u8 USBRXCSRL5;//0x156
		u8 USBRXCSRH5;//0x157
		u16 USBRXCOUNT5;//0x158 - 0x159 -> 16 bits

		u8 reserved8_12[6];//[0x15a - 0x15f]

		u16 USBTXMAXP6;//0x160 - 0x161 -> 16 bits
		u8 USBTXCSRL6;//0x162
		u8 USBTXCSRH6;//0x163
		u16 USBRXMAXP6;//0x164 - 0x165 -> 16 bits
		u8 USBRXCSRL6;//0x166
		u8 USBRXCSRH6;//0x167
		u16 USBRXCOUNT6;//0x168 - 0x169 -> 16 bits

		u8 reserved8_13[6];//[0x16a - 0x16f]

		u16 USBTXMAXP7;//0x170 - 0x171 -> 16 bits
		u8 USBTXCSRL7;//0x172
		u8 USBTXCSRH7;//0x173
		u16 USBRXMAXP7;//0x174 - 0x175 -> 16 bits
		u8 USBRXCSRL7;//0x176
		u8 USBRXCSRH7;//0x177
		u16 USBRXCOUNT7;//0x178 - 0x179 -> 16 bits

		u8 reserved8_14[454];//[0x17a - 0x33f]

		u16 USBRXDPKTBUFDIS;//0x340 - 0x341 -> 16 bits
		u16 USBTXDPKTBUFDIS;//0x342 - 0x343 -> 16 bits

		u8 reserved8_15[204];//[0x344 - 0x40f]

		u32 USBDRRIS;//0x410 - 0x411 - 0x412 - 0x413 -> 32 bits
		u32 USBDRIM;//0x414 - 0x415 - 0x416 - 0x417 -> 32 bits
		u32 USBDRISC;//0x418 - 0x419 - 0x41a - 0x41b -> 32 bits

		u8 reserved8_16[52];//[0x41c - 0x44f]

		u32 USBDMASEL;//0x450 - 0x451 - 0x452 - 0x453 -> 32 bits

		//u8 reserved8_17[2924];//[0x454 - 0xfbf]

		//u32 USBPP;//0xfc0 - 0xfc1 - 0xfc2 - 0xfc3 -> 32 bits

	};


	// check...
	//USB0DeviceIntHandler->			usblib/device/usbdhandler.c
	//USB0DeviceIntHandlerInternal->	usblib/device/usbdenum.c
	//USBIntStatusControl->				driverlib/usb.c


}
