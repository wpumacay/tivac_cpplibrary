#include "include/clock.hpp"
#include "include/Gpio.hpp"
#include "delays.hpp"
#include "interrupts.hpp"
#include "include/Core.hpp"
#include "include/Gptimer.hpp"
#include "linalg.hpp"
//#include "vectors.hpp"
#include "systemIdent.hpp"

//linalg::matrix A(2,2);
//linalg::matrix B(2,2);
//linalg::matrix C(2,2);

sysIdent::recursive::kalman::belief belief_t;
linalg::matrix A(2,2);
linalg::matrix C(1,2);
linalg::matrix R(2,2);
linalg::matrix Q(1,1);

unsigned int foo = 0;
double z = 0.0;
linalg::matrix zMatrix(1,1);

double checkedResult[4] = {0,0,0,0};

double checkedResult3[9];


int main()
{
	clock::clock::config(
			clock::configOptions::clockSource::mainOscilator,
			clock::configOptions::clockRate::clock_80Mhz,
			clock::configOptions::enablePIOSC::enablePinternalOsc,
			clock::configOptions::enableMOSC::enableMainOsc);

	Gpio::enableClock(Gpio::peripheral::GPIOF);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PF3::enableAsDigital();
	PF3::setMode(Gpio::options::mode::gpio);
	PF3::setIOmode(Gpio::options::IOmode::output);
/*
	*(A.buff + 0) = 1.0;
	*(A.buff + 1) = 1.0;
	*(A.buff + 2) = 1.0;
	*(A.buff + 3) = 1.0;

	*(B.buff + 0) = 1.0;
	*(B.buff + 1) = 2.0;
	*(B.buff + 2) = 3.0;
	*(B.buff + 3) = 4.0;
*/
	/*
	*(D.buff + 0) = 4.0;
	*(D.buff + 1) = 1.0;
	*(D.buff + 2) = 1.0;
	*(D.buff + 3) = 1.0;
	*(D.buff + 4) = 0.25;
	*(D.buff + 5) = 1.0;
	*(D.buff + 6) = 1.0;
	*(D.buff + 7) = 2.0;
	*(D.buff + 8) = 1.0;
*/
	//linalg::checkMatrix(A,checkedResult);
	//linalg::checkMatrix(B,checkedResult);
/*
	C = A*B;
	checkedResult[0] = C.getElement(0,0);
	checkedResult[1] = C.getElement(1,0);
	checkedResult[2] = C.getElement(0,1);
	checkedResult[3] = C.getElement(1,1);

	checkedResult[0] = A.getElement(0,0);
	checkedResult[1] = A.getElement(1,0);
	checkedResult[2] = A.getElement(0,1);
	checkedResult[3] = A.getElement(1,1);

	checkedResult[0] = B.getElement(0,0);
	checkedResult[1] = B.getElement(1,0);
	checkedResult[2] = B.getElement(0,1);
	checkedResult[3] = B.getElement(1,1);
	//linalg::checkMatrix(C,checkedResult);
	//linalg::checkMatrix(A,checkedResult);
	//linalg::checkMatrix(B,checkedResult);

	*(A.buff + 0) = 1.0;
	*(A.buff + 1) = 2.0;
	*(A.buff + 2) = -1.0;
	*(A.buff + 3) = -1.0;

	C = A.inv();
	checkedResult[0] = C.getElement(0,0);
	checkedResult[1] = C.getElement(1,0);
	checkedResult[2] = C.getElement(0,1);
	checkedResult[3] = C.getElement(1,1);
	//linalg::checkMatrix(C,checkedResult);
*/
	/*
	checkedResult3[0] = D.getElement(0,0);
	checkedResult3[1] = D.getElement(1,0);
	checkedResult3[2] = D.getElement(2,0);
	checkedResult3[3] = D.getElement(0,1);
	checkedResult3[4] = D.getElement(1,1);
	checkedResult3[5] = D.getElement(2,1);
	checkedResult3[6] = D.getElement(0,2);
	checkedResult3[7] = D.getElement(1,2);
	checkedResult3[8] = D.getElement(2,2);

	E = D.inv();
	checkedResult3[0] = E.getElement(0,0);
	checkedResult3[1] = E.getElement(1,0);
	checkedResult3[2] = E.getElement(2,0);
	checkedResult3[3] = E.getElement(0,1);
	checkedResult3[4] = E.getElement(1,1);
	checkedResult3[5] = E.getElement(2,1);
	checkedResult3[6] = E.getElement(0,2);
	checkedResult3[7] = E.getElement(1,2);
	checkedResult3[8] = E.getElement(2,2);
*/

	A = linalg::identity(A.nRows);
	*(C.buff + 0) = 1.0;
	*(C.buff + 1) = 1.0;


	belief_t.mu 	= linalg::matrix(2,1);
	belief_t.sigma	= linalg::matrix(2,2);
	*(belief_t.mu.buff + 0) = 0.0;
	*(belief_t.mu.buff + 1) = 0.0;
	*(belief_t.sigma.buff + 0) = 100.0;
	*(belief_t.sigma.buff + 1) = 0.0;
	*(belief_t.sigma.buff + 2) = 0.0;
	*(belief_t.sigma.buff + 3) = 100.0;

	*(Q.buff + 0) = 0.000001;



	timer0::enableClock();
	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);
	timer0::config(gpTimer::subTimers::subTimerA,
			       100,
			       gpTimer::options::timerOptions::joiningMode::timers_joinTimers,
			       gpTimer::options::timerOptions::timerMode::periodic,
			       gpTimer::options::timerOptions::countDirection::down,
			       gpTimer::options::timerOptions::interrupts::timerAtimeout);
	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);

	core::IntEnableMaster();
	core::enableInterrupt(core::interrupts::timer0a_simple);


	while(1)
	{

	}
}

void interruptFuncs::timer0SubA_isr()
{
	timer0::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);
	//E = D.inv();
	linalg::matrix D(3,3);
	linalg::matrix E(3,3);
	linalg::matrix F(3,3);



	*(D.buff + 0) = 4.0;
	*(D.buff + 1) = 1.0;
	*(D.buff + 2) = 1.0;
	*(D.buff + 3) = 1.0;
	*(D.buff + 4) = 0.25;
	*(D.buff + 5) = 1.0;
	*(D.buff + 6) = 1.0;
	*(D.buff + 7) = 2.0;
	*(D.buff + 8) = 1.0;

	*(E.buff + 0) = 4.0;
	*(E.buff + 1) = 1.0;
	*(E.buff + 2) = 1.0;
	*(E.buff + 3) = 1.0;
	*(E.buff + 4) = 0.25;
	*(E.buff + 5) = 1.0;
	*(E.buff + 6) = 1.0;
	*(E.buff + 7) = 2.0;
	*(E.buff + 8) = 1.0;

	F = D + E;
	F = E + D;
	F = E + D + E;
	E = D.inv();

	belief_t = sysIdent::recursive::kalman::predict(belief_t,A,R);
	belief_t = sysIdent::recursive::kalman::correct(belief_t,C,Q,zMatrix);

/*
	checkedResult3[0] = E.getElement(0,0);
	checkedResult3[1] = E.getElement(1,0);
	checkedResult3[2] = E.getElement(2,0);
	checkedResult3[3] = E.getElement(0,1);
	checkedResult3[4] = E.getElement(1,1);
	checkedResult3[5] = E.getElement(2,1);
	checkedResult3[6] = E.getElement(0,2);
	checkedResult3[7] = E.getElement(1,2);
	checkedResult3[8] = E.getElement(2,2);
*/
	PF2::toogle();
}


