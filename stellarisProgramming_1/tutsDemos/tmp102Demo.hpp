#include "include/clock.hpp"
#include "include/Gpio.hpp"
#include "delays.hpp"
#include "interrupts.hpp"
#include "include/Core.hpp"
#include "include/Gptimer.hpp"
#include "include/Uart.h"
#include "sensors.hpp"

typedef sensors::temperature::tmp102::sensor<i2c::module::i2c0> tempSensor;


u8 foo = 0;

double temperature;
i16 temperatureReg;
u16 temp_u16 = 0;

int main()
{
	clock::clock::config(
			clock::configOptions::clockSource::mainOscilator,
			clock::configOptions::clockRate::clock_80Mhz,
			clock::configOptions::enablePIOSC::enablePinternalOsc,
			clock::configOptions::enableMOSC::enableMainOsc);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOA);
	Gpio::enableClock(Gpio::peripheral::GPIOB);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PF3::enableAsDigital();
	PF3::setMode(Gpio::options::mode::gpio);
	PF3::setIOmode(Gpio::options::IOmode::output);

	PA0::enableAsDigital();
	PA0::setMode(Gpio::options::mode::alternate);
	PA0::setAlternateMode(Gpio::options::altModes::alt1);

	PA1::enableAsDigital();
	PA1::setMode(Gpio::options::mode::alternate);
	PA1::setAlternateMode(Gpio::options::altModes::alt1);

	UART0::enableClock();
	UART0::configUart(uart::configOptions::baudrate::baud_1000000,
			     	  uart::configOptions::nbits::bits_8,
			     	  uart::configOptions::parity::none,
			     	  uart::configOptions::stopbits::stopBits_one,
			     	  uart::configOptions::interrupts::receiveInt,
			     	  uart::configOptions::fifo::fifoDisabled,
			     	  uart::clockSource::systemClock);

	PB2::enableAsDigital();
	PB2::setMode(Gpio::options::mode::alternate);
	PB2::setAlternateMode(Gpio::options::altModes::alt3);
	PB2::setPullUp();

	PB3::enableAsDigital();
	PB3::setMode(Gpio::options::mode::alternate);
	PB3::setAlternateMode(Gpio::options::altModes::alt3);
	PB3::setOpenDrain();

	I2C0::enableClock();
	I2C0::config(i2c::configOptions::mode::master,
				 i2c::configOptions::loopback::disabled,
				 i2c::configOptions::speed::standarMode);
	tempSensor::configSensor(sensors::temperature::tmp102::configOptions::nBits::bits_12,
								 sensors::temperature::tmp102::configOptions::conversionRate::rate_8_00Hz,
								 sensors::temperature::tmp102::configOptions::shutdown::no,
								 sensors::temperature::tmp102::configOptions::termostatMode::comparator_mode,
								 sensors::temperature::tmp102::configOptions::oneShot::dontUseOneShotConversion);


	core::IntEnableMaster();
	core::enableInterrupt(core::interrupts::uart0);


	while(1)
	{
		PF3::toogle();
		delays::delay_ms(500);
	}
}

void interruptFuncs::uart0rxtx_isr()
{
	UART0::clearInterrupts(uart::configOptions::interrupts::receiveInt);
	foo = UART0::receiveByte();
	temperatureReg = tempSensor::getTemperature();
	temp_u16 = (u16)(temperatureReg);
	temp_u16 = temp_u16<<4;
	temperature = ((float)(temperatureReg))*0.0625;
	UART0::sendByte((unsigned char)((temp_u16>>8)&(0x00ff)));
	UART0::sendByte((unsigned char)((temp_u16>>0)&(0x00ff)));

	PF2::toogle();
}


