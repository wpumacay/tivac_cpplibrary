#include "include/clock.hpp"
#include "include/Gpio.hpp"
#include "delays.hpp"
#include "interrupts.hpp"
#include "include/Core.hpp"
#include "include/I2C.h"
#include "mcpDAC.hpp"
#include "include/Gptimer.hpp"
#include "include/Adc.h"
#include "linalg.hpp"
//#include "vectors.hpp"
#include "systemIdent.hpp"

sysIdent::recursive::kalman::belief belief_t;
linalg::matrix A(2,2);
linalg::matrix C(1,2);
linalg::matrix R(2,2);
linalg::matrix Q(1,1);

double z = 0.0;
linalg::matrix zMatrix(1,1);
bool rlsGo = false;

double a1 = 0.0;
double b1 = 0.0;
double y_k_1 = 0.0;
//double u_k_1 = 0.0;

typedef PB2 i2c0scl;
typedef PB3 i2c0sda;
typedef mcp4725::dac<i2c::module::i2c0> analogOutput;


u8 foo;
u32 fun = 0;
double vout = 0.0;
bool go;
unsigned short buffADC[1];
u16 voltageSet;

int main()
{
	clock::clock::config(
			clock::configOptions::clockSource::mainOscilator,
			clock::configOptions::clockRate::clock_80Mhz,
			clock::configOptions::enablePIOSC::enablePinternalOsc,
			clock::configOptions::enableMOSC::enableMainOsc);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOE);
	Gpio::enableClock(Gpio::peripheral::GPIOB);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PF3::enableAsDigital();
	PF3::setMode(Gpio::options::mode::gpio);
	PF3::setIOmode(Gpio::options::IOmode::output);

	PE3::setMode(Gpio::options::mode::alternate);
	PE3::disableDigitalFunc();
	PE3::setAnalogMode();

	adc::enableClock(adc::converter::adc0);
	adc::setSampleRate(adc::converter::adc0,adc::configOptions::sampleRate::sample_1msps);
	adc0seq0::disableSequencer();
	adc0seq0::selectTrigger(adc::configOptions::trigger::software);
	adc0seq0::sampleConfigure(adc::configOptions::sample::sample_1st,
							  adc::configOptions::inputpin::ain0,
							  adc::configOptions::tempsensor::ommitTempSensor,
							  adc::configOptions::interruptEnable::sampleIntEnable,
							  adc::configOptions::endOfSequence::isEndOfSample,
							  adc::configOptions::differentialMode::ommitDiffMode);
	adc0seq0::enableSequencer();


	i2c0scl::enableAsDigital();
	i2c0scl::setMode(Gpio::options::mode::alternate);
	i2c0scl::setAlternateMode(Gpio::options::altModes::alt3);
	i2c0scl::setPullUp();

	i2c0sda::enableAsDigital();
	i2c0sda::setMode(Gpio::options::mode::alternate);
	i2c0sda::setAlternateMode(Gpio::options::altModes::alt3);
	i2c0sda::setOpenDrain();

	I2C0::enableClock();
	I2C0::config(i2c::configOptions::mode::master,
				 i2c::configOptions::loopback::disabled,
				 i2c::configOptions::speed::standarMode);

	A = linalg::identity(A.nRows);
	*(C.buff + 0) = 1.0;
	*(C.buff + 1) = 1.0;


	belief_t.mu 	= linalg::matrix(2,1);
	belief_t.sigma	= linalg::matrix(2,2);
	*(belief_t.mu.buff + 0) = 0.0;
	*(belief_t.mu.buff + 1) = 0.0;
	*(belief_t.sigma.buff + 0) = 100.0;
	*(belief_t.sigma.buff + 1) = 0.0;
	*(belief_t.sigma.buff + 2) = 0.0;
	*(belief_t.sigma.buff + 3) = 100.0;

	*(Q.buff + 0) = 0.000001;

	timer0::enableClock();
	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);
	timer0::config(gpTimer::subTimers::subTimerA,
			       100,
			       gpTimer::options::timerOptions::joiningMode::timers_joinTimers,
			       gpTimer::options::timerOptions::timerMode::periodic,
			       gpTimer::options::timerOptions::countDirection::down,
			       gpTimer::options::timerOptions::interrupts::timerAtimeout);
	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);

	core::IntEnableMaster();
	core::enableInterrupt(core::interrupts::timer0a_simple);


	while(1)
	{
		if(rlsGo)
		{

		}
	}
}


void interruptFuncs::timer0SubA_isr()
{
	timer0::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);
	fun++;
	voltageSet = analogOutput::setVoltage(vout,3.3);
	adc0seq0::clearInterrupt();
	adc0seq0::softBeginSampling();
	while(!adc0seq0::finishedSampling());
	adc0seq0::takeData(buffADC);
	z = (((double)(buffADC[0]))/4095.0)*3.3;
	*(zMatrix.buff + 0) = z;
	*(C.buff + 0) = y_k_1;
	*(C.buff + 1) = vout;
	belief_t = sysIdent::recursive::kalman::predict(belief_t,A,R);
	belief_t = sysIdent::recursive::kalman::correct(belief_t,C,Q,zMatrix);
	a1 = *(belief_t.mu.buff + 0);
	b1 = *(belief_t.mu.buff + 1);
	y_k_1 = z;
	//rlsGo = false;
	PF3::toogle();
	//rlsGo = true;
	if(fun>100)
	{
		fun = 0;
	}

	if(fun>50)
	{
		vout = 2.0;
	}
	else
	{
		vout = 0.0;
	}
	/*
	switch(fun%2)
	{
		case 0:
			vout = 1.0;
		break;
		case 1:
			vout = 2.0;
		break;
	}*/
	PF2::toogle();
}


