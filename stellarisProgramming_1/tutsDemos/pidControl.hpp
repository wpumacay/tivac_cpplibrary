#include "include/clock.hpp"
#include "include/Gpio.hpp"
#include "interrupts.hpp"
#include "include/Gptimer.hpp"
#include "include/Core.hpp"
#include "delays.hpp"
#include "include/I2C.h"
#include "include/Adc.h"
#include "include/Uart.h"

typedef PB2 i2c0scl;
typedef PB3 i2c0sda;
typedef PE3 analogPin;
typedef adc0seq0 analogVolt;

unsigned short buffi2cval;
unsigned char buffDataI2C[2] = {0x0f,0xff};
unsigned int voltage[2]={0,0};
unsigned int count = 0;
bool fun = false;
bool go = false;

float x = 0.0;
float Kp = 1.0;
float Ki = 1.5;
float Kd = 1.0;
float e_t = 0.0;
float e_t_1 = 0.0;

float ep = 0.0;
float ei = 0.0;
float ed = 0.0;

float xd = 1.0;
float u = 0.0;

void initclock()
{
	clock::clock::config(
			clock::configOptions::clockSource::mainOscilator,
			clock::configOptions::clockRate::clock_80Mhz,
			clock::configOptions::enablePIOSC::enablePinternalOsc,
			clock::configOptions::enableMOSC::enableMainOsc);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOB);
	Gpio::enableClock(Gpio::peripheral::GPIOE);
	Gpio::enableClock(Gpio::peripheral::GPIOA);
}

void initGpio()
{
	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PF3::enableAsDigital();
	PF3::setMode(Gpio::options::mode::gpio);
	PF3::setIOmode(Gpio::options::IOmode::output);

	PA0::enableAsDigital();
	PA0::setMode(Gpio::options::mode::alternate);
	PA0::setAlternateMode(Gpio::options::altModes::alt1);

	PA1::enableAsDigital();
	PA1::setMode(Gpio::options::mode::alternate);
	PA1::setAlternateMode(Gpio::options::altModes::alt1);

	i2c0scl::enableAsDigital();
	i2c0scl::setMode(Gpio::options::mode::alternate);
	i2c0scl::setAlternateMode(Gpio::options::altModes::alt3);
	i2c0scl::setPullUp();

	i2c0sda::enableAsDigital();
	i2c0sda::setMode(Gpio::options::mode::alternate);
	i2c0sda::setAlternateMode(Gpio::options::altModes::alt3);
	i2c0sda::setOpenDrain();

	analogPin::setMode(Gpio::options::mode::alternate);
	analogPin::disableDigitalFunc();
	analogPin::setAnalogMode();
}

void initUart()
{
	UART0::enableClock();
	UART0::configUart(uart::configOptions::baudrate::baud_115200,
					  uart::configOptions::nbits::bits_8,
					  uart::configOptions::parity::none,
					  uart::configOptions::stopbits::stopBits_one,
					  uart::configOptions::interrupts::receiveInt,
					  uart::configOptions::fifo::fifoDisabled,
					  uart::clockSource::systemClock);
}

void initI2C()
{
	I2C0::enableClock();
	I2C0::config(i2c::configOptions::mode::master,
				 i2c::configOptions::loopback::disabled,
				 i2c::configOptions::speed::standarMode);
}

void initADC()
{

	adc::enableClock(adc::converter::adc0);
	adc::setSampleRate(adc::converter::adc0,adc::configOptions::sampleRate::sample_250ksps);

	analogVolt::disableSequencer();
	analogVolt::selectTrigger(adc::configOptions::trigger::software);
	analogVolt::sampleConfigure(adc::configOptions::sample::sample_1st,
								adc::configOptions::inputpin::ain0,
								adc::configOptions::tempsensor::ommitTempSensor,
								adc::configOptions::interruptEnable::sampleIntDisabled,
								adc::configOptions::endOfSequence::isntEndOfSample,
								adc::configOptions::differentialMode::ommitDiffMode);
	analogVolt::sampleConfigure(adc::configOptions::sample::sample_2nd,
								adc::configOptions::inputpin::ain0,
								adc::configOptions::tempsensor::ommitTempSensor,
								adc::configOptions::interruptEnable::sampleIntEnable,
								adc::configOptions::endOfSequence::isEndOfSample,
								adc::configOptions::differentialMode::ommitDiffMode);
	analogVolt::enableSequencer();
}

void initTimer()
{
	timer0::enableClock();
	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);
	timer0::config(gpTimer::subTimers::subTimerA,
				   2000,
				   gpTimer::options::timerOptions::joiningMode::timers_joinTimers,
				   gpTimer::options::timerOptions::timerMode::periodic,
				   gpTimer::options::timerOptions::countDirection::down,
				   gpTimer::options::timerOptions::interrupts::timerAtimeout);
	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);
	timer1::enableClock();
	timer1::disableSubTimer(gpTimer::subTimers::subTimerA);
	timer1::config(gpTimer::subTimers::subTimerA,
				   10,
				   gpTimer::options::timerOptions::joiningMode::timers_joinTimers,
				   gpTimer::options::timerOptions::timerMode::periodic,
				   gpTimer::options::timerOptions::countDirection::down,
				   gpTimer::options::timerOptions::interrupts::timerAtimeout);
	timer1::enableSubTimer(gpTimer::subTimers::subTimerA);
}

void enableInterrupts()
{
	core::enableInterrupt(core::interrupts::timer0a_simple);
	core::enableInterrupt(core::interrupts::timer1a_simple);
	core::enableInterrupt(core::interrupts::uart0);
	core::IntEnableMaster();
}

float pidController(float xval,float setpoint)
{
	float u = 0.0;
	e_t = setpoint - xval;
	ep = e_t;
	ei += 0.01*e_t;
	ed = (e_t - e_t_1) / 0.01;
	u = Kp*ep + Ki*ei + Kd*ed;
	if(u>3.3)
	{
		u=3.3;
	}
	else if(u<0.0)
	{
		u=0.0;
	}
	return u;
}

int main()
{
	initclock();
	initGpio();
	initUart();
	initI2C();
	initADC();
	initTimer();

	enableInterrupts();

	while(1)
	{
		if(go)
		{
			analogVolt::clearInterrupt();
			analogVolt::softBeginSampling();
			while(!analogVolt::finishedSampling())
			{
			}
			analogVolt::takeData(voltage);//value in the range [0-4096] <> [0.0 - 3.3V]
			x = ((float)(voltage[0])/4096.0)*3.3;
			u = pidController(x,xd);
			buffi2cval = (unsigned short)(((u/3.3)*4095.0));
			buffDataI2C[0] = (unsigned char)((buffi2cval>>8)&(0x000f));
			buffDataI2C[1] = (unsigned char)((buffi2cval>>0)&(0x00ff));
			//buffDataI2C[0] = 0x0f;
			//buffDataI2C[1] = 0xff;
			I2C0::send(0x60,buffDataI2C,2);
			go = false;
			PF2::toogle();
		}
	}
}

void interruptFuncs::uart0rxtx_isr()
{
	unsigned char foo = UART0::receiveByte();
	fun = true;
	UART0::clearInterrupts(uart::configOptions::interrupts::receiveInt);
	UART0::sendByte((unsigned char)((voltage[0]>>8)&(0x00ff)));
	UART0::sendByte((unsigned char)((voltage[0]>>0)&(0x00ff)));
	PF2::toogle();
}

void interruptFuncs::timer0SubA_isr()
{
	timer0::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);
	if(fun)
	{
		count++;
		switch(count%2)
		{
			case 0:
				xd = 2.0;
			break;
			case 1:
				xd = 1.0;
			break;
		}
	}
	PF3::toogle();
}

void interruptFuncs::timer1SubA_isr()
{

	timer1::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);
	go = true;
}
