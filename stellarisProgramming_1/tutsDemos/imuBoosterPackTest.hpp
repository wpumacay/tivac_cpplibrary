/*
 * imuBoosterPackTest.hpp
 *
 *  Created on: 24/09/2013
 *      Author: wilbert
 */
#include "include/clock.hpp"
#include "include/Gpio.hpp"
#include "delays.hpp"
#include "interrupts.hpp"
#include "include/Core.hpp"
#include "include/Gptimer.hpp"
#include "sensors.hpp"
#include "include/I2C.h"

#include "linalg.hpp"

unsigned long foo = 0;
i16 temperatureReg = 0;
float temperature = 0.0;
u16 configRegister = 0;

struct gyro
{
	i16 x;
	i16 y;
	i16 z;
};

struct accel
{
	i16 x;
	i16 y;
	i16 z;
};

struct magnt
{
	i16 x;
	i16 y;
	i16 z;
};


struct readings
{
	gyro gyroReading;
	accel accelReading;
	magnt magntReading;
};

readings imuReadings;


float gyroZ;
float accelX;
float accelY;
float accelZ;


typedef PD0 i2c0scl;
typedef PD1 i2c0sda;
//typedef sensors::temperature::tmp102::sensor<i2c::module::i2c0> tempSensor;
typedef sensors::ivensense::mpu9150::sensor<i2c::module::i2c3> imu;

u8 reg;

/*
linalg::vector a(3);

linalg::vector b(3);

linalg::vector c(3);
*/

int main()
{
	clock::clock::config(
			clock::configOptions::clockSource::mainOscilator,
			clock::configOptions::clockRate::clock_80Mhz,
			clock::configOptions::enablePIOSC::enablePinternalOsc,
			clock::configOptions::enableMOSC::enableMainOsc);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOD);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PF3::enableAsDigital();
	PF3::setMode(Gpio::options::mode::gpio);
	PF3::setIOmode(Gpio::options::IOmode::output);



	i2c0scl::enableAsDigital();
	i2c0scl::setMode(Gpio::options::mode::alternate);
	i2c0scl::setAlternateMode(Gpio::options::altModes::alt3);
	i2c0scl::setPullUp();

	i2c0sda::enableAsDigital();
	i2c0sda::setMode(Gpio::options::mode::alternate);
	i2c0sda::setAlternateMode(Gpio::options::altModes::alt3);
	i2c0sda::setOpenDrain();

	I2C3::enableClock();
	I2C3::config(i2c::configOptions::mode::master,
				 i2c::configOptions::loopback::disabled,
				 i2c::configOptions::speed::fastMode);



/*
	tempSensor::configSensor(sensors::temperature::tmp102::configOptions::nBits::bits_12,
							 sensors::temperature::tmp102::configOptions::conversionRate::rate_8_00Hz,
							 sensors::temperature::tmp102::configOptions::shutdown::no,
							 sensors::temperature::tmp102::configOptions::termostatMode::comparator_mode,
							 sensors::temperature::tmp102::configOptions::oneShot::dontUseOneShotConversion);
*/
	//configRegister = tempSensor::getConfigRegister();


	imu::config(sensors::ivensense::mpu9150::configOptions::sampleRate::sample_1000hz,
				sensors::ivensense::mpu9150::configOptions::digitalLPF::opt0,
				sensors::ivensense::mpu9150::configOptions::gyroScale::scale_500dps,
				sensors::ivensense::mpu9150::configOptions::accelScale::scale_4g,
				sensors::ivensense::mpu9150::configOptions::accelDHPF::none,
				sensors::ivensense::mpu9150::configOptions::tempSensor::disabled,
				sensors::ivensense::mpu9150::configOptions::clockSel::pll_xaxis_gyro_ref);

	reg = imu::readRegister(28);

/*
	*(a.buff+0) = 1;
	*(a.buff+1) = 2;
	*(a.buff+2) = 3;

	*(b.buff+0) = 1;
	*(b.buff+1) = 3;
	*(b.buff+2) = 5;
	//c = a * b;
*/
	timer0::enableClock();
	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);
	timer0::config(gpTimer::subTimers::subTimerA,
				   100,
				   gpTimer::options::timerOptions::joiningMode::timers_joinTimers,
				   gpTimer::options::timerOptions::timerMode::periodic,
				   gpTimer::options::timerOptions::countDirection::down,
				   gpTimer::options::timerOptions::interrupts::timerAtimeout);
	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);

	core::IntEnableMaster();
	core::enableInterrupt(core::interrupts::timer0a_simple);


	while(1)
	{

	}
}

void interruptFuncs::timer0SubA_isr()
{
	timer0::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);
	//temperatureReg = tempSensor::getTemperature();
	//temperature = ((float)(temperatureReg))*0.0625;

	imuReadings.gyroReading.x = imu::readGyro(sensors::ivensense::mpu9150::axis::xAxis);
	imuReadings.gyroReading.y = imu::readGyro(sensors::ivensense::mpu9150::axis::yAxis);
	imuReadings.gyroReading.z = imu::readGyro(sensors::ivensense::mpu9150::axis::zAxis);

	gyroZ = ((float)(imuReadings.gyroReading.z)/32768.0)*500.0;

	imuReadings.accelReading.x = imu::readAccel(sensors::ivensense::mpu9150::axis::xAxis);
	imuReadings.accelReading.y = imu::readAccel(sensors::ivensense::mpu9150::axis::yAxis);
	imuReadings.accelReading.z = imu::readAccel(sensors::ivensense::mpu9150::axis::zAxis);

	accelX = ((float)(imuReadings.accelReading.x)/32768.0)*4.0;
	accelY = ((float)(imuReadings.accelReading.y)/32768.0)*4.0;
	accelZ = ((float)(imuReadings.accelReading.z)/32768.0)*4.0;

	imu::startMag();
	//reg = imu::readMagReg(0);
	//imu::writeReg(10,0x2);
	//reg = imu::readRegister(2);
	imuReadings.magntReading.x = imu::readMagnt(sensors::ivensense::mpu9150::axis::xAxis);
	imuReadings.magntReading.y = imu::readMagnt(sensors::ivensense::mpu9150::axis::yAxis);
	imuReadings.magntReading.z = imu::readMagnt(sensors::ivensense::mpu9150::axis::zAxis);

	//c = a + b;
	PF3::toogle();
}
