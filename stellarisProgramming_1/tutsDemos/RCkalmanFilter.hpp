#include "include/clock.hpp"
#include "include/Gpio.hpp"
#include "delays.hpp"
#include "interrupts.hpp"
#include "include/Core.hpp"
#include "include/I2C.h"
#include "include/Uart.h"
#include "mcpDAC.hpp"
#include "include/Gptimer.hpp"
#include "include/Adc.h"
#include "linalg.hpp"

typedef PB2 i2c0scl;
typedef PB3 i2c0sda;
typedef mcp4725::dac<i2c::module::i2c0> analogOutput;


u8 foo;
u32 fun = 0;
double vout = 1.0;
bool go;
unsigned int buffADC[1];
u8 buffTxUart[2];
u8 buffRxUart[3] = {0,0,0};
u16 voltageSet;
unsigned int pos = 0;

int main()
{
	clock::clock::config(
			clock::configOptions::clockSource::mainOscilator,
			clock::configOptions::clockRate::clock_80Mhz,
			clock::configOptions::enablePIOSC::enablePinternalOsc,
			clock::configOptions::enableMOSC::enableMainOsc);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOE);
	Gpio::enableClock(Gpio::peripheral::GPIOB);
	Gpio::enableClock(Gpio::peripheral::GPIOA);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PF3::enableAsDigital();
	PF3::setMode(Gpio::options::mode::gpio);
	PF3::setIOmode(Gpio::options::IOmode::output);

	PA0::enableAsDigital();
	PA0::setMode(Gpio::options::mode::alternate);
	PA0::setAlternateMode(Gpio::options::altModes::alt1);

	PA1::enableAsDigital();
	PA1::setMode(Gpio::options::mode::alternate);
	PA1::setAlternateMode(Gpio::options::altModes::alt1);

	PE3::setMode(Gpio::options::mode::alternate);
	PE3::disableDigitalFunc();
	PE3::setAnalogMode();

	adc::enableClock(adc::converter::adc0);
	adc::setSampleRate(adc::converter::adc0,adc::configOptions::sampleRate::sample_1msps);
	adc0seq0::disableSequencer();
	adc0seq0::selectTrigger(adc::configOptions::trigger::software);
	adc0seq0::sampleConfigure(adc::configOptions::sample::sample_1st,
							  adc::configOptions::inputpin::ain0,
							  adc::configOptions::tempsensor::ommitTempSensor,
							  adc::configOptions::interruptEnable::sampleIntEnable,
							  adc::configOptions::endOfSequence::isEndOfSample,
							  adc::configOptions::differentialMode::ommitDiffMode);
	adc0seq0::enableSequencer();


	i2c0scl::enableAsDigital();
	i2c0scl::setMode(Gpio::options::mode::alternate);
	i2c0scl::setAlternateMode(Gpio::options::altModes::alt3);
	i2c0scl::setPullUp();

	i2c0sda::enableAsDigital();
	i2c0sda::setMode(Gpio::options::mode::alternate);
	i2c0sda::setAlternateMode(Gpio::options::altModes::alt3);
	i2c0sda::setOpenDrain();

	I2C0::enableClock();
	I2C0::config(i2c::configOptions::mode::master,
				 i2c::configOptions::loopback::disabled,
				 i2c::configOptions::speed::standarMode);

	UART0::enableClock();
	UART0::configUart(uart::configOptions::baudrate::baud_1000000,
					  uart::configOptions::nbits::bits_8,
					  uart::configOptions::parity::none,
					  uart::configOptions::stopbits::stopBits_one,
					  uart::configOptions::interrupts::receiveInt,
					  uart::configOptions::fifo::fifoDisabled,
					  uart::clockSource::systemClock);

	timer0::enableClock();
	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);
	timer0::config(gpTimer::subTimers::subTimerA,
			       4000,
			       gpTimer::options::timerOptions::joiningMode::timers_joinTimers,
			       gpTimer::options::timerOptions::timerMode::periodic,
			       gpTimer::options::timerOptions::countDirection::down,
			       gpTimer::options::timerOptions::interrupts::timerAtimeout);
	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);

	core::IntEnableMaster();
	core::enableInterrupt(core::interrupts::uart0);
	core::enableInterrupt(core::interrupts::timer0a_simple);


	while(1)
	{
		if(go)
		{
			voltageSet = analogOutput::setVoltage(vout,3.3);
			adc0seq0::clearInterrupt();
			adc0seq0::softBeginSampling();
			while(!adc0seq0::finishedSampling());
			adc0seq0::takeData(buffADC);
			buffTxUart[0] = (u8)((buffADC[0]>>8)&(0x00ff));
			buffTxUart[1] = (u8)((buffADC[0]>>0)&(0x00ff));
			UART0::sendByte(buffTxUart[0]);
			UART0::sendByte(buffTxUart[1]);
			go = false;
			PF3::toogle();
		}
	}
}

void interruptFuncs::uart0rxtx_isr()
{
	UART0::clearInterrupts(uart::configOptions::interrupts::receiveInt);
	buffRxUart[pos] = UART0::receiveByte();
	pos++;
	if(pos>=3)
	{
		if(buffRxUart[0] == 100)
		{
			vout = (((double)((buffRxUart[1]<<8 | buffRxUart[2])))/4095.0)*3.3;
			go = true;
		}
		pos = 0;
	}

}

void interruptFuncs::timer0SubA_isr()
{
	timer0::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);
	fun++;
	/*
	switch(fun%2)
	{
		case 0:
			vout = 1.0;
		break;
		case 1:
			vout = 2.0;
		break;
	}*/
	PF2::toogle();
}


