#include "include/clock.hpp"
#include "include/Gpio.hpp"
#include "interrupts.hpp"
#include "include/Gptimer.hpp"
#include "include/Core.hpp"
#include "delays.hpp"
#include "include/I2C.h"
#include "include/Adc.h"
#include "include/Uart.h"

typedef PB2 i2c0scl;
typedef PB3 i2c0sda;
typedef PE3 analogPin;

typedef adc0seq0 analogVolt;

unsigned char buffDataI2C[2] = {0x0f,0xff};
unsigned int voltage[2]={0,0};
unsigned int count = 0;
bool fun = false;

int main()
{
	clock::clock::config(
			clock::configOptions::clockSource::mainOscilator,
			clock::configOptions::clockRate::clock_80Mhz,
			clock::configOptions::enablePIOSC::enablePinternalOsc,
			clock::configOptions::enableMOSC::enableMainOsc);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOB);
	Gpio::enableClock(Gpio::peripheral::GPIOE);
	Gpio::enableClock(Gpio::peripheral::GPIOA);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PF3::enableAsDigital();
	PF3::setMode(Gpio::options::mode::gpio);
	PF3::setIOmode(Gpio::options::IOmode::output);

	PA0::enableAsDigital();
	PA0::setMode(Gpio::options::mode::alternate);
	PA0::setAlternateMode(Gpio::options::altModes::alt1);

	PA1::enableAsDigital();
	PA1::setMode(Gpio::options::mode::alternate);
	PA1::setAlternateMode(Gpio::options::altModes::alt1);

	UART0::enableClock();
	UART0::configUart(uart::configOptions::baudrate::baud_115200,
					  uart::configOptions::nbits::bits_8,
					  uart::configOptions::parity::none,
					  uart::configOptions::stopbits::stopBits_one,
					  uart::configOptions::interrupts::receiveInt,
					  uart::configOptions::fifo::fifoDisabled,
					  uart::clockSource::systemClock);
	I2C0::enableClock();
	i2c0scl::enableAsDigital();
	i2c0scl::setMode(Gpio::options::mode::alternate);
	i2c0scl::setAlternateMode(Gpio::options::altModes::alt3);
	i2c0scl::setPullUp();

	i2c0sda::enableAsDigital();
	i2c0sda::setMode(Gpio::options::mode::alternate);
	i2c0sda::setAlternateMode(Gpio::options::altModes::alt3);
	i2c0sda::setOpenDrain();

	I2C0::config(i2c::configOptions::mode::master,
				 i2c::configOptions::loopback::disabled,
				 i2c::configOptions::speed::standarMode);

	adc::enableClock(adc::converter::adc0);
	adc::setSampleRate(adc::converter::adc0,adc::configOptions::sampleRate::sample_250ksps);

	analogPin::setMode(Gpio::options::mode::alternate);
	analogPin::disableDigitalFunc();
	analogPin::setAnalogMode();

	analogVolt::disableSequencer();
	analogVolt::selectTrigger(adc::configOptions::trigger::software);
	analogVolt::sampleConfigure(adc::configOptions::sample::sample_1st,
								adc::configOptions::inputpin::ain0,
								adc::configOptions::tempsensor::ommitTempSensor,
								adc::configOptions::interruptEnable::sampleIntDisabled,
								adc::configOptions::endOfSequence::isntEndOfSample,
								adc::configOptions::differentialMode::ommitDiffMode);
	analogVolt::sampleConfigure(adc::configOptions::sample::sample_2nd,
								adc::configOptions::inputpin::ain0,
								adc::configOptions::tempsensor::ommitTempSensor,
								adc::configOptions::interruptEnable::sampleIntEnable,
								adc::configOptions::endOfSequence::isEndOfSample,
								adc::configOptions::differentialMode::ommitDiffMode);
	analogVolt::enableSequencer();

	timer0::enableClock();
	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);
	timer0::config(gpTimer::subTimers::subTimerA,
				   10000,
				   gpTimer::options::timerOptions::joiningMode::timers_joinTimers,
				   gpTimer::options::timerOptions::timerMode::periodic,
				   gpTimer::options::timerOptions::countDirection::down,
				   gpTimer::options::timerOptions::interrupts::timerAtimeout);
	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);
	core::enableInterrupt(core::interrupts::timer0a_simple);
	core::enableInterrupt(core::interrupts::uart0);
	core::IntEnableMaster();

	while(1)
	{

	}
}

void interruptFuncs::uart0rxtx_isr()
{
	unsigned char foo = UART0::receiveByte();
	fun = true;
	UART0::clearInterrupts(uart::configOptions::interrupts::receiveInt);
	I2C0::send(0x60,buffDataI2C,2);
	analogVolt::clearInterrupt();
	analogVolt::softBeginSampling();
	while(!analogVolt::finishedSampling())
	{
	}
	analogVolt::takeData(voltage);//value in the range [0-4096] <> [0.0 - 3.3V]
	PF2::toogle();
	UART0::sendByte((unsigned char)((voltage[0]>>8)&(0x00ff)));
	UART0::sendByte((unsigned char)((voltage[0]>>0)&(0x00ff)));
}

void interruptFuncs::timer0SubA_isr()
{
	timer0::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);
	if(fun)
	{
		count++;
		switch(count%2)
		{
			case 0:
				buffDataI2C[0] = 0x0f;
				buffDataI2C[1] = 0xff;
			break;
			case 1:
				buffDataI2C[0] = 0x00;
				buffDataI2C[1] = 0x00;
			break;
		}
	}

	PF3::toogle();
}

