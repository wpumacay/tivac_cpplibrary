#include "include/clock.hpp"
#include "include/Gpio.hpp"
#include "include/Uart.h"
#include "delays.hpp"
#include "include/Core.hpp"
#include "interrupts.hpp"
#include "include/Gptimer.hpp"
//#include "usb.hpp"
int main()
{
	clock::clock::config(
			clock::configOptions::clockSource::mainOscilator,
			clock::configOptions::clockRate::clock_80Mhz,
			clock::configOptions::enablePIOSC::enablePinternalOsc,
			clock::configOptions::enableMOSC::enableMainOsc);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOA);

	PF1::enableAsDigital();
	PF1::setMode(Gpio::options::mode::gpio);
	PF1::setIOmode(Gpio::options::IOmode::output);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PF3::enableAsDigital();
	PF3::setMode(Gpio::options::mode::gpio);
	PF3::setIOmode(Gpio::options::IOmode::output);

	PA0::enableAsDigital();
	PA0::setMode(Gpio::options::mode::alternate);
	PA0::setAlternateMode(Gpio::options::altModes::alt1);

	PA1::enableAsDigital();
	PA1::setMode(Gpio::options::mode::alternate);
	PA1::setAlternateMode(Gpio::options::altModes::alt1);

	UART0::enableClock();
	UART0::configUart(uart::configOptions::baudrate::baud_1000000,
					  uart::configOptions::nbits::bits_8,
					  uart::configOptions::parity::none,
					  uart::configOptions::stopbits::stopBits_one,
					  uart::configOptions::interrupts::receiveInt,
					  uart::configOptions::fifo::fifoDisabled,
					  uart::clockSource::systemClock);

	timer1::enableClock();
	timer1::disableSubTimer(gpTimer::subTimers::subTimerA);
	timer1::config(gpTimer::subTimers::subTimerA,
				   500,
				   gpTimer::options::timerOptions::joiningMode::timers_joinTimers,
				   gpTimer::options::timerOptions::timerMode::periodic,
				   gpTimer::options::timerOptions::countDirection::down,
				   gpTimer::options::timerOptions::interrupts::timerAtimeout);
	timer1::enableSubTimer(gpTimer::subTimers::subTimerA);

	core::enableInterrupt(core::interrupts::uart0);
	core::enableInterrupt(core::interrupts::timer1a_simple);
	core::IntEnableMaster();


	while(1)
	{/*
		PF3::setHigh();
		delays::delay_ms(500);
		PF3::setLow();
		delays::delay_ms(500);
	 */
	}
}

void interruptFuncs::timer1SubA_isr()
{
	timer1::clearInterrupt(gpTimer::
						   options::
						   timerOptions::
						   interrupts::
						   timerAtimeout);
	//UART0::sendString("timer reventó!!!\n\r");
	PF2::toogle();
}


void interruptFuncs::uart0rxtx_isr()
{
	unsigned char foo = UART0::receiveByte();
	UART0::sendString("ha enviado: ");
	UART0::sendByte(foo);
	UART0::sendString("\n\r");
	UART0::clearInterrupts(uart::configOptions::interrupts::receiveInt);
	PF3::toogle();
}
