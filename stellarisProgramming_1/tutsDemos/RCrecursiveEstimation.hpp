#include "include/clock.hpp"
#include "include/Gpio.hpp"
#include "delays.hpp"
#include "interrupts.hpp"
#include "include/Core.hpp"
#include "include/I2C.h"
#include "mcpDAC.hpp"
#include "include/Gptimer.hpp"
#include "include/Adc.h"
#include "linalg.hpp"
//#include "vectors.hpp"
#include "systemIdent.hpp"

//linalg::matrix A(2,2);
//linalg::matrix B(2,2);
//linalg::matrix C(2,2);

//sysIdent::recursive::kalman::belief belief_t;
linalg::matrix mu(2,1);
linalg::matrix sigma(2,2);

linalg::matrix K(2,1);
linalg::matrix S(1,1);
linalg::matrix I(2,2);

linalg::matrix dummy1(1,1);

linalg::matrix A(2,2);
linalg::matrix At(2,2);
linalg::matrix C(1,2);
linalg::matrix Ct(2,1);
linalg::matrix R(2,2);
linalg::matrix Q(1,1);

unsigned int foo = 0;
double z = 0.0;
linalg::matrix zMatrix(1,1);

double checkedResult[4] = {0,0,0,0};

double checkedResult3[9];

double a1 = 0.0;
double b1 = 0.0;
double y_k_1 = 0.1;
//double u_k_1 = 0.0;

typedef PB2 i2c0scl;
typedef PB3 i2c0sda;
typedef mcp4725::dac<i2c::module::i2c0> analogOutput;

u32 fun = 0;
double vout = 0.0;
bool go;
unsigned short buffADC[1];
u16 voltageSet;

double bar;
double bar2;
linalg::matrix dummyMat(2,2);
linalg::matrix dummyMat2(2,2);

int main()
{
	clock::clock::config(
			clock::configOptions::clockSource::mainOscilator,
			clock::configOptions::clockRate::clock_80Mhz,
			clock::configOptions::enablePIOSC::enablePinternalOsc,
			clock::configOptions::enableMOSC::enableMainOsc);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOE);
	Gpio::enableClock(Gpio::peripheral::GPIOB);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PF3::enableAsDigital();
	PF3::setMode(Gpio::options::mode::gpio);
	PF3::setIOmode(Gpio::options::IOmode::output);

	PE3::setMode(Gpio::options::mode::alternate);
	PE3::disableDigitalFunc();
	PE3::setAnalogMode();

	//z = 1.0/0.0000000000000000000000000000000000000000000000000000000000000000000000001;

	adc::enableClock(adc::converter::adc0);
	adc::setSampleRate(adc::converter::adc0,adc::configOptions::sampleRate::sample_1msps);
	adc0seq0::disableSequencer();
	adc0seq0::selectTrigger(adc::configOptions::trigger::software);
	adc0seq0::sampleConfigure(adc::configOptions::sample::sample_1st,
							  adc::configOptions::inputpin::ain0,
							  adc::configOptions::tempsensor::ommitTempSensor,
							  adc::configOptions::interruptEnable::sampleIntEnable,
							  adc::configOptions::endOfSequence::isEndOfSample,
							  adc::configOptions::differentialMode::ommitDiffMode);
	adc0seq0::enableSequencer();

	i2c0scl::enableAsDigital();
	i2c0scl::setMode(Gpio::options::mode::alternate);
	i2c0scl::setAlternateMode(Gpio::options::altModes::alt3);
	i2c0scl::setPullUp();

	i2c0sda::enableAsDigital();
	i2c0sda::setMode(Gpio::options::mode::alternate);
	i2c0sda::setAlternateMode(Gpio::options::altModes::alt3);
	i2c0sda::setOpenDrain();

	I2C0::enableClock();
	I2C0::config(i2c::configOptions::mode::master,
				 i2c::configOptions::loopback::disabled,
				 i2c::configOptions::speed::standarMode);

/*
	*(A.buff + 0) = 1.0;
	*(A.buff + 1) = 1.0;
	*(A.buff + 2) = 1.0;
	*(A.buff + 3) = 1.0;

	*(B.buff + 0) = 1.0;
	*(B.buff + 1) = 2.0;
	*(B.buff + 2) = 3.0;
	*(B.buff + 3) = 4.0;
*/
	/*
	*(D.buff + 0) = 4.0;
	*(D.buff + 1) = 1.0;
	*(D.buff + 2) = 1.0;
	*(D.buff + 3) = 1.0;
	*(D.buff + 4) = 0.25;
	*(D.buff + 5) = 1.0;
	*(D.buff + 6) = 1.0;
	*(D.buff + 7) = 2.0;
	*(D.buff + 8) = 1.0;
*/
	//linalg::checkMatrix(A,checkedResult);
	//linalg::checkMatrix(B,checkedResult);
/*
	C = A*B;
	checkedResult[0] = C.getElement(0,0);
	checkedResult[1] = C.getElement(1,0);
	checkedResult[2] = C.getElement(0,1);
	checkedResult[3] = C.getElement(1,1);

	checkedResult[0] = A.getElement(0,0);
	checkedResult[1] = A.getElement(1,0);
	checkedResult[2] = A.getElement(0,1);
	checkedResult[3] = A.getElement(1,1);

	checkedResult[0] = B.getElement(0,0);
	checkedResult[1] = B.getElement(1,0);
	checkedResult[2] = B.getElement(0,1);
	checkedResult[3] = B.getElement(1,1);
	//linalg::checkMatrix(C,checkedResult);
	//linalg::checkMatrix(A,checkedResult);
	//linalg::checkMatrix(B,checkedResult);

	*(A.buff + 0) = 1.0;
	*(A.buff + 1) = 2.0;
	*(A.buff + 2) = -1.0;
	*(A.buff + 3) = -1.0;

	C = A.inv();
	checkedResult[0] = C.getElement(0,0);
	checkedResult[1] = C.getElement(1,0);
	checkedResult[2] = C.getElement(0,1);
	checkedResult[3] = C.getElement(1,1);
	//linalg::checkMatrix(C,checkedResult);
*/
	/*
	checkedResult3[0] = D.getElement(0,0);
	checkedResult3[1] = D.getElement(1,0);
	checkedResult3[2] = D.getElement(2,0);
	checkedResult3[3] = D.getElement(0,1);
	checkedResult3[4] = D.getElement(1,1);
	checkedResult3[5] = D.getElement(2,1);
	checkedResult3[6] = D.getElement(0,2);
	checkedResult3[7] = D.getElement(1,2);
	checkedResult3[8] = D.getElement(2,2);

	E = D.inv();
	checkedResult3[0] = E.getElement(0,0);
	checkedResult3[1] = E.getElement(1,0);
	checkedResult3[2] = E.getElement(2,0);
	checkedResult3[3] = E.getElement(0,1);
	checkedResult3[4] = E.getElement(1,1);
	checkedResult3[5] = E.getElement(2,1);
	checkedResult3[6] = E.getElement(0,2);
	checkedResult3[7] = E.getElement(1,2);
	checkedResult3[8] = E.getElement(2,2);
*/

	A = linalg::identity(2);
	At = A.trsp();
	I = linalg::identity(2);
/*
	C.setValue(0,0,1.0);
	C.setValue(0,1,2.0);
	K.setValue(0,0,1.0);
	K.setValue(1,0,2.0);

	dummyMat = K*C;

	bar = dummyMat.getElement(0,0);
	bar = dummyMat.getElement(0,1);
	bar = dummyMat.getElement(1,0);
	bar = dummyMat.getElement(1,1);

	C.setValue(0,0,1.0);
	C.setValue(0,1,1.0);
	K.setValue(0,0,1.0);
	K.setValue(1,0,1.0);

	dummyMat = K*C;

	bar = dummyMat.getElement(0,0);
	bar = dummyMat.getElement(0,1);
	bar = dummyMat.getElement(1,0);
	bar = dummyMat.getElement(1,1);

	C.setValue(0,0,1.0);
	C.setValue(0,1,3.0);
	K.setValue(0,0,1.0);
	K.setValue(1,0,3.0);

	dummyMat = K*C;

	bar = dummyMat.getElement(0,0);
	bar = dummyMat.getElement(0,1);
	bar = dummyMat.getElement(1,0);
	bar = dummyMat.getElement(1,1);

	dummyMat2 = A*At;

	bar2 = dummyMat2.getElement(0,0);
	bar2 = dummyMat2.getElement(0,1);
	bar2 = dummyMat2.getElement(1,0);
	bar2 = dummyMat2.getElement(1,1);

	A.setValue(0,0,1.0);
	A.setValue(0,1,1.0);
	A.setValue(1,0,1.0);
	A.setValue(1,1,1.0);

	At.setValue(0,0,1.0);
	At.setValue(0,1,1.0);
	At.setValue(1,0,1.0);
	At.setValue(1,1,1.0);

	dummyMat2 = A*At;

	bar2 = dummyMat2.getElement(0,0);
	bar2 = dummyMat2.getElement(0,1);
	bar2 = dummyMat2.getElement(1,0);
	bar2 = dummyMat2.getElement(1,1);
*/



	//*(C.buff + 0) = 1.0;
	//*(C.buff + 1) = 1.0;
	//C.setValue(0,0,0.0);
	//C.setValue(0,0,0.0);
	//C.setValue(0,0,0.0);
	//C.setValue(0,0,0.0);
	C.setValue(0,0,0.0);
	C.setValue(0,1,0.0);
	//zMatrix = C*mu;



	mu.setValue(0,0,0.0);
	mu.setValue(1,0,0.0);
	sigma.setValue(0,0,100.0);
	sigma.setValue(0,1,0.0);
	sigma.setValue(1,0,0.0);
	sigma.setValue(1,1,100.0);

/*
	belief_t.mu 	= linalg::matrix(2,1);
	belief_t.sigma	= linalg::matrix(2,2);
	*(belief_t.mu.buff + 0) = 0.0;
	*(belief_t.mu.buff + 1) = 0.0;
	*(belief_t.sigma.buff + 0) = 100.0;
	*(belief_t.sigma.buff + 1) = 0.0;
	*(belief_t.sigma.buff + 2) = 0.0;
	*(belief_t.sigma.buff + 3) = 100.0;
*/

	//*(Q.buff + 0) = 0.000001;
	Q.setValue(0,0,0.00000001);
	R.setValue(0,0,0.0);
	R.setValue(0,1,0.0);
	R.setValue(1,0,0.0);
	R.setValue(1,1,0.0);


	timer0::enableClock();
	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);
	timer0::config(gpTimer::subTimers::subTimerA,
			       10,
			       gpTimer::options::timerOptions::joiningMode::timers_joinTimers,
			       gpTimer::options::timerOptions::timerMode::periodic,
			       gpTimer::options::timerOptions::countDirection::down,
			       gpTimer::options::timerOptions::interrupts::timerAtimeout);
	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);

	core::IntEnableMaster();
	core::enableInterrupt(core::interrupts::timer0a_simple);


	while(1)
	{

	}
}

void interruptFuncs::timer0SubA_isr()
{
	timer0::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);
	//E = D.inv();
	/*
	linalg::matrix D(3,3);
	linalg::matrix E(3,3);
	linalg::matrix F(3,3);



	*(D.buff + 0) = 4.0;
	*(D.buff + 1) = 1.0;
	*(D.buff + 2) = 1.0;
	*(D.buff + 3) = 1.0;
	*(D.buff + 4) = 0.25;
	*(D.buff + 5) = 1.0;
	*(D.buff + 6) = 1.0;
	*(D.buff + 7) = 2.0;
	*(D.buff + 8) = 1.0;

	*(E.buff + 0) = 4.0;
	*(E.buff + 1) = 1.0;
	*(E.buff + 2) = 1.0;
	*(E.buff + 3) = 1.0;
	*(E.buff + 4) = 0.25;
	*(E.buff + 5) = 1.0;
	*(E.buff + 6) = 1.0;
	*(E.buff + 7) = 2.0;
	*(E.buff + 8) = 1.0;

	F = D + E;
	F = E + D;
	F = E + D + E;
	E = D.inv();

*/
	fun++;
	voltageSet = analogOutput::setVoltage(vout,3.3);
	adc0seq0::clearInterrupt();
	adc0seq0::softBeginSampling();
	while(!adc0seq0::finishedSampling());
	adc0seq0::takeData(buffADC);
	z = (((double)(buffADC[0]))/4095.0)*3.3;

	//*(zMatrix.buff) = z;
	zMatrix.setValue(0,0,z);
	C.setValue(0,0,y_k_1);//-----
	C.setValue(0,1,vout);
	Ct = C.trsp();
	At = A.trsp();
	//*(C.buff + 0) = y_k_1;
	//*(C.buff + 1) = vout;

	//belief_t = sysIdent::recursive::kalman::predict(belief_t,A,R);
	//belief_t = sysIdent::recursive::kalman::correct(belief_t,C,Q,zMatrix);

	mu = A*mu;
	sigma = A*sigma*At + R;

	S = (C*sigma)*(Ct) + Q;
	S = S.inv();
	K = sigma*(Ct)*S;

	dummy1 = C*mu;
	dummy1 = zMatrix - dummy1;

	//mu = mu + K*(zMatrix - C*mu);
	mu = mu + K*dummy1;
	sigma = (I - K*C)*sigma;//-----

	a1 = mu.getElement(0,0);
	b1 = mu.getElement(1,0);

	//a1 = belief_t.mu.getElement(0,0);
	//b1 = belief_t.mu.getElement(1,0);

	y_k_1 = z;


	if(fun>1000)
	{
		fun = 0;
	}

	if(fun>500)
	{
		vout = 2.0;
	}
	else
	{
		vout = 0.0;
	}
/*
	checkedResult3[0] = E.getElement(0,0);
	checkedResult3[1] = E.getElement(1,0);
	checkedResult3[2] = E.getElement(2,0);
	checkedResult3[3] = E.getElement(0,1);
	checkedResult3[4] = E.getElement(1,1);
	checkedResult3[5] = E.getElement(2,1);
	checkedResult3[6] = E.getElement(0,2);
	checkedResult3[7] = E.getElement(1,2);
	checkedResult3[8] = E.getElement(2,2);
*/
	PF2::toogle();
}


