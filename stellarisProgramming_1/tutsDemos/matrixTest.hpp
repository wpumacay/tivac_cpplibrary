#include "include/clock.hpp"
#include "include/Gpio.hpp"
#include "delays.hpp"
#include "interrupts.hpp"
#include "include/Core.hpp"
#include "include/Gptimer.hpp"
#include "linalg.hpp"

linalg::matrix A(2,2);
linalg::matrix B(2,2);
linalg::matrix C(2,2);

double checkedResult[4];


int main()
{
	clock::clock::config(
			clock::configOptions::clockSource::mainOscilator,
			clock::configOptions::clockRate::clock_80Mhz,
			clock::configOptions::enablePIOSC::enablePinternalOsc,
			clock::configOptions::enableMOSC::enableMainOsc);

	Gpio::enableClock(Gpio::peripheral::GPIOF);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PF3::enableAsDigital();
	PF3::setMode(Gpio::options::mode::gpio);
	PF3::setIOmode(Gpio::options::IOmode::output);

	*(A.buff + 0) = 1.0;
	*(A.buff + 1) = 1.0;
	*(A.buff + 2) = 1.0;
	*(A.buff + 3) = 1.0;

	*(B.buff + 0) = 1.0;
	*(B.buff + 1) = 2.0;
	*(B.buff + 2) = 3.0;
	*(B.buff + 3) = 4.0;

	linalg::checkMatrix(A,checkedResult);
	linalg::checkMatrix(B,checkedResult);
	C = A+B;
	linalg::checkMatrix(C,checkedResult);

	timer0::enableClock();
	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);
	timer0::config(gpTimer::subTimers::subTimerA,
			       100,
			       gpTimer::options::timerOptions::joiningMode::timers_joinTimers,
			       gpTimer::options::timerOptions::timerMode::periodic,
			       gpTimer::options::timerOptions::countDirection::down,
			       gpTimer::options::timerOptions::interrupts::timerAtimeout);
	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);

	core::IntEnableMaster();
	core::enableInterrupt(core::interrupts::timer0a_simple);


	while(1)
	{

	}
}

void interruptFuncs::timer0SubA_isr()
{
	timer0::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);


	PF2::toogle();
}


