//en este video usaremos .. m�s leds.. xD
//pero ahora... usaremos algo de c++ T_T!

#include "include/Gpio.hpp"
#include "include/clock.hpp"
#include "delays.hpp"
#include "ledBar.hpp"

ledbar myBar(Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin0,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin1,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin2,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin3,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin4,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin5,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin6,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin7,
		   	   	    Gpio::Port::GPIOPortE_APB,Gpio::Pin::pin3,
		   	   	    Gpio::Port::GPIOPortE_APB,Gpio::Pin::pin2);

int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
								 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOB);
	Gpio::enableClock(Gpio::peripheral::GPIOE);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	myBar.init();

	while(1)
	{
		PF2::toogle();
		//myBar.step();
		myBar.funkyStep();
		delays::delay_ms(100);
	}
}
