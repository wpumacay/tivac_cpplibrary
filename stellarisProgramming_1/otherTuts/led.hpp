//prendiendo un led xD!

#include "include/Gpio.hpp"
#include "include/clock.hpp"
#include "delays.hpp"

int main()
{

	//CONFIGURAR EL CLOCK DEL MICROCONTROLADOR PARA 80 MHz y usar el oscilador principal externo
	//de 16 mhz
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz);

	//habilitar el clock para el puerto F
	Gpio::enableClock(Gpio::peripheral::GPIOF);

	//CONFIGURAR EL PIN PF2 como salida digita (Gpio output)
	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	//listo!

	while(1)
	{
		//los nombres describen bien cada funci�n...
		PF2::setHigh();//pf2 a nivel alto (salida 3.3v)
		delays::delay_ms(250);//delay de 250 ms
		PF2::setLow();//pf2 a nivel bajo (salida 0.0v)
		delays::delay_ms(250);//delay de 250 ms
	}
}

//a probarlo!!!
//compilar
//a programarlo
