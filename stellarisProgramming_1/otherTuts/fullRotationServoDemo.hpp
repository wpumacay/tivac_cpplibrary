#include "include/Gpio.hpp"
#include "include/clock.hpp"
#include "delays.hpp"
#include "include/Gptimer.hpp"
#include "ledBar.hpp"

float duty = 0.03;
ledbar myBar(Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin0,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin1,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin2,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin3,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin4,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin5,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin6,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin7,
		   	   	    Gpio::Port::GPIOPortE_APB,Gpio::Pin::pin3,
		   	   	    Gpio::Port::GPIOPortE_APB,Gpio::Pin::pin2);

int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
								 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOC);
	Gpio::enableClock(Gpio::peripheral::GPIOB);
	Gpio::enableClock(Gpio::peripheral::GPIOE);

	myBar.init();

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PC6::enableAsDigital();
	PC6::setMode(Gpio::options::mode::alternate);
	PC6::setAlternateMode(Gpio::options::altModes::alt7);

	pwmW_1A::enableClock();
	pwmW_1A::config(20,0.06,gpTimer::options::pwmOptions::outputLevel::inverted);

	while(1)
	{
		PF2::toogle();
		myBar.funkyStep();
		delays::delay_ms(250);
		pwmW_1A::setDuty(duty);
		duty += 0.0006;
		if(duty>=0.09)
		{
			duty = 0.06;
		}
	}
}
