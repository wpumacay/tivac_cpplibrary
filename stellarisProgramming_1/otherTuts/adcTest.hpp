//usemos el m�dulo anal�gio

#include "include/Gpio.hpp"
#include "include/clock.hpp"
#include "include/Adc.h"
#include "delays.hpp"

//variable para almacenar la muestra
u16 buffADC[1];


//TNEOG CONECTADO EL PIN PE3 A UN SENSOR LDR DE INTENSIDAD DE LUZ
//PLOTEEMOS LOS DATOS
//OK, ESO ES TODO POR ESTA SECCI�N
//EN LA SIGUIENTE VEREMOS COMO USAR EL....M�DULO I2C...
int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);//para los pines del puerto F
	Gpio::enableClock(Gpio::peripheral::GPIOE);//para el pin pe3 de la entrada anal�gica aIN0

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	//configurando el pin pe3 como entrada anal�gica
	PE3::setMode(Gpio::options::mode::alternate);//que trabaje como modo alterno... uno de ellos, el adc
	PE3::disableDigitalFunc();//deshabilitar la funci�n digital del pin...
	PE3::setAnalogMode();//setear el modo anal�gico

	//ahora, configuremos el ADC
	//....
	adc::enableClock(adc::converter::adc0);
	adc::setSampleRate(adc::converter::adc0,adc::configOptions::sampleRate::sample_1msps);
	adc0seq0::disableSequencer();
	adc0seq0::selectTrigger(adc::configOptions::trigger::software);
	adc0seq0::sampleConfigure(adc::configOptions::sample::sample_1st,
							  adc::configOptions::inputpin::ain0,
							  adc::configOptions::tempsensor::ommitTempSensor,
							  adc::configOptions::interruptEnable::sampleIntEnable,
							  adc::configOptions::endOfSequence::isEndOfSample,
							  adc::configOptions::differentialMode::ommitDiffMode);
	adc0seq0::enableSequencer();
	adc0seq0::clearInterrupt();

	while(1)
	{
		adc0seq0::softBeginSampling();//comenzar la toma de la muestra... por software...//trigger!
		while(!adc0seq0::finishedSampling());//wait hasta q se haya terminado de procesar la muestra... 1us-> 1msps
		adc0seq0::takeData(buffADC);
		PF2::toogle();
		delays::delay_ms(20);
	}


}
