#include "include/Gpio.hpp"
#include "include/clock.hpp"
#include "delays.hpp"
#include "QEI.hpp"
#include "include/Gptimer.hpp"
#include "interrupts.hpp"
#include "include/Core.hpp"

typedef qei::qeiMod<mapQEI::QEImodules::QEI0> qe0;

u32 encoderCount = 0;
u32 encoderCount_t_1 = 0;
i32 pulses = 0;
u32 speedCount	 = 0;
double rpm = 0;

int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOD);
	Gpio::enableClock(Gpio::peripheral::GPIOC);

	PF1::enableAsDigital();
	PF1::setMode(Gpio::options::mode::gpio);
	PF1::setIOmode(Gpio::options::IOmode::output);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PF3::enableAsDigital();
	PF3::setMode(Gpio::options::mode::gpio);
	PF3::setIOmode(Gpio::options::IOmode::output);


	PF4::enableAsDigital();
	PF4::setPullUp();
	PF4::setMode(Gpio::options::mode::gpio);
	PF4::setIOmode(Gpio::options::IOmode::input);
	PF4::disableInterrupt();
	PF4::intSense(Gpio::options::Interrupt::intSense::edge,Gpio::options::Interrupt::intEvent::down);
	PF4::enableInterrupt();

	PC6::enableAsDigital();
	PC6::setMode(Gpio::options::mode::alternate);
	PC6::setAlternateMode(Gpio::options::altModes::alt7);

	pwmW_1A::enableClock();
	pwmW_1A::config(1,0.8,gpTimer::options::pwmOptions::outputLevel::inverted);

	PD6::unlock();//
	PD6::enableCommit();
	PD6::enableAsDigital();
	//PD6::setPullUp();
	PD6::setMode(Gpio::options::mode::alternate);
	PD6::setAlternateMode(Gpio::options::altModes::alt6);

	PD7::unlock();//nmi
	PD7::enableCommit();
	PD7::enableAsDigital();
	//PD7::setPullUp();
	PD7::setMode(Gpio::options::mode::alternate);
	PD7::setAlternateMode(Gpio::options::altModes::alt6);

	qe0::enableClock();
	qe0::config(200,
				qei::options::stallOnDebugger::dontStall,//continuar despu�s de pausa
				qei::options::velocityPredivider::div_1,
				qei::options::capVelocity::dontCaptVel,
				qei::options::captureMode::mode1);
	//qe0::setTimer(8000000);
	qe0::enableQEI();

	timer0::enableClock();
	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);
	timer0::config(gpTimer::subTimers::subTimerA,
				   1000,
				   gpTimer::options::timerOptions::joiningMode::timers_joinTimers,
				   gpTimer::options::timerOptions::timerMode::periodic,
				   gpTimer::options::timerOptions::countDirection::down,
				   gpTimer::options::timerOptions::interrupts::timerAtimeout);
	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);

	core::IntEnableMaster();
	core::enableInterrupt(core::interrupts::timer0a_simple);
	//core::enableInterrupt(core::interrupts::gpio_portA);
	core::enableInterrupt(core::interrupts::gpio_portF);


	while(1)
	{
		//PF2::toogle();
		//speedCount		= qe0::getSpeedReg();
		//rpm = (1000.*1.*speedCount*60.)/(80000)
		//rpm = ((double)(speedCount))*60/18.;
		//delays::delay_ms(100);
	}
}

void interruptFuncs::timer0SubA_isr()
{
	timer0::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);

	PF3::toogle();
	encoderCount 	= qe0::getCount();
	pulses = encoderCount - encoderCount_t_1;
	encoderCount_t_1 = encoderCount;

}

void interruptFuncs::gpioF_isr()
{
	PF1::toogle();
	PF4::clearInterrupt();

}
