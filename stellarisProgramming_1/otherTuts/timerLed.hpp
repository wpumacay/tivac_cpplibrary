//usnado timers!!!... e interrupciones...

#include "include/clock.hpp"//librer�as del clock
#include "include/Gpio.hpp"//librer�as para el gpio
#include "include/Gptimer.hpp"//librer�as para los timers...
#include "include/Core.hpp"//librer�as para el m�dulo Core del microcontrolador
#include "interrupts.hpp"//definici�n de los prototipos de interrupci�ny otras cosas "m�s"!!!


int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz);

	//vamos a prender el mismo led, pero ahora usando interrupciones
	Gpio::enableClock(Gpio::peripheral::GPIOF);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	//a configurar le timer...
	//el microcontrolador tiene 12!!! timers de prop�sito general xD
	timer0::enableClock();
	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);//por cada timer hya 2 subtimers
	timer0::config(gpTimer::subTimers::subTimerA,
				   250,
				   gpTimer::options::timerOptions::joiningMode::timers_joinTimers,
				   gpTimer::options::timerOptions::timerMode::periodic,
				   gpTimer::options::timerOptions::countDirection::down,
				   gpTimer::options::timerOptions::interrupts::timerAtimeout);
	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);
	//usaremos el subtimer A, el cu�l es de 16 bits.. pero para poder usar todos los 32 bits...
	//podemos unir los subtimers A y B... s�lo para poder obtener un periodo mayor a 200ms
	///yyyy... varias opciones de configuraci�n m�s... q por el nombre se puede dar una idea

	//habilitar las interrupciones
	core::IntEnableMaster();//habilitar el master general para todas las interrupciones
	core::enableInterrupt(core::interrupts::timer0a_simple);//habilitamos la interrupci�n del timer0 subtimer "a"

	while(1)
	{
		//.....
	}

}


//usemos el prototipo de interrupci�n...
void interruptFuncs::timer0SubA_isr()
{
	//limpiemos la flag
	timer0::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);

	//toogleemos el LED en el pin PF2...
	PF2::toogle();
	//xD... eso es todo!
	// a probarlo!!!
	//ok, eso es todo por esta secci�n....
}
