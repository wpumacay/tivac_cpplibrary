//usando el puerto serie
//y usaremos interrupciones

#include "include/clock.hpp"
#include "include/Gpio.hpp"
#include "include/Core.hpp"
#include "interrupts.hpp"
#include "include/Uart.h"
#include "delays.hpp"

int main()
{

	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);//para los pines del puerto F
	Gpio::enableClock(Gpio::peripheral::GPIOA);//para los pines del puerto A (uarttx y uartrx)

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	//configurarndo los pines del uart RX y TX
	PA0::enableAsDigital();
	PA0::setMode(Gpio::options::mode::alternate);//elegimos el modo alterno para usarlo como UART y no como GPIO
	PA0::setAlternateMode(Gpio::options::altModes::alt1);//el modo alterno para este pin corresponde al del UART RX

	PA1::enableAsDigital();
	PA1::setMode(Gpio::options::mode::alternate);//elegimos el modo alterno para usarlo como UART y no como GPIO
	PA1::setAlternateMode(Gpio::options::altModes::alt1);//el modo alterno para este pin corresponde al del UART TX

	UART0::enableClock();
	UART0::configUart(uart::configOptions::baudrate::baud_1000000,//1mbps
					  uart::configOptions::nbits::bits_8,
					  uart::configOptions::parity::none,
					  uart::configOptions::stopbits::stopBits_one,
					  uart::configOptions::interrupts::receiveInt,//int. de recepci�n
					  uart::configOptions::fifo::fifoDisabled,
					  uart::clockSource::systemClock);

	core::IntEnableMaster();
	core::enableInterrupt(core::interrupts::uart0);

	while(1)
	{
		PF2::setHigh();
		delays::delay_ms(500);
		PF2::setLow();
		delays::delay_ms(500);
		//borramos lo anterior para no tener conflicto al enviar varios bytes....
		//dos partes del programa estar�an peleando por el mismo recurso (UART)
	}

}

//prototipo de funci�n de interrupci�n
void interruptFuncs::uart0rxtx_isr()
{
	//limpiando la flag
	UART0::clearInterrupts(uart::configOptions::interrupts::receiveInt);
	unsigned char foo = UART0::receiveByte();//readbyte()... vamos a cambiarle de nombre a la funci�n luego xD

	//echo.. devolvemos el byte enviado...

	UART0::sendString("recibido: ");
	UART0::sendByte(foo);
	UART0::sendString(" !!!\n\r");
	PF3::toogle();//toogle el otro led, q est� conectado al pin pf3
}
//ok, eso es todo por esta secci�n...
//en al siguiente veremos .... los m�dulos anal�gicos...ADC


