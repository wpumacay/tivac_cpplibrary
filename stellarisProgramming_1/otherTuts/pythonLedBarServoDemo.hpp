//probemos todo junto...

//vamos a mandar por puerto serie... 3 bytes
//el primero de chequeo, el siguiente indica o barra de leds.. o servo
//el siguiente indica la rapidez a la que cambian... en el servo, la velocidad..
//en la barra, el delay

//ya lo tengo hecho xD

#include "include/Gpio.hpp"
#include "include/clock.hpp"
#include "delays.hpp"
#include "include/Gptimer.hpp"//PARA USAR LAS PWMS
#include "ledBar.hpp"//PARA USAR LA BARRA DE LEDS
#include "include/Uart.h"//PARA USER EL PUERTO SERIE
#include "include/Core.hpp"
#include "interrupts.hpp"//PARA USAR LAS INTERRUPCIONES...

//O.O
u8 buffRx[3];//ARRAY Q CONTIENE LOS BYTES.... Q VAMOS A RECIBIR POR PUERTO SERIE
u8 pos = 0;//UNA VARIABLE PARA SELECCIONAR EL ELEMENTO DEL ARRAY
u32 ledDelay = 100;//el delay de la barra de leds

float duty = 0.06;//el duty cycle del servo
ledbar myBar(Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin0,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin1,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin2,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin3,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin4,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin5,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin6,
		   	   	    Gpio::Port::GPIOPortB_APB,Gpio::Pin::pin7,
		   	   	    Gpio::Port::GPIOPortE_APB,Gpio::Pin::pin3,
		   	   	    Gpio::Port::GPIOPortE_APB,Gpio::Pin::pin2);
//el objeto barra de leds....
//ok, funciona bien...
//nos vemos enel siguiente video... grx
int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
								 clock::configOptions::clockRate::clock_80Mhz);

	//LOS PERIFÉRICOS Q VAMOS A NECESITAR
	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOC);
	Gpio::enableClock(Gpio::peripheral::GPIOB);
	Gpio::enableClock(Gpio::peripheral::GPIOE);
	Gpio::enableClock(Gpio::peripheral::GPIOA);

//INICIALIANDO EL OBJETO...
	myBar.init();

	PA0::enableAsDigital();
	PA0::setMode(Gpio::options::mode::alternate);
	PA0::setAlternateMode(Gpio::options::altModes::alt1);

	PA1::enableAsDigital();
	PA1::setMode(Gpio::options::mode::alternate);
	PA1::setAlternateMode(Gpio::options::altModes::alt1);

	UART0::enableClock();
	UART0::configUart(uart::configOptions::baudrate::baud_115200,
								  uart::configOptions::nbits::bits_8,
								  uart::configOptions::parity::none,
								  uart::configOptions::stopbits::stopBits_one,
								  uart::configOptions::interrupts::receiveInt,
								  uart::configOptions::fifo::fifoDisabled,
								  uart::clockSource::systemClock);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PC6::enableAsDigital();
	PC6::setMode(Gpio::options::mode::alternate);
	PC6::setAlternateMode(Gpio::options::altModes::alt7);

	pwmW_1A::enableClock();
	pwmW_1A::config(20,0.06,gpTimer::options::pwmOptions::outputLevel::inverted);

	core::IntEnableMaster();
	core::enableInterrupt(core::interrupts::uart0);

	while(1)
	{
		PF2::toogle();
		myBar.funkyStep();
		delays::delay_ms(ledDelay);
		/*
		pwmW_1A::setDuty(duty);
		duty += 0.0006;
		if(duty>=0.09)
		{
			duty = 0.06;
		}
		*/
	}
}
//LO ANTERIOR YA LO HAN VISTO :D



void interruptFuncs::uart0rxtx_isr()
{
	UART0::clearInterrupts(uart::configOptions::interrupts::receiveInt);
	buffRx[pos] = UART0::receiveByte();//RECIBIMOS EL BYTE
	pos++;//Y AUMENTAMOS EL INDICADOR
	if(pos>=3)//CUADNO HAYAMOS RECIBIDO 3 BYTES... PROCESAR Y RESETEAR EL INDICADOR
	{
		if(buffRx[0] == 's')//un byte de chequeo.....
		{
			switch(buffRx[1])
			{
				case 'm'://motor
						duty = 0.06 + (0.03)*((float)(buffRx[2]))/100.0;
						pwmW_1A::setDuty(duty);
				break;
				case 'l'://leds
						ledDelay = 100 + 4*((u32)(buffRx[2]));
				break;
			}
		}
		pos = 0;
	}

}
//ok,probemos todo
//listo... a probarlo
