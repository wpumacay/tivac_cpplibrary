/*
 * myIostream.hpp
 *
 *  Created on: 25/09/2013
 *      Author: wilbert
 */
#include "include/Uart.h"

namespace myIOstream
{

	template<uart::module::instance module_>
	class c_iostream
	{
		public:

			void operator << (unsigned char *buff,unsigned int numOfBytes);

	};

	void cout::operator <<(unsigned char *buff,unsigned int numOfBytes)
	{
		unsigned int i;
		for(i=0;i<numOfBytes;i++)
		{
			uart::uartMod<module_>::sendByte(*buff);
			buff++;
		}
	}


}
