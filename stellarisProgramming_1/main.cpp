#include "include/Gpio.hpp"
#include "include/clock.hpp"
#include "delays.hpp"

int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
								 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	while(1)
	{
		PF2::toogle();
		delays::delay_ms(100);
	}
}
