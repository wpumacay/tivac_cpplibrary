/*
 * Uart.h
 *
 *  Created on: 26/07/2013
 *      Author: wilbert
 */

#ifndef UART_H_
#define UART_H_

#include "MapUart.h"
#include "mapClock.h"




namespace uart
{




	namespace module
	{
		enum instance
		{
			uart0 = 0x4000c000,
			uart1 = 0x4000d000,
			uart2 = 0x4000e000,
			uart3 = 0x4000f000,
			uart4 = 0x40010000,
			uart5 = 0x40011000,
			uart6 = 0x40012000,
			uart7 = 0x40013000
		};

	}

	namespace uartclock
	{
		enum _uartclock
		{
			uartclock_16Mhz = 16000000
		};
	}

	namespace clockSource
	{
		enum _clockSource
		{
			systemClock = 0x0,
			pioscClock  = 0x5
		};
	}

	namespace configOptions
	{
		namespace baudrate
		{
			enum _baudrate
			{
				baud_9600   = 9600,
				baud_19200  = 19200,
				baud_38400  = 38400,
				baud_57600  = 57600,
				baud_115200 = 115200,
				baud_100000 = 100000,
				baud_230400 = 230400
			};
		}
		namespace nbits
		{
			enum _nbits
			{
				bits_8 = 0x03,
				bits_7 = 0x02,
				bits_6 = 0x01,
				bits_5 = 0x00
			};
		}


		namespace parity{
			enum _parity
			{
				none = 0,
				odd  = 1,
				even = 3
			};
		}
		namespace stopbits
		{
			enum _stopbits
			{
				stopBits_one = 0,
				stopBits_two = 1
			};
		}
		namespace interrupts
		{
			enum _interrupts
			{
				receiveInt 			= 16,
				receiveTimeOut 		= 64
			};
		}
		namespace fifo
		{
			enum _fifo
			{
				fifoEnabled  = 1,
				fifoDisabled = 0
			};
		}

		namespace dma
		{
			enum dmaOpts
			{
				dmaTxEnabled = 2,
				dmaRxEnabled = 1
			};
		}
	}


	namespace utils
	{
		void calcBrd(unsigned int uartClock,unsigned int baudRate,unsigned int& brdInt,unsigned int& brdFrc)
		{
			float foo = (float)(uartClock)/(16.0*((float)(baudRate)));
			brdInt = (unsigned int)foo;
			brdFrc = (unsigned int)((foo-brdInt)*64.0 + 0.5);
		}

	}

	template<uart::module::instance mod>
	class uart
	{
		public:

			static inline void enableClock()
			{
/*				reinterpret_cast<mapClock::ClockRegs *>
					(mapClock::moduleAddresses::clockAddress)
					->RCGCUART |= (1<<(((mod-0x40000000)>>12)-12));
*/
				switch (mod) {
					case module::uart0:
						reinterpret_cast<mapClock::ClockRegs *>
							(mapClock::moduleAddresses::clockAddress)
							->RCGCUART |= (1<<0);
					break;
					case module::uart1:
						reinterpret_cast<mapClock::ClockRegs *>
							(mapClock::moduleAddresses::clockAddress)
							->RCGCUART |= (1<<1);
					break;
					case module::uart2:
						reinterpret_cast<mapClock::ClockRegs *>
							(mapClock::moduleAddresses::clockAddress)
							->RCGCUART |= (1<<2);
					break;
					case module::uart3:
						reinterpret_cast<mapClock::ClockRegs *>
							(mapClock::moduleAddresses::clockAddress)
							->RCGCUART |= (1<<3);
					break;
					case module::uart4:
						reinterpret_cast<mapClock::ClockRegs *>
							(mapClock::moduleAddresses::clockAddress)
							->RCGCUART |= (1<<4);
					break;
					case module::uart5:
						reinterpret_cast<mapClock::ClockRegs *>
							(mapClock::moduleAddresses::clockAddress)
							->RCGCUART |= (1<<5);
					break;
					case module::uart6:
						reinterpret_cast<mapClock::ClockRegs *>
							(mapClock::moduleAddresses::clockAddress)
							->RCGCUART |= (1<<6);
					break;
					case module::uart7:
						reinterpret_cast<mapClock::ClockRegs *>
							(mapClock::moduleAddresses::clockAddress)
							->RCGCUART |= (1<<7);
					break;

				}
			}

			static inline void disableUart()
			{
				reinterpret_cast<mapUart::UartRegs *>
					(mod)->UARTCTL &= 0xfffe;
			}
			static inline void enableUart()
			{
				reinterpret_cast<mapUart::UartRegs *>
					(mod)->UARTCTL |= 0x0001;
			}


			static inline void configUart(configOptions::baudrate::_baudrate baudrate_,
										  configOptions::nbits::_nbits nbits_,
										  configOptions::parity::_parity parity_,
										  configOptions::stopbits::_stopbits stopbits_,
										  unsigned int interrupts_,
										  configOptions::fifo::_fifo fifo_,
										  clockSource::_clockSource clockSource_ )
			{
				unsigned int brdInt,brdFrc;
				utils::calcBrd(16000000,baudrate_,brdInt,brdFrc);
				reinterpret_cast<mapUart::UartRegs *>
					(mod)->UARTCTL &= 0xfffe;
				reinterpret_cast<mapUart::UartRegs *>
					(mod)->UARTIBRD = brdInt;
				reinterpret_cast<mapUart::UartRegs *>
					(mod)->UARTFBRD = brdFrc;
				reinterpret_cast<mapUart::UartRegs *>
					(mod)->UARTLCRH = (nbits_<<5) | (fifo_<<4) | (stopbits_<<3) | (parity_<<1);
				reinterpret_cast<mapUart::UartRegs *>
					(mod)->UARTCC = clockSource_;
				reinterpret_cast<mapUart::UartRegs *>
					(mod)->UARTIM = interrupts_;

				reinterpret_cast<mapUart::UartRegs *>
					(mod)->UARTIFLS = (0x3<<0);

				reinterpret_cast<mapUart::UartRegs *>
					(mod)->UARTCTL |= 1;
			}

			static inline void enableDMA(unsigned int dmaOptions)
			{
				reinterpret_cast<mapUart::UartRegs *>
					(mod)->UARTDMACTL = dmaOptions;
			}

			static inline void sendByte(unsigned char data)
			{
				/*unsigned int i = 0;
				reinterpret_cast<mapUart::UartRegs *>
					(mod)->UARTCTL |= (1<<8);
				reinterpret_cast<mapUart::UartRegs *>
					(mod)->UARTDR = data;
				while((reinterpret_cast<mapUart::UartRegs *>(mod)->UARTFR)&0x8 == 0x8);
				reinterpret_cast<mapUart::UartRegs *>
					(mod)->UARTCTL &= 0xffffffff - (1<<8);
				for(i=0;i<500;i++);
				 */
				//while((reinterpret_cast<mapUart::UartRegs *>(mod)->UARTFR)&0x10 == 0x10);
				while(((reinterpret_cast<mapUart::UartRegs *>(mod)->UARTFR)&0x20) == 0x20);//THIS WORKS!!!
				//while(((reinterpret_cast<mapUart::UartRegs *>(mod)->UARTFR)&0x00000020)>>5 == 1);//this works to!!!!!
				reinterpret_cast<mapUart::UartRegs *>
					(mod)->UARTDR = data;
				//while((reinterpret_cast<mapUart::UartRegs *>(mod)->UARTFR)&0x8 == 0x8);
				//while((reinterpret_cast<mapUart::UartRegs *>(mod)->UARTFR)&0x10 != 0x10);
			}
			static inline unsigned char receiveByte()
			{
				return reinterpret_cast<mapUart::UartRegs *>(mod)->UARTDR;
			}
			static inline void clearInterrupts(unsigned int interrupts_)
			{
				reinterpret_cast<mapUart::UartRegs *>
					(mod)->UARTICR = interrupts_;
			}
	};

	//mapUart::UartRegs *uart0_regs = reinterpret_cast<mapUart::UartRegs *>(module::uart0)

}



#define UART0_REGS reinterpret_cast<mapUart::UartRegs *>(uart::module::uart0)
#define UART1_REGS reinterpret_cast<mapUart::UartRegs *>(uart::module::uart1)
#define UART2_REGS reinterpret_cast<mapUart::UartRegs *>(uart::module::uart2)
#define UART3_REGS reinterpret_cast<mapUart::UartRegs *>(uart::module::uart3)
#define UART4_REGS reinterpret_cast<mapUart::UartRegs *>(uart::module::uart4)
#define UART5_REGS reinterpret_cast<mapUart::UartRegs *>(uart::module::uart5)
#define UART6_REGS reinterpret_cast<mapUart::UartRegs *>(uart::module::uart6)
#define UART7_REGS reinterpret_cast<mapUart::UartRegs *>(uart::module::uart7)


#endif /* UART_H_ */
