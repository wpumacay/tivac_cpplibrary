/*
 * Gpio.h
 *
 *  Created on: 10/02/2013
 *      Author: Lucia
 */

#ifndef GPIO_H_
#define GPIO_H_
#include "MapGpio.h"
#include "mapClock.h"


namespace Gpio{

	namespace peripheral
	{
		enum _peripheral
		{
			GPIOA = 0,
			GPIOB = 1,
			GPIOC = 2,
			GPIOD = 3,
			GPIOE = 4,
			GPIOF = 5
		};
	}


	inline void enableClock(peripheral::_peripheral peripheral_)
	{
		reinterpret_cast<mapClock::ClockRegs *>
			(mapClock::moduleAddresses::clockAddress)
			->RCGCGPIO |= (1<<peripheral_);
	}

	namespace configOptions
	{

	}


	template<mapGpio::GpioAdress Port,
			 mapGpio::Pin pin>
	class gpioCurrentDrive{

		public:
			inline static void setCurrentDrive(mapGpio::options::currentDrive::currentDrive currentDrive){
				switch (currentDrive){
					case mapGpio::options::currentDrive::mA_2:
						reinterpret_cast<mapGpio::GpioRegs *>
							(Port)->GPIODR2R = 1<<pin;
						break;
					case mapGpio::options::currentDrive::mA_4:
						reinterpret_cast<mapGpio::GpioRegs *>
							(Port)->GPIODR4R = 1<<pin;
						break;
					case mapGpio::options::currentDrive::mA_8:
						reinterpret_cast<mapGpio::GpioRegs *>
							(Port)->GPIODR8R = 1<<pin;
						break;
				}
			}
	};

	template<mapGpio::GpioAdress Port,mapGpio::Pin pin>
	class gpio{
		public:

			inline static void unlock(){
				reinterpret_cast<mapGpio::GpioRegs *>
					(Port)->GPIOLOCK = 0x4c4f434b;
			}

			inline static void enableCommit(){
				reinterpret_cast<mapGpio::GpioRegs *>
					(Port)->GPIOCR = 1<<pin;
			}

			inline static void setPullUp(){
				reinterpret_cast<mapGpio::GpioRegs *>
					(Port)->GPIOPUR |= 1<<pin;
			}

			inline static void setPullDown(){
				reinterpret_cast<mapGpio::GpioRegs *>
					(Port)->GPIOPDR |= 1<<pin;
			}

			inline static void setOpenDrain(){
				reinterpret_cast<mapGpio::GpioRegs *>
					(Port)->GPIOODR |= 1<<pin;
			}

			inline static void enableAsDigital(){
				reinterpret_cast<mapGpio::GpioRegs *>
					(Port)->GPIODEN |= 1<<pin;
			}

			inline static void disableDigitalFunc(){
				reinterpret_cast<mapGpio::GpioRegs *>
					(Port)->GPIODEN &= 0x00ff - (1<<pin);
			}

			inline static void setMode(mapGpio::options::mode::mode moder){
				reinterpret_cast<mapGpio::GpioRegs *>
					(Port)->GPIOAFSEL |= moder<<pin;
			}
			inline static void setAnalogMode()
			{
				reinterpret_cast<mapGpio::GpioRegs *>
					(Port)->GPIOAMSEL |= 1<<pin;//
			}

			inline static void setIOmode(mapGpio::options::IOmode::IOmode iomode){
				reinterpret_cast<mapGpio::GpioRegs *>
					(Port)->GPIODIR |= iomode<<pin;
			}

			inline static void setAlternateMode(mapGpio::options::altModes::altModes altMode){
				reinterpret_cast<mapGpio::GpioRegs *>
					(Port)->GPIOPCTL |= altMode<<4*pin;
			}

			inline static void setCurrentDrive_mA(mapGpio::options::currentDrive::currentDrive mA){
				gpioCurrentDrive<Port,pin>::setCurrentDrive(mA);
			}
/*
 * this functions have been successfully tested
 *
 */
			inline static void setHigh(){
				reinterpret_cast<mapGpio::GpioRegs *>
					(Port + 0x000003FC)->GPIODATA |= 1<<pin;
			}
			inline static bool isHigh(){
				return ((reinterpret_cast<mapGpio::GpioRegs *>
						(Port + 0x000003FC)->GPIODATA & (1<<pin)) == (1<<pin));
			}
			inline static void setLow(){
				reinterpret_cast<mapGpio::GpioRegs *>
					(Port + 0x000003FC)->GPIODATA &= 0x00ff - (1<<pin);
			}
			inline static void toogle(){
				if(gpio::isHigh()){
					gpio::setLow();
				}
				else{
					gpio::setHigh();
				}
			}


	};



}



#endif /* GPIO_H_ */
