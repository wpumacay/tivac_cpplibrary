//usando el puerto serie

#include "include/clock.hpp"
#include "include/Gpio.hpp"
#include "include/Core.hpp"
//#include "interrupts.hpp"
#include "include/Uart.h"
#include "delays.hpp"

//listo, tne�a q cerrar labview para liberar el puerto....
//estaba probadno con labview
// no es exactamente lo q esperabamos...
//vamos a editarlo...

// ahora si se ve mejor xD!!!
//ok, en la siguiente continuaremos con la interrupci�n
//del UART....
//......
int main()
{

	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);//para los pines del puerto F
	Gpio::enableClock(Gpio::peripheral::GPIOA);//para los pines del puerto A (uarttx y uartrx)

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	//configurarndo los pines del uart RX y TX
	PA0::enableAsDigital();
	PA0::setMode(Gpio::options::mode::alternate);//elegimos el modo alterno para usarlo como UART y no como GPIO
	PA0::setAlternateMode(Gpio::options::altModes::alt1);//el modo alterno para este pin corresponde al del UART RX

	PA1::enableAsDigital();
	PA1::setMode(Gpio::options::mode::alternate);//elegimos el modo alterno para usarlo como UART y no como GPIO
	PA1::setAlternateMode(Gpio::options::altModes::alt1);//el modo alterno para este pin corresponde al del UART TX

	UART0::enableClock();
	UART0::configUart(uart::configOptions::baudrate::baud_1000000,//1mbps
					  uart::configOptions::nbits::bits_8,
					  uart::configOptions::parity::none,
					  uart::configOptions::stopbits::stopBits_one,
					  uart::configOptions::interrupts::none,
					  uart::configOptions::fifo::fifoDisabled,
					  uart::clockSource::systemClock);


	while(1)
	{
		PF2::setHigh();
		UART0::sendString("led pf2 en alto!!!\n\r");
		delays::delay_ms(500);
		PF2::setLow();
		UART0::sendString("led pf2 en bajo!!!\r\n");
		delays::delay_ms(500);
	}

}
