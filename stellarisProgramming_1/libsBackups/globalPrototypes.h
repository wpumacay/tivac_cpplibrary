/*
 * globalPrototypes.h
 *
 *  Created on: 30/06/2013
 *      Author: wilbert
 */

#ifndef GLOBALPROTOTYPES_H_
#define GLOBALPROTOTYPES_H_

//extern void initPieVectTable();
typedef void (*PINT)(void);


struct VECTABLE {
	PINT stackTop;
	PINT reset_isr;
	PINT nmi_isr;
	PINT fault_isr;
	PINT mpuFault_isr;
	PINT busFault_isr;
	PINT usageFault_isr;
	PINT reserved_1_1;
	PINT reserved_1_2;
	PINT reserved_1_3;
	PINT reserved_1_4;
	PINT svCall_isr;
	PINT debugMonitor_isr;
	PINT reserved_2_1;
	PINT pendSV_isr;
	PINT sysTick_isr;
	PINT gpioA_isr;
	PINT gpioB_isr;
	PINT gpioC_isr;
	PINT gpioD_isr;
	PINT gpioE_isr;
	PINT uart0rxtx_isr;
	PINT uart1rxtx_isr;
	PINT ssi0rxtx_isr;
	PINT i2c0masterSlave_isr;
	PINT pwmFault_isr;
	PINT pwm0_isr;
	PINT pwm1_isr;
	PINT pwm2_isr;
	PINT quadEnc0_isr;
	PINT adcSeq0_isr;
	PINT adcSeq1_isr;
	PINT adcSeq2_isr;
	PINT adcSeq3_isr;
	PINT watchdogTimer_isr;
	PINT timer0SubA_isr;
	PINT timer0SubB_isr;
	PINT timer1SubA_isr;
	PINT timer1SubB_isr;
	PINT timer2SubA_isr;
	PINT timer2SubB_isr;
	PINT analogComparator0_isr;
	PINT analogComparator1_isr;
	PINT analogComparator2_isr;
	PINT sysCtrl_isr;
	PINT flashCtrl_isr;
	PINT gpioF_isr;
	PINT gpioG_isr;
	PINT gpioH_isr;
	PINT uart2rxtx_isr;
	PINT ssi1rxtx_isr;
	PINT timer3SubA_isr;
	PINT timer3SubB_isr;
	PINT i2c1masterSlave_isr;
	PINT quadEnc1_isr;
	PINT can0_isr;
	PINT can1_isr;
	PINT can2_isr;
	PINT ethernet_isr;
	PINT hibernate_isr;
	PINT usb0_isr;
	PINT pwm3_isr;
	PINT uDMAsofTransfer_isr;
	PINT uDMAerror_isr;
	PINT adc_1_Seq0_isr;
	PINT adc_1_Seq1_isr;
	PINT adc_1_Seq2_isr;
	PINT adc_1_Seq3_isr;
	PINT i2s0_isr;
	PINT extBusInterface_isr;
	PINT gpioJ_isr;
	PINT gpioK_isr;
	PINT gpioL_isr;
	PINT ssi2rxtx_isr;
	PINT ssi3rxtx_isr;
	PINT uart3rxtx_isr;
	PINT uart4rxtx_isr;
	PINT uart5rxtx_isr;
	PINT uart6rxtx_isr;
	PINT uart7rxtx_isr;
	PINT reserver_3_1;
	PINT reserver_3_2;
	PINT reserver_3_3;
	PINT reserver_3_4;
	PINT i2c2masterSlave_isr;
	PINT i2c3masterSlave_isr;
	PINT timer4SubA_isr;
	PINT timer4SubB_isr;
	PINT reserver_4_1;
	PINT reserver_4_2;
	PINT reserver_4_3;
	PINT reserver_4_4;
	PINT reserver_4_5;
	PINT reserver_4_6;
	PINT reserver_4_7;
	PINT reserver_4_8;
	PINT reserver_4_9;
	PINT reserver_4_10;
	PINT reserver_4_11;
	PINT reserver_4_12;
	PINT reserver_4_13;
	PINT reserver_4_14;
	PINT reserver_4_15;
	PINT reserver_4_16;
	PINT reserver_4_17;
	PINT reserver_4_18;
	PINT reserver_4_19;
	PINT reserver_4_20;
	PINT timer5SubA_isr;
	PINT timer5SubB_isr;
	PINT timer0WideA_isr;
	PINT timer0WideB_isr;
	PINT timer1WideA_isr;
	PINT timer1WideB_isr;
	PINT timer2WideA_isr;
	PINT timer2WideB_isr;
	PINT timer3WideA_isr;
	PINT timer3WideB_isr;
	PINT timer4WideA_isr;
	PINT timer4WideB_isr;
	PINT timer5WideA_isr;
	PINT timer5WideB_isr;
	PINT fpu_isr;
	PINT peci0_isr;
	PINT lpc0_isr;
	PINT i2c4masterSlave_isr;
	PINT i2c5masterSlave_isr;
	PINT gpioM_isr;
	PINT gpioN_isr;
	PINT quadEnc2_isr;
	PINT fan0_isr;
	PINT reserved_5_1;
	PINT gpioP_isr;
	PINT gpioP1_isr;
	PINT gpioP2_isr;
	PINT gpioP3_isr;
	PINT gpioP4_isr;
	PINT gpioP5_isr;
	PINT gpioP6_isr;
	PINT gpioP7_isr;
	PINT gpioQ_isr;
	PINT gpioQ1_isr;
	PINT gpioQ2_isr;
	PINT gpioQ3_isr;
	PINT gpioQ4_isr;
	PINT gpioQ5_isr;
	PINT gpioQ6_isr;
	PINT gpioQ7_isr;
	PINT gpioR_isr;
	PINT gpioS_isr;
	PINT pwm_1_0_isr;
	PINT pwm_1_1_isr;
	PINT pwm_1_2_isr;
	PINT pwm_1_3_isr;
	PINT pwm_1_Fault_isr;
};


//extern volatile struct VECTABLE vectorTable;

#endif /* GLOBALPROTOTYPES_H_ */
