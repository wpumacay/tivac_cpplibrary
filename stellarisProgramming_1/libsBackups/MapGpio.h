/*
 * MapGpio.h
 *
 *  Created on: 10/02/2013
 *      Author: Lucia
 */

#ifndef MAPGPIO_H_
#define MAPGPIO_H_

#include "common.h"

namespace mapGpio {

	namespace options{

		namespace mode{
			enum mode{
				gpio = 0,
				alternate = 1
			};
		}
		namespace IOmode{
			enum IOmode{
				input = 0,
				output = 1
			};
		}
		namespace altModes{
			enum altModes{
				alt1  = 1,
				alt2  = 2,
				alt3  = 3,
				alt4  = 4,
				alt5  = 5,
				alt6  = 6,
				alt7  = 7,
				alt8  = 8,
				alt9  = 9,
				alt10 = 10,
				alt11 = 11,
				alt12 = 12,
				alt13 = 13,
				alt14 = 14,
				alt15 = 15
			};
		}
		namespace currentDrive {
			enum currentDrive{
				mA_2 = 0,
				mA_4 = 1,
				mA_8 = 2
			};
		}
	}

	enum Pin{
		gpio0 = 0,
		gpio1 = 1,
		gpio2 = 2,
		gpio3 = 3,
		gpio4 = 4,
		gpio5 = 5,
		gpio6 = 6,
		gpio7 = 7
	};



	enum GpioAdress{

		GPIOPortA_APB= 0x40004000,
		GPIOPortA_AHB= 0x40058000,
		GPIOPortB_APB= 0x40005000,
		GPIOPortB_AHB= 0x40059000,
		GPIOPortC_APB= 0x40006000,
		GPIOPortC_AHB= 0x4005A000,
		GPIOPortD_APB= 0x40007000,
		GPIOPortD_AHB= 0x4005B000,
		GPIOPortE_APB= 0x40024000,
		GPIOPortE_AHB= 0x4005C000,
		GPIOPortF_APB= 0x40025000,
		GPIOPortF_AHB= 0x4005D000
	};

	struct GpioRegs{
		u32 GPIODATA;//0x000
		u32 rsvd1[255];//[0x004-0x400>
		u32 GPIODIR;//0x400
		u32 GPIOIS;//0x404
		u32 GPIOIBE;//0x408
		u32 GPIOIEV;//0x40c
		u32 GPIOIM;//0x410
		u32 GPIORIS;//0x414
		u32 GPIOMIS;//0x418
		u32 GPIOICR;//0x41c
		u32 GPIOAFSEL;//0x420
		u32 rsvd2[55];//[0x424-0x500>
		u32 GPIODR2R;//0x500
		u32 GPIODR4R;//0x504
		u32 GPIODR8R;//0x508
		u32 GPIOODR;//0x50c
		u32 GPIOPUR;//0x510
		u32 GPIOPDR;//0x514
		u32 GPIOSLR;//0x518
		u32 GPIODEN;//0x51c
		u32 GPIOLOCK;//0x520
		u32 GPIOCR;//0x524
		u32 GPIOAMSEL;//0x528
		u32 GPIOPCTL;//0x52c
		u32 GPIOADCCTL;//0x530
		u32 GPIODMACTL;//0x534
		u32 rsvd3[679];//[0x538-0xfd0>
		u32 GPIOPeriphID4;//0xfd0
		u32 GPIOPeriphID5;//0xfd4
		u32 GPIOPeriphID6;//0xfd8
		u32 GPIOPeriphID7;//0xfdc
		u32 GPIOPeriphID0;//0xfe0
		u32 GPIOPeriphID1;//0xfe4
		u32 GPIOPeriphID2;//0xfe8
		u32 GPIOPeriphID3;//0xfec
		u32 GPIOPCellID0;//0xff0
		u32 GPIOPCellID1;//0xff4
		u32 GPIOPCellID2;//0xff8
		u32 GPIOPCellID3;//0xffc
	};

}



#endif /* MAPGPIO_H_ */
