/*
 * I2C.h
 *
 *  Created on: 31/07/2013
 *      Author: wilbert
 */

#ifndef I2C_H_
#define I2C_H_

#include "MapI2C.h"

namespace i2c
{

	namespace module
	{
		enum _module
		{
			i2c0 = 0x40020000,
			i2c1 = 0x40021000,
			i2c2 = 0x40022000,
			i2c3 = 0x40023000
		};
	}

	namespace options
	{
		namespace rw
		{
			enum _rw
			{
				read = 1,
				write = 0
			};
		}
	}


	namespace configOptions
	{
		namespace mode
		{
			enum _mode
			{
				master = 1,
				slave  = 2
			};

		}
		namespace loopback
		{
			enum _loopback
			{
				enabled  = 1,
				disabled = 0
			};
		}

		namespace speed
		{
			enum _speed
			{
				standarMode  = 100000,
				fastMode     = 400000,
				fastPlusMode = 1000000
			};
		}
	}

	template<module::_module module_>
	class i2c
	{
		public:
			static inline void writeSlaveAddress(unsigned int address,
												 options::rw::_rw rw_)
			{
				reinterpret_cast<mapi2c::I2CRegs *>
					(module_)->I2CMSA = (address<<1) | rw_;
			}
			static inline void configSpeed(configOptions::speed::_speed speed_)
			{
				reinterpret_cast<mapi2c::I2CRegs *>
					(module_)->I2CMTPR = (80*1000000/(2*10*speed_))-1;
			}
			static inline void config(configOptions::mode::_mode mode_,
									  configOptions::loopback::_loopback loopback_,
									  configOptions::speed::_speed speed_)
			{
				reinterpret_cast<mapi2c::I2CRegs *>
					(module_)->I2CMCR = (mode_<<4) | (loopback_);
				reinterpret_cast<mapi2c::I2CRegs *>
					(module_)->I2CMTPR = (80*1000000/(2*10*speed_))-1;
			}
			static inline bool send(unsigned char slaveAddress,
									unsigned char *buffData,
									unsigned int nBytes)
			{
				unsigned int i = 0;
				//bool sentinel = true;
				reinterpret_cast<mapi2c::I2CRegs *>
					(module_)->I2CMSA = (slaveAddress<<1)|0x00;
				reinterpret_cast<mapi2c::I2CRegs *>
					(module_)->I2CMDR = *buffData;
				//while(((reinterpret_cast<mapi2c::I2CRegs *>(module_)->I2CMCS)&(0x40))==0x40);
				reinterpret_cast<mapi2c::I2CRegs *>
					(module_)->I2CMCS = 0x03;
				buffData++;
				i++;
				while(true)
				{
					while(((reinterpret_cast<mapi2c::I2CRegs *>(module_)->I2CMCS)&(0x01))==1);
					if(((reinterpret_cast<mapi2c::I2CRegs *>(module_)->I2CMCS)&0x02)==0x02)
					{
						while(1);
						//return false;
					}
					reinterpret_cast<mapi2c::I2CRegs *>
						(module_)->I2CMDR = *buffData;
					buffData++;
					i++;
					if(i==(nBytes))
					{
						break;
					}
					reinterpret_cast<mapi2c::I2CRegs *>
						(module_)->I2CMCS = 0x01;
				}
				reinterpret_cast<mapi2c::I2CRegs *>
					(module_)->I2CMCS = 0x05;
				while(((reinterpret_cast<mapi2c::I2CRegs *>(module_)->I2CMCS)&(0x01))==0x01);
				if(((reinterpret_cast<mapi2c::I2CRegs *>(module_)->I2CMCS)&0x02)==0x02)
				{
					while(1);
					//return false;
				}
				return true;
			}
			//static inline void read()
	};


}



#endif /* I2C_H_ */
