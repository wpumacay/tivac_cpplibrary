/*
 * Adc.h
 *
 *  Created on: 29/07/2013
 *      Author: wilbert
 */

#ifndef ADC_H_
#define ADC_H_

#include "MapADC.h"
#include "mapClock.h"

namespace adc
{

	namespace converter
	{
		enum _converter
		{
			adc0 = 0x40038000,
			adc1 = 0x40039000
		};
	}
	namespace sequencer
	{
		enum _sequencer
		{
			sequencer0 = 1,
			sequencer1 = 2,
			sequencer2 = 4,
			sequencer3 = 8
		};

	}
	namespace configOptions
	{
		namespace trigger
		{
			enum _trigger
			{
				software 	= 0x00,
				analogComp0 = 0x01,
				analogComp1 = 0x02,
				extGpio     = 0x04,
				timer	    = 0x05,
				continuous  = 0x0f
			};
		}

		namespace sample
		{
			enum _sample
			{
				sample_1st = 0,
				sample_2nd = 4,
				sample_3rd = 8,
				sample_4th = 12,
				sample_5th = 16,
				sample_6th = 20,
				sample_7th = 24,
				sample_8th = 28
			};
		}


		namespace inputpin
		{
			enum _inputpin
			{
				ain0  = 0,
				ain1  = 1,
				ain2  = 2,
				ain3  = 3,
				ain4  = 4,
				ain5  = 5,
				ain6  = 6,
				ain7  = 7,
				ain8  = 8,
				ain9  = 9,
				ain10 = 10,
				ain11 = 11
			};
		}
		namespace tempsensor
		{
			enum _tempsensor
			{
				selectTempSensor  = 8,
				ommitTempSensor   = 0
			};
		}
		namespace interruptEnable
		{
			enum _interruptEnable
			{
				sampleIntEnable   = 4,
				sampleIntDisabled = 0
			};
		}
		namespace endOfSequence
		{
			enum _endOfSequence
			{
				isEndOfSample   = 2,
				isntEndOfSample = 0
			};
		}
		namespace differentialMode
		{
			enum _differentialMode
			{
				selectDiffMode = 1,
				ommitDiffMode  = 0
			};
		}

		namespace sampleRate
		{
			enum _sampleRate{
				sample_125ksps  = 0x1,
				sample_250ksps  = 0x3,
				sample_500ksps  = 0x5,
				sample_1msps	= 0x7
			};
		}
	}




	inline void setSampleRate(converter::_converter converter_,configOptions::sampleRate::_sampleRate sampleRate_)
	{
		reinterpret_cast<mapAdc::AdcRegs *>
			(converter_)->ADCPC = sampleRate_;
	}

	inline void enableClock(converter::_converter converter_)
	{
		switch(converter_) {
			case converter::adc0:
				reinterpret_cast<mapClock::ClockRegs *>
					(mapClock::moduleAddresses::clockAddress)
					->RCGCADC |= (1<<0);
			break;
			case converter::adc1:
				reinterpret_cast<mapClock::ClockRegs *>
					(mapClock::moduleAddresses::clockAddress)
					->RCGCADC |= (1<<1);
			break;
		}
	}

	template<adc::converter::_converter converter_,adc::sequencer::_sequencer sequencer_>
	class adc
	{

		public:



			static inline void disableSequencer()
			{
				reinterpret_cast<mapAdc::AdcRegs *>
					(converter_)->ADCACTSS &= (0x000f - sequencer_);
			}
			static inline void enableSequencer()
			{
				reinterpret_cast<mapAdc::AdcRegs *>
					(converter_)->ADCACTSS |= sequencer_;
			}

			static inline void selectTrigger(configOptions::trigger::_trigger trigger_)
			{
				reinterpret_cast<mapAdc::AdcRegs *>
					(converter_)->ADCEMUX = trigger_;
			}

			static inline void sampleConfigure(configOptions::sample::_sample sample_,
											   configOptions::inputpin::_inputpin inputpin_,
											   configOptions::tempsensor::_tempsensor tempsensor_,
											   configOptions::interruptEnable::_interruptEnable intEnable_,
											   configOptions::endOfSequence::_endOfSequence endOfSequence_,
											   configOptions::differentialMode::_differentialMode diffMode_)
			{
				switch (sequencer_)
				{
					case sequencer::sequencer0 :
						reinterpret_cast<mapAdc::AdcRegs *>
							(converter_)->ADCSSMUX0 |= (inputpin_ << sample_);
						reinterpret_cast<mapAdc::AdcRegs *>
							(converter_)->ADCSSCTL0 |= ((tempsensor_|intEnable_|endOfSequence_|diffMode_) << sample_);
						break;
					case sequencer::sequencer1 :
						reinterpret_cast<mapAdc::AdcRegs *>
							(converter_)->ADCSSMUX1 |= (inputpin_ << sample_);
						reinterpret_cast<mapAdc::AdcRegs *>
							(converter_)->ADCSSCTL1 |= ((tempsensor_|intEnable_|endOfSequence_|diffMode_) << sample_);
						break;
					case sequencer::sequencer2 :
						reinterpret_cast<mapAdc::AdcRegs *>
							(converter_)->ADCSSMUX2 |= (inputpin_ << sample_);
						reinterpret_cast<mapAdc::AdcRegs *>
							(converter_)->ADCSSCTL2 |= ((tempsensor_|intEnable_|endOfSequence_|diffMode_) << sample_);
						break;
					case sequencer::sequencer3 :
						reinterpret_cast<mapAdc::AdcRegs *>
							(converter_)->ADCSSMUX3 |= (inputpin_ << sample_);
						reinterpret_cast<mapAdc::AdcRegs *>
							(converter_)->ADCSSCTL3 |= ((tempsensor_|intEnable_|endOfSequence_|diffMode_) << sample_);
						break;
					default:
						break;
				}
			}

			static inline void softBeginSampling()
			{
				reinterpret_cast<mapAdc::AdcRegs *>
					(converter_)->ADCPSSI |=sequencer_;
			}

			static inline bool finishedSampling()
			{
				if(((reinterpret_cast<mapAdc::AdcRegs *>(converter_)->ADCRIS)&(sequencer_))== sequencer_ )
				{
					return true;
				}
				return false;
			}
			static inline void clearInterrupt()
			{
				reinterpret_cast<mapAdc::AdcRegs *>
					(converter_)->ADCISC |=sequencer_;
			}



			static inline void takeData(unsigned int* buff)
			{
				switch(sequencer_)
				{
				case sequencer::sequencer0 :
					while(!(((reinterpret_cast<mapAdc::AdcRegs *>(converter_)->ADCSSFSTAT0)&(0x0100))==(0x0100)))
					{
						*buff = reinterpret_cast<mapAdc::AdcRegs *>(converter_)->ADCSSFIFO0;
						buff++;
					}
					break;
				case sequencer::sequencer1 :
					while(!(((reinterpret_cast<mapAdc::AdcRegs *>(converter_)->ADCSSFSTAT1)&(0x0100))==(0x0100)))
					{
						*buff = reinterpret_cast<mapAdc::AdcRegs *>(converter_)->ADCSSFIFO1;
						buff++;
					}
					break;
				case sequencer::sequencer2 :
					while(!(((reinterpret_cast<mapAdc::AdcRegs *>(converter_)->ADCSSFSTAT2)&(0x0100))==(0x0100)))
					{
						*buff = reinterpret_cast<mapAdc::AdcRegs *>(converter_)->ADCSSFIFO2;
						buff++;
					}
					break;
				case sequencer::sequencer3 :
					while(!(((reinterpret_cast<mapAdc::AdcRegs *>(converter_)->ADCSSFSTAT3)&(0x0100))==(0x0100)))
					{
						*buff = reinterpret_cast<mapAdc::AdcRegs *>(converter_)->ADCSSFIFO3;
						buff++;
					}
					break;
				default:
					break;
				}
			}
	};


}


#endif /* ADC_H_ */
