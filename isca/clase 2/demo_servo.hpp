// Demo usando un servomotor
// https://www.servocity.com/html/hs-422_super_sport_.html
#include "include/clock.hpp"
#include "include/Gpio.hpp"
#include "delays.hpp"
#include "analogSensors.hpp"
#include "include/Gptimer.hpp"
#include "include/Uart.h"
#include "include/Core.hpp"
#include "interrupts.hpp"

analogSensors::potentiometer<adc::converter::adc0,
							 adc::sequencer::sequencer0,
							 adc::configOptions::inputpin::ain0,
							 Gpio::Port::GPIOPortE_APB,
							 Gpio::Pin::pin3> vin;


double voltaje = 0;

float duty = 0;

unsigned char byte_rx = 0;

int main(){

	// Inicializar el microcontrolador
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOE);
	Gpio::enableClock(Gpio::peripheral::GPIOC);
	Gpio::enableClock(Gpio::peripheral::GPIOA);

	PA0::enableAsDigital();
	PA0::setMode(Gpio::options::mode::alternate);
	PA0::setAlternateMode(Gpio::options::altModes::alt1);

	PA1::enableAsDigital();
	PA1::setMode(Gpio::options::mode::alternate);
	PA1::setAlternateMode(Gpio::options::altModes::alt1);

	UART0::enableClock();
	UART0::configUart(uart::configOptions::baudrate::baud_1000000);

	PC4::enableAsDigital();
	PC4::setMode(Gpio::options::mode::alternate);
	PC4::setAlternateMode(Gpio::options::altModes::alt7);

	PF1::enableAsDigital();
	PF1::setMode(Gpio::options::mode::gpio);
	PF1::setIOmode(Gpio::options::IOmode::output);

	pwmW_0A::enableClock();
	pwmW_0A::config(20,
					duty,
					gpTimer::options::pwmOptions::outputLevel::inverted);

	vin.config(3.3,adc::configOptions::sampleRate::sample_500ksps);
	core::IntEnableMaster();
	core::enableInterrupt(core::interrupts::uart0);
	// Superloop
	while(1)
	{
		voltaje = vin.getVoltage();
		pwmW_0A::setDuty(duty);
		PF1::toogle();
		delays::delay_ms(100);
	}

}


void interruptFuncs::uart0rxtx_isr(){

	UART0::clearInterrupts(uart::configOptions::interrupts::receiveInt);
	byte_rx = UART0::receiveByte();
	duty = 0.03 + (byte_rx/255.0)*(0.12 - 0.03);
}


