

lista = [1,2,3,4,5]

print "mostrando elementos en la lista!"
for x in lista:
	print x

lista = [1,2.0,"soy un troll",True,[1,2,3]]

print "mostrando una lista troll!!!"

for x in lista:
	print "elemento: " , x
	print "tipo: " , type(x)

#usando range
print "usando range"
for x in range(10):
	print x

#usando xrange
print "usando xrange"
for x in xrange(0,9+1):
	print x

#tomando los elementos de la lista
print "otro ejemplo... con listas"

for x in range(len(lista)):
	print "lista[" + str(x) + "] = " , lista[x]


#usando un while loop
print "usando while loops"
k = 1
while k<10:
	k += 1 # equivalente a k = k + 1
	print "k: " , k

#usando break
k = 1
print "usando break"
while True:
	k += 1
	print "k: " , k
	if k>=10:
		break









